<?php
$tdataannouncer2022 = array();
$tdataannouncer2022[".searchableFields"] = array();
$tdataannouncer2022[".ShortName"] = "announcer2022";
$tdataannouncer2022[".OwnerID"] = "";
$tdataannouncer2022[".OriginalTable"] = "announcer2022";


$tdataannouncer2022[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataannouncer2022[".originalPagesByType"] = $tdataannouncer2022[".pagesByType"];
$tdataannouncer2022[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataannouncer2022[".originalPages"] = $tdataannouncer2022[".pages"];
$tdataannouncer2022[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataannouncer2022[".originalDefaultPages"] = $tdataannouncer2022[".defaultPages"];

//	field labels
$fieldLabelsannouncer2022 = array();
$fieldToolTipsannouncer2022 = array();
$pageTitlesannouncer2022 = array();
$placeHoldersannouncer2022 = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsannouncer2022["English"] = array();
	$fieldToolTipsannouncer2022["English"] = array();
	$placeHoldersannouncer2022["English"] = array();
	$pageTitlesannouncer2022["English"] = array();
	$fieldLabelsannouncer2022["English"]["ID"] = "ID";
	$fieldToolTipsannouncer2022["English"]["ID"] = "";
	$placeHoldersannouncer2022["English"]["ID"] = "";
	$fieldLabelsannouncer2022["English"]["FormID"] = "Form ID";
	$fieldToolTipsannouncer2022["English"]["FormID"] = "";
	$placeHoldersannouncer2022["English"]["FormID"] = "";
	$fieldLabelsannouncer2022["English"]["NameOfOrg"] = "Name Of Org";
	$fieldToolTipsannouncer2022["English"]["NameOfOrg"] = "";
	$placeHoldersannouncer2022["English"]["NameOfOrg"] = "";
	$fieldLabelsannouncer2022["English"]["KeyPeople"] = "Key People";
	$fieldToolTipsannouncer2022["English"]["KeyPeople"] = "";
	$placeHoldersannouncer2022["English"]["KeyPeople"] = "";
	$fieldLabelsannouncer2022["English"]["DescriptionOfEntry"] = "Description Of Entry";
	$fieldToolTipsannouncer2022["English"]["DescriptionOfEntry"] = "";
	$placeHoldersannouncer2022["English"]["DescriptionOfEntry"] = "";
	$fieldLabelsannouncer2022["English"]["LowerThirds"] = "Lower Thirds";
	$fieldToolTipsannouncer2022["English"]["LowerThirds"] = "";
	$placeHoldersannouncer2022["English"]["LowerThirds"] = "";
	if (count($fieldToolTipsannouncer2022["English"]))
		$tdataannouncer2022[".isUseToolTips"] = true;
}


	$tdataannouncer2022[".NCSearch"] = true;



$tdataannouncer2022[".shortTableName"] = "announcer2022";
$tdataannouncer2022[".nSecOptions"] = 0;

$tdataannouncer2022[".mainTableOwnerID"] = "";
$tdataannouncer2022[".entityType"] = 0;
$tdataannouncer2022[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdataannouncer2022[".strOriginalTableName"] = "announcer2022";

		 



$tdataannouncer2022[".showAddInPopup"] = false;

$tdataannouncer2022[".showEditInPopup"] = false;

$tdataannouncer2022[".showViewInPopup"] = false;

$tdataannouncer2022[".listAjax"] = false;
//	temporary
//$tdataannouncer2022[".listAjax"] = false;

	$tdataannouncer2022[".audit"] = false;

	$tdataannouncer2022[".locking"] = false;


$pages = $tdataannouncer2022[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataannouncer2022[".edit"] = true;
	$tdataannouncer2022[".afterEditAction"] = 1;
	$tdataannouncer2022[".closePopupAfterEdit"] = 1;
	$tdataannouncer2022[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataannouncer2022[".add"] = true;
$tdataannouncer2022[".afterAddAction"] = 1;
$tdataannouncer2022[".closePopupAfterAdd"] = 1;
$tdataannouncer2022[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataannouncer2022[".list"] = true;
}



$tdataannouncer2022[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataannouncer2022[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataannouncer2022[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataannouncer2022[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataannouncer2022[".printFriendly"] = true;
}



$tdataannouncer2022[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataannouncer2022[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataannouncer2022[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataannouncer2022[".isUseAjaxSuggest"] = true;

$tdataannouncer2022[".rowHighlite"] = true;



					
		

$tdataannouncer2022[".ajaxCodeSnippetAdded"] = false;

$tdataannouncer2022[".buttonsAdded"] = true;

$tdataannouncer2022[".addPageEvents"] = true;

// use timepicker for search panel
$tdataannouncer2022[".isUseTimeForSearch"] = false;


$tdataannouncer2022[".badgeColor"] = "DAA520";


$tdataannouncer2022[".allSearchFields"] = array();
$tdataannouncer2022[".filterFields"] = array();
$tdataannouncer2022[".requiredSearchFields"] = array();

$tdataannouncer2022[".googleLikeFields"] = array();
$tdataannouncer2022[".googleLikeFields"][] = "ID";
$tdataannouncer2022[".googleLikeFields"][] = "FormID";
$tdataannouncer2022[".googleLikeFields"][] = "NameOfOrg";
$tdataannouncer2022[".googleLikeFields"][] = "KeyPeople";
$tdataannouncer2022[".googleLikeFields"][] = "DescriptionOfEntry";
$tdataannouncer2022[".googleLikeFields"][] = "LowerThirds";



$tdataannouncer2022[".tableType"] = "list";

$tdataannouncer2022[".printerPageOrientation"] = 0;
$tdataannouncer2022[".nPrinterPageScale"] = 100;

$tdataannouncer2022[".nPrinterSplitRecords"] = 40;

$tdataannouncer2022[".geocodingEnabled"] = false;










$tdataannouncer2022[".pageSize"] = 20;

$tdataannouncer2022[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdataannouncer2022[".strOrderBy"] = $tstrOrderBy;

$tdataannouncer2022[".orderindexes"] = array();


$tdataannouncer2022[".sqlHead"] = "SELECT ID,  	FormID,  	NameOfOrg,  	KeyPeople,  	DescriptionOfEntry,  	LowerThirds";
$tdataannouncer2022[".sqlFrom"] = "FROM announcer2022";
$tdataannouncer2022[".sqlWhereExpr"] = "";
$tdataannouncer2022[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataannouncer2022[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataannouncer2022[".arrGroupsPerPage"] = $arrGPP;

$tdataannouncer2022[".highlightSearchResults"] = true;

$tableKeysannouncer2022 = array();
$tableKeysannouncer2022[] = "ID";
$tdataannouncer2022[".Keys"] = $tableKeysannouncer2022;


$tdataannouncer2022[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "announcer2022";
	$fdata["Label"] = GetFieldLabel("announcer2022","ID");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "ID";

		$fdata["sourceSingle"] = "ID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2022["ID"] = $fdata;
		$tdataannouncer2022[".searchableFields"][] = "ID";
//	FormID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "FormID";
	$fdata["GoodName"] = "FormID";
	$fdata["ownerTable"] = "announcer2022";
	$fdata["Label"] = GetFieldLabel("announcer2022","FormID");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "FormID";

		$fdata["sourceSingle"] = "FormID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "FormID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "Form2022";
		$edata["listPageId"] = "list";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 2;

	
		
	$edata["LinkField"] = "ID";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "ID";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	
				//dependent dropdowns @deprecated data ?
	$edata["DependentLookups"] = array();
	$edata["DependentLookups"][] = "NameOfOrg";
				//dependent dropdowns @deprecated data ?
	$edata["DependentLookups"] = array();
	$edata["DependentLookups"][] = "KeyPeople";
				//dependent dropdowns @deprecated data ?
	$edata["DependentLookups"] = array();
	$edata["DependentLookups"][] = "DescriptionOfEntry";
				//dependent dropdowns @deprecated data ?
	$edata["DependentLookups"] = array();
	$edata["DependentLookups"][] = "LowerThirds";

	
	
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
		$eventsData = array();
	$eventsData[] = array( "name" => "FormID_event1", "type" => "click" );
	$edata["fieldEvents"] = $eventsData;


// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "Form2022";
		$edata["listPageId"] = "announcersel";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 2;

	
		
	$edata["LinkField"] = "ID";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "ID";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	
				//dependent dropdowns @deprecated data ?
	$edata["DependentLookups"] = array();
	$edata["DependentLookups"][] = "NameOfOrg";
				//dependent dropdowns @deprecated data ?
	$edata["DependentLookups"] = array();
	$edata["DependentLookups"][] = "KeyPeople";
				//dependent dropdowns @deprecated data ?
	$edata["DependentLookups"] = array();
	$edata["DependentLookups"][] = "DescriptionOfEntry";
				//dependent dropdowns @deprecated data ?
	$edata["DependentLookups"] = array();
	$edata["DependentLookups"][] = "LowerThirds";

	
	
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "Form2022";
		$edata["listPageId"] = "list";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 2;

	
		
	$edata["LinkField"] = "ID";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "ID";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	
				//dependent dropdowns @deprecated data ?
	$edata["DependentLookups"] = array();
	$edata["DependentLookups"][] = "NameOfOrg";
				//dependent dropdowns @deprecated data ?
	$edata["DependentLookups"] = array();
	$edata["DependentLookups"][] = "KeyPeople";
				//dependent dropdowns @deprecated data ?
	$edata["DependentLookups"] = array();
	$edata["DependentLookups"][] = "DescriptionOfEntry";
				//dependent dropdowns @deprecated data ?
	$edata["DependentLookups"] = array();
	$edata["DependentLookups"][] = "LowerThirds";

	
	
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2022["FormID"] = $fdata;
		$tdataannouncer2022[".searchableFields"][] = "FormID";
//	NameOfOrg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "NameOfOrg";
	$fdata["GoodName"] = "NameOfOrg";
	$fdata["ownerTable"] = "announcer2022";
	$fdata["Label"] = GetFieldLabel("announcer2022","NameOfOrg");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "NameOfOrg";

		$fdata["sourceSingle"] = "NameOfOrg";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NameOfOrg";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "Form2022";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "ID";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "NameOfOrg";

	

	
	$edata["LookupOrderBy"] = "";

	
		$edata["UseCategory"] = true;
	$edata["categoryFields"] = array();
	$edata["categoryFields"][] = array( "main" => "FormID", "lookup" => "ID" );

	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2022["NameOfOrg"] = $fdata;
		$tdataannouncer2022[".searchableFields"][] = "NameOfOrg";
//	KeyPeople
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "KeyPeople";
	$fdata["GoodName"] = "KeyPeople";
	$fdata["ownerTable"] = "announcer2022";
	$fdata["Label"] = GetFieldLabel("announcer2022","KeyPeople");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "KeyPeople";

		$fdata["sourceSingle"] = "KeyPeople";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "KeyPeople";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "Form2022";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "ID";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "KeyPeople";

	

	
	$edata["LookupOrderBy"] = "";

	
		$edata["UseCategory"] = true;
	$edata["categoryFields"] = array();
	$edata["categoryFields"][] = array( "main" => "FormID", "lookup" => "ID" );

	
	

	
	
		$edata["SelectSize"] = 5;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2022["KeyPeople"] = $fdata;
		$tdataannouncer2022[".searchableFields"][] = "KeyPeople";
//	DescriptionOfEntry
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "DescriptionOfEntry";
	$fdata["GoodName"] = "DescriptionOfEntry";
	$fdata["ownerTable"] = "announcer2022";
	$fdata["Label"] = GetFieldLabel("announcer2022","DescriptionOfEntry");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "DescriptionOfEntry";

		$fdata["sourceSingle"] = "DescriptionOfEntry";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "DescriptionOfEntry";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "Form2022";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "ID";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "DescriptionOfEntry";

	

	
	$edata["LookupOrderBy"] = "";

	
		$edata["UseCategory"] = true;
	$edata["categoryFields"] = array();
	$edata["categoryFields"][] = array( "main" => "FormID", "lookup" => "ID" );

	
	

	
	
		$edata["SelectSize"] = 10;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2022["DescriptionOfEntry"] = $fdata;
		$tdataannouncer2022[".searchableFields"][] = "DescriptionOfEntry";
//	LowerThirds
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "LowerThirds";
	$fdata["GoodName"] = "LowerThirds";
	$fdata["ownerTable"] = "announcer2022";
	$fdata["Label"] = GetFieldLabel("announcer2022","LowerThirds");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "LowerThirds";

		$fdata["sourceSingle"] = "LowerThirds";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "LowerThirds";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "Form2022";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "ID";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "LowerThirds";

	

	
	$edata["LookupOrderBy"] = "";

	
		$edata["UseCategory"] = true;
	$edata["categoryFields"] = array();
	$edata["categoryFields"][] = array( "main" => "FormID", "lookup" => "ID" );

	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2022["LowerThirds"] = $fdata;
		$tdataannouncer2022[".searchableFields"][] = "LowerThirds";


$tables_data["announcer2022"]=&$tdataannouncer2022;
$field_labels["announcer2022"] = &$fieldLabelsannouncer2022;
$fieldToolTips["announcer2022"] = &$fieldToolTipsannouncer2022;
$placeHolders["announcer2022"] = &$placeHoldersannouncer2022;
$page_titles["announcer2022"] = &$pageTitlesannouncer2022;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["announcer2022"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["announcer2022"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_announcer2022()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	FormID,  	NameOfOrg,  	KeyPeople,  	DescriptionOfEntry,  	LowerThirds";
$proto0["m_strFrom"] = "FROM announcer2022";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "announcer2022",
	"m_srcTableName" => "announcer2022"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "announcer2022";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "FormID",
	"m_strTable" => "announcer2022",
	"m_srcTableName" => "announcer2022"
));

$proto8["m_sql"] = "FormID";
$proto8["m_srcTableName"] = "announcer2022";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "NameOfOrg",
	"m_strTable" => "announcer2022",
	"m_srcTableName" => "announcer2022"
));

$proto10["m_sql"] = "NameOfOrg";
$proto10["m_srcTableName"] = "announcer2022";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "KeyPeople",
	"m_strTable" => "announcer2022",
	"m_srcTableName" => "announcer2022"
));

$proto12["m_sql"] = "KeyPeople";
$proto12["m_srcTableName"] = "announcer2022";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "DescriptionOfEntry",
	"m_strTable" => "announcer2022",
	"m_srcTableName" => "announcer2022"
));

$proto14["m_sql"] = "DescriptionOfEntry";
$proto14["m_srcTableName"] = "announcer2022";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "LowerThirds",
	"m_strTable" => "announcer2022",
	"m_srcTableName" => "announcer2022"
));

$proto16["m_sql"] = "LowerThirds";
$proto16["m_srcTableName"] = "announcer2022";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto18=array();
$proto18["m_link"] = "SQLL_MAIN";
			$proto19=array();
$proto19["m_strName"] = "announcer2022";
$proto19["m_srcTableName"] = "announcer2022";
$proto19["m_columns"] = array();
$proto19["m_columns"][] = "ID";
$proto19["m_columns"][] = "FormID";
$proto19["m_columns"][] = "NameOfOrg";
$proto19["m_columns"][] = "KeyPeople";
$proto19["m_columns"][] = "DescriptionOfEntry";
$proto19["m_columns"][] = "LowerThirds";
$obj = new SQLTable($proto19);

$proto18["m_table"] = $obj;
$proto18["m_sql"] = "announcer2022";
$proto18["m_alias"] = "";
$proto18["m_srcTableName"] = "announcer2022";
$proto20=array();
$proto20["m_sql"] = "";
$proto20["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto20["m_column"]=$obj;
$proto20["m_contained"] = array();
$proto20["m_strCase"] = "";
$proto20["m_havingmode"] = false;
$proto20["m_inBrackets"] = false;
$proto20["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto20);

$proto18["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto18);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="announcer2022";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_announcer2022 = createSqlQuery_announcer2022();


	
					
;

						

$tdataannouncer2022[".sqlquery"] = $queryData_announcer2022;



$tableEvents["announcer2022"] = new eventsBase;
$tdataannouncer2022[".hasEvents"] = false;

?>