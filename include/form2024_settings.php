<?php
$tdataform2024 = array();
$tdataform2024[".searchableFields"] = array();
$tdataform2024[".ShortName"] = "form2024";
$tdataform2024[".OwnerID"] = "";
$tdataform2024[".OriginalTable"] = "Form2024";


$tdataform2024[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataform2024[".originalPagesByType"] = $tdataform2024[".pagesByType"];
$tdataform2024[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataform2024[".originalPages"] = $tdataform2024[".pages"];
$tdataform2024[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataform2024[".originalDefaultPages"] = $tdataform2024[".defaultPages"];

//	field labels
$fieldLabelsform2024 = array();
$fieldToolTipsform2024 = array();
$pageTitlesform2024 = array();
$placeHoldersform2024 = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsform2024["English"] = array();
	$fieldToolTipsform2024["English"] = array();
	$placeHoldersform2024["English"] = array();
	$pageTitlesform2024["English"] = array();
	$fieldLabelsform2024["English"]["ID"] = "ID";
	$fieldToolTipsform2024["English"]["ID"] = "";
	$placeHoldersform2024["English"]["ID"] = "";
	$fieldLabelsform2024["English"]["NameOfOrg"] = "Name Of Org";
	$fieldToolTipsform2024["English"]["NameOfOrg"] = "";
	$placeHoldersform2024["English"]["NameOfOrg"] = "";
	$fieldLabelsform2024["English"]["TypeOfOrg"] = "Type Of Org";
	$fieldToolTipsform2024["English"]["TypeOfOrg"] = "";
	$placeHoldersform2024["English"]["TypeOfOrg"] = "";
	$fieldLabelsform2024["English"]["SchoolClass"] = "School Class";
	$fieldToolTipsform2024["English"]["SchoolClass"] = "";
	$placeHoldersform2024["English"]["SchoolClass"] = "";
	$fieldLabelsform2024["English"]["EntryType"] = "Entry Type";
	$fieldToolTipsform2024["English"]["EntryType"] = "";
	$placeHoldersform2024["English"]["EntryType"] = "";
	$fieldLabelsform2024["English"]["KeyPeople"] = "Key People";
	$fieldToolTipsform2024["English"]["KeyPeople"] = "";
	$placeHoldersform2024["English"]["KeyPeople"] = "";
	$fieldLabelsform2024["English"]["EntrySize"] = "Entry Size";
	$fieldToolTipsform2024["English"]["EntrySize"] = "";
	$placeHoldersform2024["English"]["EntrySize"] = "";
	$fieldLabelsform2024["English"]["ContactName"] = "Contact Name";
	$fieldToolTipsform2024["English"]["ContactName"] = "";
	$placeHoldersform2024["English"]["ContactName"] = "";
	$fieldLabelsform2024["English"]["ContactAddress"] = "Contact Address";
	$fieldToolTipsform2024["English"]["ContactAddress"] = "";
	$placeHoldersform2024["English"]["ContactAddress"] = "";
	$fieldLabelsform2024["English"]["ContactEmail"] = "Contact Email";
	$fieldToolTipsform2024["English"]["ContactEmail"] = "";
	$placeHoldersform2024["English"]["ContactEmail"] = "";
	$fieldLabelsform2024["English"]["ContactZip"] = "Contact Zip";
	$fieldToolTipsform2024["English"]["ContactZip"] = "";
	$placeHoldersform2024["English"]["ContactZip"] = "";
	$fieldLabelsform2024["English"]["ContactPhone"] = "Contact Phone";
	$fieldToolTipsform2024["English"]["ContactPhone"] = "";
	$placeHoldersform2024["English"]["ContactPhone"] = "";
	$fieldLabelsform2024["English"]["ContactFax"] = "Contact Fax";
	$fieldToolTipsform2024["English"]["ContactFax"] = "";
	$placeHoldersform2024["English"]["ContactFax"] = "";
	$fieldLabelsform2024["English"]["ContactCell"] = "Contact Cell";
	$fieldToolTipsform2024["English"]["ContactCell"] = "";
	$placeHoldersform2024["English"]["ContactCell"] = "";
	$fieldLabelsform2024["English"]["DescriptionOfEntry"] = "Description Of Entry";
	$fieldToolTipsform2024["English"]["DescriptionOfEntry"] = "";
	$placeHoldersform2024["English"]["DescriptionOfEntry"] = "";
	$fieldLabelsform2024["English"]["UID"] = "UID";
	$fieldToolTipsform2024["English"]["UID"] = "";
	$placeHoldersform2024["English"]["UID"] = "";
	$fieldLabelsform2024["English"]["NumOfHorses"] = "Num Of Horses";
	$fieldToolTipsform2024["English"]["NumOfHorses"] = "";
	$placeHoldersform2024["English"]["NumOfHorses"] = "";
	$fieldLabelsform2024["English"]["Approved"] = "Approved";
	$fieldToolTipsform2024["English"]["Approved"] = "";
	$placeHoldersform2024["English"]["Approved"] = "";
	$fieldLabelsform2024["English"]["LowerThirds"] = "Lower Thirds";
	$fieldToolTipsform2024["English"]["LowerThirds"] = "";
	$placeHoldersform2024["English"]["LowerThirds"] = "";
	$fieldLabelsform2024["English"]["ParadeOrder"] = "Parade Order";
	$fieldToolTipsform2024["English"]["ParadeOrder"] = "";
	$placeHoldersform2024["English"]["ParadeOrder"] = "";
	$fieldLabelsform2024["English"]["GroupOrganizer"] = "Group Organizer";
	$fieldToolTipsform2024["English"]["GroupOrganizer"] = "";
	$placeHoldersform2024["English"]["GroupOrganizer"] = "";
	$fieldLabelsform2024["English"]["AgreetoRules"] = "Agreeto Rules";
	$fieldToolTipsform2024["English"]["AgreetoRules"] = "";
	$placeHoldersform2024["English"]["AgreetoRules"] = "";
	$fieldLabelsform2024["English"]["LiabilityWavierSigned"] = "Liability Wavier Signed";
	$fieldToolTipsform2024["English"]["LiabilityWavierSigned"] = "";
	$placeHoldersform2024["English"]["LiabilityWavierSigned"] = "";
	if (count($fieldToolTipsform2024["English"]))
		$tdataform2024[".isUseToolTips"] = true;
}


	$tdataform2024[".NCSearch"] = true;



$tdataform2024[".shortTableName"] = "form2024";
$tdataform2024[".nSecOptions"] = 0;

$tdataform2024[".mainTableOwnerID"] = "";
$tdataform2024[".entityType"] = 0;
$tdataform2024[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdataform2024[".strOriginalTableName"] = "Form2024";

		 



$tdataform2024[".showAddInPopup"] = false;

$tdataform2024[".showEditInPopup"] = false;

$tdataform2024[".showViewInPopup"] = false;

$tdataform2024[".listAjax"] = false;
//	temporary
//$tdataform2024[".listAjax"] = false;

	$tdataform2024[".audit"] = false;

	$tdataform2024[".locking"] = false;


$pages = $tdataform2024[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataform2024[".edit"] = true;
	$tdataform2024[".afterEditAction"] = 1;
	$tdataform2024[".closePopupAfterEdit"] = 1;
	$tdataform2024[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataform2024[".add"] = true;
$tdataform2024[".afterAddAction"] = 1;
$tdataform2024[".closePopupAfterAdd"] = 1;
$tdataform2024[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataform2024[".list"] = true;
}



$tdataform2024[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataform2024[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataform2024[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataform2024[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataform2024[".printFriendly"] = true;
}



$tdataform2024[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataform2024[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataform2024[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataform2024[".isUseAjaxSuggest"] = true;

$tdataform2024[".rowHighlite"] = true;



						

$tdataform2024[".ajaxCodeSnippetAdded"] = false;

$tdataform2024[".buttonsAdded"] = false;

$tdataform2024[".addPageEvents"] = false;

// use timepicker for search panel
$tdataform2024[".isUseTimeForSearch"] = false;


$tdataform2024[".badgeColor"] = "7B68EE";


$tdataform2024[".allSearchFields"] = array();
$tdataform2024[".filterFields"] = array();
$tdataform2024[".requiredSearchFields"] = array();

$tdataform2024[".googleLikeFields"] = array();
$tdataform2024[".googleLikeFields"][] = "ID";
$tdataform2024[".googleLikeFields"][] = "NameOfOrg";
$tdataform2024[".googleLikeFields"][] = "TypeOfOrg";
$tdataform2024[".googleLikeFields"][] = "SchoolClass";
$tdataform2024[".googleLikeFields"][] = "EntryType";
$tdataform2024[".googleLikeFields"][] = "KeyPeople";
$tdataform2024[".googleLikeFields"][] = "EntrySize";
$tdataform2024[".googleLikeFields"][] = "ContactName";
$tdataform2024[".googleLikeFields"][] = "ContactAddress";
$tdataform2024[".googleLikeFields"][] = "ContactEmail";
$tdataform2024[".googleLikeFields"][] = "ContactZip";
$tdataform2024[".googleLikeFields"][] = "ContactPhone";
$tdataform2024[".googleLikeFields"][] = "ContactFax";
$tdataform2024[".googleLikeFields"][] = "ContactCell";
$tdataform2024[".googleLikeFields"][] = "DescriptionOfEntry";
$tdataform2024[".googleLikeFields"][] = "UID";
$tdataform2024[".googleLikeFields"][] = "NumOfHorses";
$tdataform2024[".googleLikeFields"][] = "Approved";
$tdataform2024[".googleLikeFields"][] = "LowerThirds";
$tdataform2024[".googleLikeFields"][] = "ParadeOrder";
$tdataform2024[".googleLikeFields"][] = "GroupOrganizer";
$tdataform2024[".googleLikeFields"][] = "AgreetoRules";
$tdataform2024[".googleLikeFields"][] = "LiabilityWavierSigned";



$tdataform2024[".tableType"] = "list";

$tdataform2024[".printerPageOrientation"] = 0;
$tdataform2024[".nPrinterPageScale"] = 100;

$tdataform2024[".nPrinterSplitRecords"] = 40;

$tdataform2024[".geocodingEnabled"] = false;










$tdataform2024[".pageSize"] = 20;

$tdataform2024[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdataform2024[".strOrderBy"] = $tstrOrderBy;

$tdataform2024[".orderindexes"] = array();


$tdataform2024[".sqlHead"] = "SELECT ID,  	NameOfOrg,  	TypeOfOrg,  	SchoolClass,  	EntryType,  	KeyPeople,  	EntrySize,  	ContactName,  	ContactAddress,  	ContactEmail,  	ContactZip,  	ContactPhone,  	ContactFax,  	ContactCell,  	DescriptionOfEntry,  	`UID`,  	NumOfHorses,  	Approved,  	LowerThirds,  	ParadeOrder,  	GroupOrganizer,  	AgreetoRules,  	LiabilityWavierSigned";
$tdataform2024[".sqlFrom"] = "FROM Form2024";
$tdataform2024[".sqlWhereExpr"] = "";
$tdataform2024[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataform2024[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataform2024[".arrGroupsPerPage"] = $arrGPP;

$tdataform2024[".highlightSearchResults"] = true;

$tableKeysform2024 = array();
$tableKeysform2024[] = "ID";
$tdataform2024[".Keys"] = $tableKeysform2024;


$tdataform2024[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

		$fdata["sourceSingle"] = "ID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["ID"] = $fdata;
		$tdataform2024[".searchableFields"][] = "ID";
//	NameOfOrg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "NameOfOrg";
	$fdata["GoodName"] = "NameOfOrg";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","NameOfOrg");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "NameOfOrg";

		$fdata["sourceSingle"] = "NameOfOrg";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NameOfOrg";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["NameOfOrg"] = $fdata;
		$tdataform2024[".searchableFields"][] = "NameOfOrg";
//	TypeOfOrg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "TypeOfOrg";
	$fdata["GoodName"] = "TypeOfOrg";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","TypeOfOrg");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "TypeOfOrg";

		$fdata["sourceSingle"] = "TypeOfOrg";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "TypeOfOrg";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["TypeOfOrg"] = $fdata;
		$tdataform2024[".searchableFields"][] = "TypeOfOrg";
//	SchoolClass
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "SchoolClass";
	$fdata["GoodName"] = "SchoolClass";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","SchoolClass");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "SchoolClass";

		$fdata["sourceSingle"] = "SchoolClass";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "SchoolClass";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["SchoolClass"] = $fdata;
		$tdataform2024[".searchableFields"][] = "SchoolClass";
//	EntryType
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "EntryType";
	$fdata["GoodName"] = "EntryType";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","EntryType");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "EntryType";

		$fdata["sourceSingle"] = "EntryType";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "EntryType";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["EntryType"] = $fdata;
		$tdataform2024[".searchableFields"][] = "EntryType";
//	KeyPeople
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "KeyPeople";
	$fdata["GoodName"] = "KeyPeople";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","KeyPeople");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "KeyPeople";

		$fdata["sourceSingle"] = "KeyPeople";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "KeyPeople";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["KeyPeople"] = $fdata;
		$tdataform2024[".searchableFields"][] = "KeyPeople";
//	EntrySize
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "EntrySize";
	$fdata["GoodName"] = "EntrySize";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","EntrySize");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "EntrySize";

		$fdata["sourceSingle"] = "EntrySize";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "EntrySize";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["EntrySize"] = $fdata;
		$tdataform2024[".searchableFields"][] = "EntrySize";
//	ContactName
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "ContactName";
	$fdata["GoodName"] = "ContactName";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","ContactName");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactName";

		$fdata["sourceSingle"] = "ContactName";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ContactName";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=200";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["ContactName"] = $fdata;
		$tdataform2024[".searchableFields"][] = "ContactName";
//	ContactAddress
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "ContactAddress";
	$fdata["GoodName"] = "ContactAddress";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","ContactAddress");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactAddress";

		$fdata["sourceSingle"] = "ContactAddress";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ContactAddress";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["ContactAddress"] = $fdata;
		$tdataform2024[".searchableFields"][] = "ContactAddress";
//	ContactEmail
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "ContactEmail";
	$fdata["GoodName"] = "ContactEmail";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","ContactEmail");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactEmail";

		$fdata["sourceSingle"] = "ContactEmail";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ContactEmail";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["ContactEmail"] = $fdata;
		$tdataform2024[".searchableFields"][] = "ContactEmail";
//	ContactZip
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "ContactZip";
	$fdata["GoodName"] = "ContactZip";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","ContactZip");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactZip";

		$fdata["sourceSingle"] = "ContactZip";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ContactZip";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["ContactZip"] = $fdata;
		$tdataform2024[".searchableFields"][] = "ContactZip";
//	ContactPhone
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "ContactPhone";
	$fdata["GoodName"] = "ContactPhone";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","ContactPhone");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactPhone";

		$fdata["sourceSingle"] = "ContactPhone";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ContactPhone";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["ContactPhone"] = $fdata;
		$tdataform2024[".searchableFields"][] = "ContactPhone";
//	ContactFax
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "ContactFax";
	$fdata["GoodName"] = "ContactFax";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","ContactFax");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactFax";

		$fdata["sourceSingle"] = "ContactFax";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ContactFax";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["ContactFax"] = $fdata;
		$tdataform2024[".searchableFields"][] = "ContactFax";
//	ContactCell
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "ContactCell";
	$fdata["GoodName"] = "ContactCell";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","ContactCell");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactCell";

		$fdata["sourceSingle"] = "ContactCell";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ContactCell";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["ContactCell"] = $fdata;
		$tdataform2024[".searchableFields"][] = "ContactCell";
//	DescriptionOfEntry
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "DescriptionOfEntry";
	$fdata["GoodName"] = "DescriptionOfEntry";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","DescriptionOfEntry");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "DescriptionOfEntry";

		$fdata["sourceSingle"] = "DescriptionOfEntry";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "DescriptionOfEntry";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["DescriptionOfEntry"] = $fdata;
		$tdataform2024[".searchableFields"][] = "DescriptionOfEntry";
//	UID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "UID";
	$fdata["GoodName"] = "UID";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","UID");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "UID";

		$fdata["sourceSingle"] = "UID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`UID`";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["UID"] = $fdata;
		$tdataform2024[".searchableFields"][] = "UID";
//	NumOfHorses
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "NumOfHorses";
	$fdata["GoodName"] = "NumOfHorses";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","NumOfHorses");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "NumOfHorses";

		$fdata["sourceSingle"] = "NumOfHorses";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NumOfHorses";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["NumOfHorses"] = $fdata;
		$tdataform2024[".searchableFields"][] = "NumOfHorses";
//	Approved
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "Approved";
	$fdata["GoodName"] = "Approved";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","Approved");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "Approved";

		$fdata["sourceSingle"] = "Approved";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Approved";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["Approved"] = $fdata;
		$tdataform2024[".searchableFields"][] = "Approved";
//	LowerThirds
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "LowerThirds";
	$fdata["GoodName"] = "LowerThirds";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","LowerThirds");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "LowerThirds";

		$fdata["sourceSingle"] = "LowerThirds";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "LowerThirds";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["LowerThirds"] = $fdata;
		$tdataform2024[".searchableFields"][] = "LowerThirds";
//	ParadeOrder
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "ParadeOrder";
	$fdata["GoodName"] = "ParadeOrder";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","ParadeOrder");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ParadeOrder";

		$fdata["sourceSingle"] = "ParadeOrder";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ParadeOrder";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["ParadeOrder"] = $fdata;
		$tdataform2024[".searchableFields"][] = "ParadeOrder";
//	GroupOrganizer
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "GroupOrganizer";
	$fdata["GoodName"] = "GroupOrganizer";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","GroupOrganizer");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "GroupOrganizer";

		$fdata["sourceSingle"] = "GroupOrganizer";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "GroupOrganizer";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["GroupOrganizer"] = $fdata;
		$tdataform2024[".searchableFields"][] = "GroupOrganizer";
//	AgreetoRules
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 22;
	$fdata["strName"] = "AgreetoRules";
	$fdata["GoodName"] = "AgreetoRules";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","AgreetoRules");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "AgreetoRules";

		$fdata["sourceSingle"] = "AgreetoRules";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "AgreetoRules";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["AgreetoRules"] = $fdata;
		$tdataform2024[".searchableFields"][] = "AgreetoRules";
//	LiabilityWavierSigned
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 23;
	$fdata["strName"] = "LiabilityWavierSigned";
	$fdata["GoodName"] = "LiabilityWavierSigned";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("Form2024","LiabilityWavierSigned");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "LiabilityWavierSigned";

		$fdata["sourceSingle"] = "LiabilityWavierSigned";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "LiabilityWavierSigned";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform2024["LiabilityWavierSigned"] = $fdata;
		$tdataform2024[".searchableFields"][] = "LiabilityWavierSigned";


$tables_data["Form2024"]=&$tdataform2024;
$field_labels["Form2024"] = &$fieldLabelsform2024;
$fieldToolTips["Form2024"] = &$fieldToolTipsform2024;
$placeHolders["Form2024"] = &$placeHoldersform2024;
$page_titles["Form2024"] = &$pageTitlesform2024;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["Form2024"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["Form2024"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_form2024()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	NameOfOrg,  	TypeOfOrg,  	SchoolClass,  	EntryType,  	KeyPeople,  	EntrySize,  	ContactName,  	ContactAddress,  	ContactEmail,  	ContactZip,  	ContactPhone,  	ContactFax,  	ContactCell,  	DescriptionOfEntry,  	`UID`,  	NumOfHorses,  	Approved,  	LowerThirds,  	ParadeOrder,  	GroupOrganizer,  	AgreetoRules,  	LiabilityWavierSigned";
$proto0["m_strFrom"] = "FROM Form2024";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "Form2024";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "NameOfOrg",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto8["m_sql"] = "NameOfOrg";
$proto8["m_srcTableName"] = "Form2024";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "TypeOfOrg",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto10["m_sql"] = "TypeOfOrg";
$proto10["m_srcTableName"] = "Form2024";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "SchoolClass",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto12["m_sql"] = "SchoolClass";
$proto12["m_srcTableName"] = "Form2024";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "EntryType",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto14["m_sql"] = "EntryType";
$proto14["m_srcTableName"] = "Form2024";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "KeyPeople",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto16["m_sql"] = "KeyPeople";
$proto16["m_srcTableName"] = "Form2024";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "EntrySize",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto18["m_sql"] = "EntrySize";
$proto18["m_srcTableName"] = "Form2024";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactName",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto20["m_sql"] = "ContactName";
$proto20["m_srcTableName"] = "Form2024";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactAddress",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto22["m_sql"] = "ContactAddress";
$proto22["m_srcTableName"] = "Form2024";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactEmail",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto24["m_sql"] = "ContactEmail";
$proto24["m_srcTableName"] = "Form2024";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactZip",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto26["m_sql"] = "ContactZip";
$proto26["m_srcTableName"] = "Form2024";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactPhone",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto28["m_sql"] = "ContactPhone";
$proto28["m_srcTableName"] = "Form2024";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactFax",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto30["m_sql"] = "ContactFax";
$proto30["m_srcTableName"] = "Form2024";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactCell",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto32["m_sql"] = "ContactCell";
$proto32["m_srcTableName"] = "Form2024";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "DescriptionOfEntry",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto34["m_sql"] = "DescriptionOfEntry";
$proto34["m_srcTableName"] = "Form2024";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "UID",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto36["m_sql"] = "`UID`";
$proto36["m_srcTableName"] = "Form2024";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "NumOfHorses",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto38["m_sql"] = "NumOfHorses";
$proto38["m_srcTableName"] = "Form2024";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "Approved",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto40["m_sql"] = "Approved";
$proto40["m_srcTableName"] = "Form2024";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "LowerThirds",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto42["m_sql"] = "LowerThirds";
$proto42["m_srcTableName"] = "Form2024";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "ParadeOrder",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto44["m_sql"] = "ParadeOrder";
$proto44["m_srcTableName"] = "Form2024";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$obj = new SQLField(array(
	"m_strName" => "GroupOrganizer",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto46["m_sql"] = "GroupOrganizer";
$proto46["m_srcTableName"] = "Form2024";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
						$proto48=array();
			$obj = new SQLField(array(
	"m_strName" => "AgreetoRules",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto48["m_sql"] = "AgreetoRules";
$proto48["m_srcTableName"] = "Form2024";
$proto48["m_expr"]=$obj;
$proto48["m_alias"] = "";
$obj = new SQLFieldListItem($proto48);

$proto0["m_fieldlist"][]=$obj;
						$proto50=array();
			$obj = new SQLField(array(
	"m_strName" => "LiabilityWavierSigned",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "Form2024"
));

$proto50["m_sql"] = "LiabilityWavierSigned";
$proto50["m_srcTableName"] = "Form2024";
$proto50["m_expr"]=$obj;
$proto50["m_alias"] = "";
$obj = new SQLFieldListItem($proto50);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto52=array();
$proto52["m_link"] = "SQLL_MAIN";
			$proto53=array();
$proto53["m_strName"] = "Form2024";
$proto53["m_srcTableName"] = "Form2024";
$proto53["m_columns"] = array();
$proto53["m_columns"][] = "ID";
$proto53["m_columns"][] = "NameOfOrg";
$proto53["m_columns"][] = "TypeOfOrg";
$proto53["m_columns"][] = "SchoolClass";
$proto53["m_columns"][] = "EntryType";
$proto53["m_columns"][] = "KeyPeople";
$proto53["m_columns"][] = "EntrySize";
$proto53["m_columns"][] = "ContactName";
$proto53["m_columns"][] = "ContactAddress";
$proto53["m_columns"][] = "ContactEmail";
$proto53["m_columns"][] = "ContactZip";
$proto53["m_columns"][] = "ContactPhone";
$proto53["m_columns"][] = "ContactFax";
$proto53["m_columns"][] = "ContactCell";
$proto53["m_columns"][] = "DescriptionOfEntry";
$proto53["m_columns"][] = "UID";
$proto53["m_columns"][] = "NumOfHorses";
$proto53["m_columns"][] = "Approved";
$proto53["m_columns"][] = "LowerThirds";
$proto53["m_columns"][] = "ParadeOrder";
$proto53["m_columns"][] = "GroupOrganizer";
$proto53["m_columns"][] = "AgreetoRules";
$proto53["m_columns"][] = "LiabilityWavierSigned";
$obj = new SQLTable($proto53);

$proto52["m_table"] = $obj;
$proto52["m_sql"] = "Form2024";
$proto52["m_alias"] = "";
$proto52["m_srcTableName"] = "Form2024";
$proto54=array();
$proto54["m_sql"] = "";
$proto54["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto54["m_column"]=$obj;
$proto54["m_contained"] = array();
$proto54["m_strCase"] = "";
$proto54["m_havingmode"] = false;
$proto54["m_inBrackets"] = false;
$proto54["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto54);

$proto52["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto52);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="Form2024";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_form2024 = createSqlQuery_form2024();


	
					
;

																							

$tdataform2024[".sqlquery"] = $queryData_form2024;



$tableEvents["Form2024"] = new eventsBase;
$tdataform2024[".hasEvents"] = false;

?>