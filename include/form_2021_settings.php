<?php
$tdataform_2021 = array();
$tdataform_2021[".searchableFields"] = array();
$tdataform_2021[".ShortName"] = "form_2021";
$tdataform_2021[".OwnerID"] = "";
$tdataform_2021[".OriginalTable"] = "Form_2021";


$tdataform_2021[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataform_2021[".originalPagesByType"] = $tdataform_2021[".pagesByType"];
$tdataform_2021[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataform_2021[".originalPages"] = $tdataform_2021[".pages"];
$tdataform_2021[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataform_2021[".originalDefaultPages"] = $tdataform_2021[".defaultPages"];

//	field labels
$fieldLabelsform_2021 = array();
$fieldToolTipsform_2021 = array();
$pageTitlesform_2021 = array();
$placeHoldersform_2021 = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsform_2021["English"] = array();
	$fieldToolTipsform_2021["English"] = array();
	$placeHoldersform_2021["English"] = array();
	$pageTitlesform_2021["English"] = array();
	$fieldLabelsform_2021["English"]["Approved"] = "Approved";
	$fieldToolTipsform_2021["English"]["Approved"] = "";
	$placeHoldersform_2021["English"]["Approved"] = "";
	$fieldLabelsform_2021["English"]["ContactAddress"] = "Contact Address";
	$fieldToolTipsform_2021["English"]["ContactAddress"] = "";
	$placeHoldersform_2021["English"]["ContactAddress"] = "";
	$fieldLabelsform_2021["English"]["ContactCell"] = "Contact Cell";
	$fieldToolTipsform_2021["English"]["ContactCell"] = "";
	$placeHoldersform_2021["English"]["ContactCell"] = "";
	$fieldLabelsform_2021["English"]["ContactEmail"] = "Contact Email";
	$fieldToolTipsform_2021["English"]["ContactEmail"] = "";
	$placeHoldersform_2021["English"]["ContactEmail"] = "";
	$fieldLabelsform_2021["English"]["ContactFax"] = "Contact Fax";
	$fieldToolTipsform_2021["English"]["ContactFax"] = "";
	$placeHoldersform_2021["English"]["ContactFax"] = "";
	$fieldLabelsform_2021["English"]["ContactName"] = "Contact Name";
	$fieldToolTipsform_2021["English"]["ContactName"] = "";
	$placeHoldersform_2021["English"]["ContactName"] = "";
	$fieldLabelsform_2021["English"]["ContactPhone"] = "Contact Phone";
	$fieldToolTipsform_2021["English"]["ContactPhone"] = "";
	$placeHoldersform_2021["English"]["ContactPhone"] = "";
	$fieldLabelsform_2021["English"]["ContactZip"] = "Contact Zip";
	$fieldToolTipsform_2021["English"]["ContactZip"] = "";
	$placeHoldersform_2021["English"]["ContactZip"] = "";
	$fieldLabelsform_2021["English"]["DescriptionOfEntry"] = "Description Of Entry";
	$fieldToolTipsform_2021["English"]["DescriptionOfEntry"] = "";
	$placeHoldersform_2021["English"]["DescriptionOfEntry"] = "";
	$fieldLabelsform_2021["English"]["EntrySize"] = "Entry Size";
	$fieldToolTipsform_2021["English"]["EntrySize"] = "";
	$placeHoldersform_2021["English"]["EntrySize"] = "";
	$fieldLabelsform_2021["English"]["EntryType"] = "Entry Type";
	$fieldToolTipsform_2021["English"]["EntryType"] = "";
	$placeHoldersform_2021["English"]["EntryType"] = "";
	$fieldLabelsform_2021["English"]["GroupOrganizer"] = "Group Organizer";
	$fieldToolTipsform_2021["English"]["GroupOrganizer"] = "";
	$placeHoldersform_2021["English"]["GroupOrganizer"] = "";
	$fieldLabelsform_2021["English"]["ID"] = "ID";
	$fieldToolTipsform_2021["English"]["ID"] = "";
	$placeHoldersform_2021["English"]["ID"] = "";
	$fieldLabelsform_2021["English"]["KeyPeople"] = "Key People";
	$fieldToolTipsform_2021["English"]["KeyPeople"] = "";
	$placeHoldersform_2021["English"]["KeyPeople"] = "";
	$fieldLabelsform_2021["English"]["LowerThirds"] = "Lower Thirds";
	$fieldToolTipsform_2021["English"]["LowerThirds"] = "";
	$placeHoldersform_2021["English"]["LowerThirds"] = "";
	$fieldLabelsform_2021["English"]["NameOfOrg"] = "Name Of Org";
	$fieldToolTipsform_2021["English"]["NameOfOrg"] = "";
	$placeHoldersform_2021["English"]["NameOfOrg"] = "";
	$fieldLabelsform_2021["English"]["NumOfHorses"] = "Num Of Horses";
	$fieldToolTipsform_2021["English"]["NumOfHorses"] = "";
	$placeHoldersform_2021["English"]["NumOfHorses"] = "";
	$fieldLabelsform_2021["English"]["ParadeOrder"] = "Parade Order";
	$fieldToolTipsform_2021["English"]["ParadeOrder"] = "";
	$placeHoldersform_2021["English"]["ParadeOrder"] = "";
	$fieldLabelsform_2021["English"]["SchoolClass"] = "School Class";
	$fieldToolTipsform_2021["English"]["SchoolClass"] = "";
	$placeHoldersform_2021["English"]["SchoolClass"] = "";
	$fieldLabelsform_2021["English"]["TypeOfOrg"] = "Type Of Org";
	$fieldToolTipsform_2021["English"]["TypeOfOrg"] = "";
	$placeHoldersform_2021["English"]["TypeOfOrg"] = "";
	$fieldLabelsform_2021["English"]["UID"] = "UID";
	$fieldToolTipsform_2021["English"]["UID"] = "";
	$placeHoldersform_2021["English"]["UID"] = "";
	if (count($fieldToolTipsform_2021["English"]))
		$tdataform_2021[".isUseToolTips"] = true;
}


	$tdataform_2021[".NCSearch"] = true;



$tdataform_2021[".shortTableName"] = "form_2021";
$tdataform_2021[".nSecOptions"] = 0;

$tdataform_2021[".mainTableOwnerID"] = "";
$tdataform_2021[".entityType"] = 0;
$tdataform_2021[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdataform_2021[".strOriginalTableName"] = "Form_2021";

		 



$tdataform_2021[".showAddInPopup"] = false;

$tdataform_2021[".showEditInPopup"] = false;

$tdataform_2021[".showViewInPopup"] = false;

$tdataform_2021[".listAjax"] = false;
//	temporary
//$tdataform_2021[".listAjax"] = false;

	$tdataform_2021[".audit"] = false;

	$tdataform_2021[".locking"] = false;


$pages = $tdataform_2021[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataform_2021[".edit"] = true;
	$tdataform_2021[".afterEditAction"] = 1;
	$tdataform_2021[".closePopupAfterEdit"] = 1;
	$tdataform_2021[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataform_2021[".add"] = true;
$tdataform_2021[".afterAddAction"] = 1;
$tdataform_2021[".closePopupAfterAdd"] = 1;
$tdataform_2021[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataform_2021[".list"] = true;
}



$tdataform_2021[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataform_2021[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataform_2021[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataform_2021[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataform_2021[".printFriendly"] = true;
}



$tdataform_2021[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataform_2021[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataform_2021[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataform_2021[".isUseAjaxSuggest"] = true;

$tdataform_2021[".rowHighlite"] = true;



						

$tdataform_2021[".ajaxCodeSnippetAdded"] = false;

$tdataform_2021[".buttonsAdded"] = false;

$tdataform_2021[".addPageEvents"] = false;

// use timepicker for search panel
$tdataform_2021[".isUseTimeForSearch"] = false;


$tdataform_2021[".badgeColor"] = "E67349";


$tdataform_2021[".allSearchFields"] = array();
$tdataform_2021[".filterFields"] = array();
$tdataform_2021[".requiredSearchFields"] = array();

$tdataform_2021[".googleLikeFields"] = array();
$tdataform_2021[".googleLikeFields"][] = "ID";
$tdataform_2021[".googleLikeFields"][] = "NameOfOrg";
$tdataform_2021[".googleLikeFields"][] = "TypeOfOrg";
$tdataform_2021[".googleLikeFields"][] = "SchoolClass";
$tdataform_2021[".googleLikeFields"][] = "EntryType";
$tdataform_2021[".googleLikeFields"][] = "KeyPeople";
$tdataform_2021[".googleLikeFields"][] = "EntrySize";
$tdataform_2021[".googleLikeFields"][] = "ContactName";
$tdataform_2021[".googleLikeFields"][] = "ContactAddress";
$tdataform_2021[".googleLikeFields"][] = "ContactEmail";
$tdataform_2021[".googleLikeFields"][] = "ContactZip";
$tdataform_2021[".googleLikeFields"][] = "ContactPhone";
$tdataform_2021[".googleLikeFields"][] = "ContactFax";
$tdataform_2021[".googleLikeFields"][] = "ContactCell";
$tdataform_2021[".googleLikeFields"][] = "DescriptionOfEntry";
$tdataform_2021[".googleLikeFields"][] = "UID";
$tdataform_2021[".googleLikeFields"][] = "NumOfHorses";
$tdataform_2021[".googleLikeFields"][] = "Approved";
$tdataform_2021[".googleLikeFields"][] = "LowerThirds";
$tdataform_2021[".googleLikeFields"][] = "ParadeOrder";
$tdataform_2021[".googleLikeFields"][] = "GroupOrganizer";



$tdataform_2021[".tableType"] = "list";

$tdataform_2021[".printerPageOrientation"] = 0;
$tdataform_2021[".nPrinterPageScale"] = 100;

$tdataform_2021[".nPrinterSplitRecords"] = 40;

$tdataform_2021[".geocodingEnabled"] = false;










$tdataform_2021[".pageSize"] = 20;

$tdataform_2021[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdataform_2021[".strOrderBy"] = $tstrOrderBy;

$tdataform_2021[".orderindexes"] = array();


$tdataform_2021[".sqlHead"] = "SELECT ID,  	NameOfOrg,  	TypeOfOrg,  	SchoolClass,  	EntryType,  	KeyPeople,  	EntrySize,  	ContactName,  	ContactAddress,  	ContactEmail,  	ContactZip,  	ContactPhone,  	ContactFax,  	ContactCell,  	DescriptionOfEntry,  	`UID`,  	NumOfHorses,  	Approved,  	LowerThirds,  	ParadeOrder,  	GroupOrganizer";
$tdataform_2021[".sqlFrom"] = "FROM Form_2021";
$tdataform_2021[".sqlWhereExpr"] = "";
$tdataform_2021[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataform_2021[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataform_2021[".arrGroupsPerPage"] = $arrGPP;

$tdataform_2021[".highlightSearchResults"] = true;

$tableKeysform_2021 = array();
$tableKeysform_2021[] = "ID";
$tdataform_2021[".Keys"] = $tableKeysform_2021;


$tdataform_2021[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

		$fdata["sourceSingle"] = "ID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["ID"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "ID";
//	NameOfOrg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "NameOfOrg";
	$fdata["GoodName"] = "NameOfOrg";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","NameOfOrg");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "NameOfOrg";

		$fdata["sourceSingle"] = "NameOfOrg";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NameOfOrg";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["NameOfOrg"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "NameOfOrg";
//	TypeOfOrg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "TypeOfOrg";
	$fdata["GoodName"] = "TypeOfOrg";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","TypeOfOrg");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "TypeOfOrg";

		$fdata["sourceSingle"] = "TypeOfOrg";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "TypeOfOrg";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["TypeOfOrg"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "TypeOfOrg";
//	SchoolClass
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "SchoolClass";
	$fdata["GoodName"] = "SchoolClass";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","SchoolClass");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "SchoolClass";

		$fdata["sourceSingle"] = "SchoolClass";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "SchoolClass";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["SchoolClass"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "SchoolClass";
//	EntryType
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "EntryType";
	$fdata["GoodName"] = "EntryType";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","EntryType");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "EntryType";

		$fdata["sourceSingle"] = "EntryType";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "EntryType";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["EntryType"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "EntryType";
//	KeyPeople
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "KeyPeople";
	$fdata["GoodName"] = "KeyPeople";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","KeyPeople");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "KeyPeople";

		$fdata["sourceSingle"] = "KeyPeople";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "KeyPeople";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["KeyPeople"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "KeyPeople";
//	EntrySize
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "EntrySize";
	$fdata["GoodName"] = "EntrySize";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","EntrySize");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "EntrySize";

		$fdata["sourceSingle"] = "EntrySize";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "EntrySize";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["EntrySize"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "EntrySize";
//	ContactName
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "ContactName";
	$fdata["GoodName"] = "ContactName";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","ContactName");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactName";

		$fdata["sourceSingle"] = "ContactName";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ContactName";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=200";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["ContactName"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "ContactName";
//	ContactAddress
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "ContactAddress";
	$fdata["GoodName"] = "ContactAddress";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","ContactAddress");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactAddress";

		$fdata["sourceSingle"] = "ContactAddress";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ContactAddress";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["ContactAddress"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "ContactAddress";
//	ContactEmail
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "ContactEmail";
	$fdata["GoodName"] = "ContactEmail";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","ContactEmail");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactEmail";

		$fdata["sourceSingle"] = "ContactEmail";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ContactEmail";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["ContactEmail"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "ContactEmail";
//	ContactZip
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "ContactZip";
	$fdata["GoodName"] = "ContactZip";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","ContactZip");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactZip";

		$fdata["sourceSingle"] = "ContactZip";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ContactZip";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["ContactZip"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "ContactZip";
//	ContactPhone
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "ContactPhone";
	$fdata["GoodName"] = "ContactPhone";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","ContactPhone");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactPhone";

		$fdata["sourceSingle"] = "ContactPhone";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ContactPhone";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["ContactPhone"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "ContactPhone";
//	ContactFax
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "ContactFax";
	$fdata["GoodName"] = "ContactFax";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","ContactFax");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactFax";

		$fdata["sourceSingle"] = "ContactFax";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ContactFax";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["ContactFax"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "ContactFax";
//	ContactCell
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "ContactCell";
	$fdata["GoodName"] = "ContactCell";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","ContactCell");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactCell";

		$fdata["sourceSingle"] = "ContactCell";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ContactCell";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["ContactCell"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "ContactCell";
//	DescriptionOfEntry
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "DescriptionOfEntry";
	$fdata["GoodName"] = "DescriptionOfEntry";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","DescriptionOfEntry");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "DescriptionOfEntry";

		$fdata["sourceSingle"] = "DescriptionOfEntry";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "DescriptionOfEntry";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["DescriptionOfEntry"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "DescriptionOfEntry";
//	UID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "UID";
	$fdata["GoodName"] = "UID";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","UID");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "UID";

		$fdata["sourceSingle"] = "UID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`UID`";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["UID"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "UID";
//	NumOfHorses
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "NumOfHorses";
	$fdata["GoodName"] = "NumOfHorses";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","NumOfHorses");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "NumOfHorses";

		$fdata["sourceSingle"] = "NumOfHorses";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NumOfHorses";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["NumOfHorses"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "NumOfHorses";
//	Approved
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "Approved";
	$fdata["GoodName"] = "Approved";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","Approved");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "Approved";

		$fdata["sourceSingle"] = "Approved";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Approved";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["Approved"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "Approved";
//	LowerThirds
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "LowerThirds";
	$fdata["GoodName"] = "LowerThirds";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","LowerThirds");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "LowerThirds";

		$fdata["sourceSingle"] = "LowerThirds";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "LowerThirds";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["LowerThirds"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "LowerThirds";
//	ParadeOrder
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "ParadeOrder";
	$fdata["GoodName"] = "ParadeOrder";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","ParadeOrder");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ParadeOrder";

		$fdata["sourceSingle"] = "ParadeOrder";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ParadeOrder";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["ParadeOrder"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "ParadeOrder";
//	GroupOrganizer
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "GroupOrganizer";
	$fdata["GoodName"] = "GroupOrganizer";
	$fdata["ownerTable"] = "Form_2021";
	$fdata["Label"] = GetFieldLabel("Form_2021","GroupOrganizer");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "GroupOrganizer";

		$fdata["sourceSingle"] = "GroupOrganizer";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "GroupOrganizer";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataform_2021["GroupOrganizer"] = $fdata;
		$tdataform_2021[".searchableFields"][] = "GroupOrganizer";


$tables_data["Form_2021"]=&$tdataform_2021;
$field_labels["Form_2021"] = &$fieldLabelsform_2021;
$fieldToolTips["Form_2021"] = &$fieldToolTipsform_2021;
$placeHolders["Form_2021"] = &$placeHoldersform_2021;
$page_titles["Form_2021"] = &$pageTitlesform_2021;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["Form_2021"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["Form_2021"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_form_2021()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	NameOfOrg,  	TypeOfOrg,  	SchoolClass,  	EntryType,  	KeyPeople,  	EntrySize,  	ContactName,  	ContactAddress,  	ContactEmail,  	ContactZip,  	ContactPhone,  	ContactFax,  	ContactCell,  	DescriptionOfEntry,  	`UID`,  	NumOfHorses,  	Approved,  	LowerThirds,  	ParadeOrder,  	GroupOrganizer";
$proto0["m_strFrom"] = "FROM Form_2021";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "Form_2021";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "NameOfOrg",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto8["m_sql"] = "NameOfOrg";
$proto8["m_srcTableName"] = "Form_2021";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "TypeOfOrg",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto10["m_sql"] = "TypeOfOrg";
$proto10["m_srcTableName"] = "Form_2021";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "SchoolClass",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto12["m_sql"] = "SchoolClass";
$proto12["m_srcTableName"] = "Form_2021";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "EntryType",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto14["m_sql"] = "EntryType";
$proto14["m_srcTableName"] = "Form_2021";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "KeyPeople",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto16["m_sql"] = "KeyPeople";
$proto16["m_srcTableName"] = "Form_2021";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "EntrySize",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto18["m_sql"] = "EntrySize";
$proto18["m_srcTableName"] = "Form_2021";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactName",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto20["m_sql"] = "ContactName";
$proto20["m_srcTableName"] = "Form_2021";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactAddress",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto22["m_sql"] = "ContactAddress";
$proto22["m_srcTableName"] = "Form_2021";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactEmail",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto24["m_sql"] = "ContactEmail";
$proto24["m_srcTableName"] = "Form_2021";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactZip",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto26["m_sql"] = "ContactZip";
$proto26["m_srcTableName"] = "Form_2021";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactPhone",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto28["m_sql"] = "ContactPhone";
$proto28["m_srcTableName"] = "Form_2021";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactFax",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto30["m_sql"] = "ContactFax";
$proto30["m_srcTableName"] = "Form_2021";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactCell",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto32["m_sql"] = "ContactCell";
$proto32["m_srcTableName"] = "Form_2021";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "DescriptionOfEntry",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto34["m_sql"] = "DescriptionOfEntry";
$proto34["m_srcTableName"] = "Form_2021";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "UID",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto36["m_sql"] = "`UID`";
$proto36["m_srcTableName"] = "Form_2021";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "NumOfHorses",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto38["m_sql"] = "NumOfHorses";
$proto38["m_srcTableName"] = "Form_2021";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "Approved",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto40["m_sql"] = "Approved";
$proto40["m_srcTableName"] = "Form_2021";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "LowerThirds",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto42["m_sql"] = "LowerThirds";
$proto42["m_srcTableName"] = "Form_2021";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "ParadeOrder",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto44["m_sql"] = "ParadeOrder";
$proto44["m_srcTableName"] = "Form_2021";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$obj = new SQLField(array(
	"m_strName" => "GroupOrganizer",
	"m_strTable" => "Form_2021",
	"m_srcTableName" => "Form_2021"
));

$proto46["m_sql"] = "GroupOrganizer";
$proto46["m_srcTableName"] = "Form_2021";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto48=array();
$proto48["m_link"] = "SQLL_MAIN";
			$proto49=array();
$proto49["m_strName"] = "Form_2021";
$proto49["m_srcTableName"] = "Form_2021";
$proto49["m_columns"] = array();
$proto49["m_columns"][] = "ID";
$proto49["m_columns"][] = "NameOfOrg";
$proto49["m_columns"][] = "TypeOfOrg";
$proto49["m_columns"][] = "SchoolClass";
$proto49["m_columns"][] = "EntryType";
$proto49["m_columns"][] = "KeyPeople";
$proto49["m_columns"][] = "EntrySize";
$proto49["m_columns"][] = "ContactName";
$proto49["m_columns"][] = "ContactAddress";
$proto49["m_columns"][] = "ContactEmail";
$proto49["m_columns"][] = "ContactZip";
$proto49["m_columns"][] = "ContactPhone";
$proto49["m_columns"][] = "ContactFax";
$proto49["m_columns"][] = "ContactCell";
$proto49["m_columns"][] = "DescriptionOfEntry";
$proto49["m_columns"][] = "UID";
$proto49["m_columns"][] = "NumOfHorses";
$proto49["m_columns"][] = "Approved";
$proto49["m_columns"][] = "LowerThirds";
$proto49["m_columns"][] = "ParadeOrder";
$proto49["m_columns"][] = "GroupOrganizer";
$obj = new SQLTable($proto49);

$proto48["m_table"] = $obj;
$proto48["m_sql"] = "Form_2021";
$proto48["m_alias"] = "";
$proto48["m_srcTableName"] = "Form_2021";
$proto50=array();
$proto50["m_sql"] = "";
$proto50["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto50["m_column"]=$obj;
$proto50["m_contained"] = array();
$proto50["m_strCase"] = "";
$proto50["m_havingmode"] = false;
$proto50["m_inBrackets"] = false;
$proto50["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto50);

$proto48["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto48);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="Form_2021";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_form_2021 = createSqlQuery_form_2021();


	
					
;

																					

$tdataform_2021[".sqlquery"] = $queryData_form_2021;



$tableEvents["Form_2021"] = new eventsBase;
$tdataform_2021[".hasEvents"] = false;

?>