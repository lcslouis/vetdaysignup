<?php
$tdataparadesignup_users1 = array();
$tdataparadesignup_users1[".searchableFields"] = array();
$tdataparadesignup_users1[".ShortName"] = "paradesignup_users1";
$tdataparadesignup_users1[".OwnerID"] = "";
$tdataparadesignup_users1[".OriginalTable"] = "ParadeSignup_users1";


$tdataparadesignup_users1[".pagesByType"] = my_json_decode( "{\"edit\":[\"edit\"],\"list\":[\"list\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataparadesignup_users1[".originalPagesByType"] = $tdataparadesignup_users1[".pagesByType"];
$tdataparadesignup_users1[".pages"] = types2pages( my_json_decode( "{\"edit\":[\"edit\"],\"list\":[\"list\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataparadesignup_users1[".originalPages"] = $tdataparadesignup_users1[".pages"];
$tdataparadesignup_users1[".defaultPages"] = my_json_decode( "{\"edit\":\"edit\",\"list\":\"list\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataparadesignup_users1[".originalDefaultPages"] = $tdataparadesignup_users1[".defaultPages"];

//	field labels
$fieldLabelsparadesignup_users1 = array();
$fieldToolTipsparadesignup_users1 = array();
$pageTitlesparadesignup_users1 = array();
$placeHoldersparadesignup_users1 = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsparadesignup_users1["English"] = array();
	$fieldToolTipsparadesignup_users1["English"] = array();
	$placeHoldersparadesignup_users1["English"] = array();
	$pageTitlesparadesignup_users1["English"] = array();
	$fieldLabelsparadesignup_users1["English"]["ID"] = "ID";
	$fieldToolTipsparadesignup_users1["English"]["ID"] = "";
	$placeHoldersparadesignup_users1["English"]["ID"] = "";
	$fieldLabelsparadesignup_users1["English"]["username"] = "Username";
	$fieldToolTipsparadesignup_users1["English"]["username"] = "";
	$placeHoldersparadesignup_users1["English"]["username"] = "";
	$fieldLabelsparadesignup_users1["English"]["password"] = "Password";
	$fieldToolTipsparadesignup_users1["English"]["password"] = "";
	$placeHoldersparadesignup_users1["English"]["password"] = "";
	$fieldLabelsparadesignup_users1["English"]["email"] = "Email";
	$fieldToolTipsparadesignup_users1["English"]["email"] = "";
	$placeHoldersparadesignup_users1["English"]["email"] = "";
	$fieldLabelsparadesignup_users1["English"]["fullname"] = "Fullname";
	$fieldToolTipsparadesignup_users1["English"]["fullname"] = "";
	$placeHoldersparadesignup_users1["English"]["fullname"] = "";
	$fieldLabelsparadesignup_users1["English"]["groupid"] = "Groupid";
	$fieldToolTipsparadesignup_users1["English"]["groupid"] = "";
	$placeHoldersparadesignup_users1["English"]["groupid"] = "";
	$fieldLabelsparadesignup_users1["English"]["active"] = "Active";
	$fieldToolTipsparadesignup_users1["English"]["active"] = "";
	$placeHoldersparadesignup_users1["English"]["active"] = "";
	$fieldLabelsparadesignup_users1["English"]["ext_security_id"] = "Ext Security Id";
	$fieldToolTipsparadesignup_users1["English"]["ext_security_id"] = "";
	$placeHoldersparadesignup_users1["English"]["ext_security_id"] = "";
	if (count($fieldToolTipsparadesignup_users1["English"]))
		$tdataparadesignup_users1[".isUseToolTips"] = true;
}


	$tdataparadesignup_users1[".NCSearch"] = true;



$tdataparadesignup_users1[".shortTableName"] = "paradesignup_users1";
$tdataparadesignup_users1[".nSecOptions"] = 0;

$tdataparadesignup_users1[".mainTableOwnerID"] = "";
$tdataparadesignup_users1[".entityType"] = 0;
$tdataparadesignup_users1[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdataparadesignup_users1[".strOriginalTableName"] = "ParadeSignup_users1";

		 



$tdataparadesignup_users1[".showAddInPopup"] = false;

$tdataparadesignup_users1[".showEditInPopup"] = false;

$tdataparadesignup_users1[".showViewInPopup"] = false;

$tdataparadesignup_users1[".listAjax"] = false;
//	temporary
//$tdataparadesignup_users1[".listAjax"] = false;

	$tdataparadesignup_users1[".audit"] = false;

	$tdataparadesignup_users1[".locking"] = false;


$pages = $tdataparadesignup_users1[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataparadesignup_users1[".edit"] = true;
	$tdataparadesignup_users1[".afterEditAction"] = 1;
	$tdataparadesignup_users1[".closePopupAfterEdit"] = 1;
	$tdataparadesignup_users1[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataparadesignup_users1[".add"] = true;
$tdataparadesignup_users1[".afterAddAction"] = 1;
$tdataparadesignup_users1[".closePopupAfterAdd"] = 1;
$tdataparadesignup_users1[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataparadesignup_users1[".list"] = true;
}



$tdataparadesignup_users1[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataparadesignup_users1[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataparadesignup_users1[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataparadesignup_users1[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataparadesignup_users1[".printFriendly"] = true;
}



$tdataparadesignup_users1[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataparadesignup_users1[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataparadesignup_users1[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataparadesignup_users1[".isUseAjaxSuggest"] = true;

$tdataparadesignup_users1[".rowHighlite"] = true;



						

$tdataparadesignup_users1[".ajaxCodeSnippetAdded"] = false;

$tdataparadesignup_users1[".buttonsAdded"] = false;

$tdataparadesignup_users1[".addPageEvents"] = false;

// use timepicker for search panel
$tdataparadesignup_users1[".isUseTimeForSearch"] = false;


$tdataparadesignup_users1[".badgeColor"] = "6493EA";


$tdataparadesignup_users1[".allSearchFields"] = array();
$tdataparadesignup_users1[".filterFields"] = array();
$tdataparadesignup_users1[".requiredSearchFields"] = array();

$tdataparadesignup_users1[".googleLikeFields"] = array();
$tdataparadesignup_users1[".googleLikeFields"][] = "ID";
$tdataparadesignup_users1[".googleLikeFields"][] = "username";
$tdataparadesignup_users1[".googleLikeFields"][] = "password";
$tdataparadesignup_users1[".googleLikeFields"][] = "email";
$tdataparadesignup_users1[".googleLikeFields"][] = "fullname";
$tdataparadesignup_users1[".googleLikeFields"][] = "groupid";
$tdataparadesignup_users1[".googleLikeFields"][] = "active";
$tdataparadesignup_users1[".googleLikeFields"][] = "ext_security_id";



$tdataparadesignup_users1[".tableType"] = "list";

$tdataparadesignup_users1[".printerPageOrientation"] = 0;
$tdataparadesignup_users1[".nPrinterPageScale"] = 100;

$tdataparadesignup_users1[".nPrinterSplitRecords"] = 40;

$tdataparadesignup_users1[".geocodingEnabled"] = false;










$tdataparadesignup_users1[".pageSize"] = 20;

$tdataparadesignup_users1[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdataparadesignup_users1[".strOrderBy"] = $tstrOrderBy;

$tdataparadesignup_users1[".orderindexes"] = array();


$tdataparadesignup_users1[".sqlHead"] = "SELECT ID,  	username,  	password,  	email,  	fullname,  	groupid,  	active,  	ext_security_id";
$tdataparadesignup_users1[".sqlFrom"] = "FROM ParadeSignup_users1";
$tdataparadesignup_users1[".sqlWhereExpr"] = "";
$tdataparadesignup_users1[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataparadesignup_users1[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataparadesignup_users1[".arrGroupsPerPage"] = $arrGPP;

$tdataparadesignup_users1[".highlightSearchResults"] = true;

$tableKeysparadesignup_users1 = array();
$tableKeysparadesignup_users1[] = "ID";
$tdataparadesignup_users1[".Keys"] = $tableKeysparadesignup_users1;


$tdataparadesignup_users1[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "ParadeSignup_users1";
	$fdata["Label"] = GetFieldLabel("ParadeSignup_users1","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

		$fdata["sourceSingle"] = "ID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
					//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataparadesignup_users1["ID"] = $fdata;
		$tdataparadesignup_users1[".searchableFields"][] = "ID";
//	username
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "username";
	$fdata["GoodName"] = "username";
	$fdata["ownerTable"] = "ParadeSignup_users1";
	$fdata["Label"] = GetFieldLabel("ParadeSignup_users1","username");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "username";

		$fdata["sourceSingle"] = "username";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "username";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			$edata["validateAs"]["basicValidate"][] = "DenyDuplicated";
	$edata["validateAs"]["customMessages"]["DenyDuplicated"] = array("message" => "Value %value% already exists", "messageType" => "Text");

	
				if(count($edata["validateAs"]) && !in_array('IsRequired', $edata["validateAs"]['basicValidate']))
		$edata["validateAs"]['basicValidate'][] = 'IsRequired';
		//	End validation

	
			
	
		$edata["denyDuplicates"] = true;

	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataparadesignup_users1["username"] = $fdata;
		$tdataparadesignup_users1[".searchableFields"][] = "username";
//	password
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "password";
	$fdata["GoodName"] = "password";
	$fdata["ownerTable"] = "ParadeSignup_users1";
	$fdata["Label"] = GetFieldLabel("ParadeSignup_users1","password");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "password";

		$fdata["sourceSingle"] = "password";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "password";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Password");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Password");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
				if(count($edata["validateAs"]) && !in_array('IsRequired', $edata["validateAs"]['basicValidate']))
		$edata["validateAs"]['basicValidate'][] = 'IsRequired';
		//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataparadesignup_users1["password"] = $fdata;
		$tdataparadesignup_users1[".searchableFields"][] = "password";
//	email
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "email";
	$fdata["GoodName"] = "email";
	$fdata["ownerTable"] = "ParadeSignup_users1";
	$fdata["Label"] = GetFieldLabel("ParadeSignup_users1","email");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "email";

		$fdata["sourceSingle"] = "email";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "email";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
						if(count($edata["validateAs"]) && !in_array('IsEmail', $edata["validateAs"]['basicValidate']))
		$edata["validateAs"]['basicValidate'][] = 'IsEmail';
//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataparadesignup_users1["email"] = $fdata;
		$tdataparadesignup_users1[".searchableFields"][] = "email";
//	fullname
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "fullname";
	$fdata["GoodName"] = "fullname";
	$fdata["ownerTable"] = "ParadeSignup_users1";
	$fdata["Label"] = GetFieldLabel("ParadeSignup_users1","fullname");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "fullname";

		$fdata["sourceSingle"] = "fullname";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fullname";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
					//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataparadesignup_users1["fullname"] = $fdata;
		$tdataparadesignup_users1[".searchableFields"][] = "fullname";
//	groupid
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "groupid";
	$fdata["GoodName"] = "groupid";
	$fdata["ownerTable"] = "ParadeSignup_users1";
	$fdata["Label"] = GetFieldLabel("ParadeSignup_users1","groupid");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "groupid";

		$fdata["sourceSingle"] = "groupid";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "groupid";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
					//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataparadesignup_users1["groupid"] = $fdata;
		$tdataparadesignup_users1[".searchableFields"][] = "groupid";
//	active
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "active";
	$fdata["GoodName"] = "active";
	$fdata["ownerTable"] = "ParadeSignup_users1";
	$fdata["Label"] = GetFieldLabel("ParadeSignup_users1","active");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "active";

		$fdata["sourceSingle"] = "active";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "active";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
					//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataparadesignup_users1["active"] = $fdata;
		$tdataparadesignup_users1[".searchableFields"][] = "active";
//	ext_security_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "ext_security_id";
	$fdata["GoodName"] = "ext_security_id";
	$fdata["ownerTable"] = "ParadeSignup_users1";
	$fdata["Label"] = GetFieldLabel("ParadeSignup_users1","ext_security_id");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ext_security_id";

		$fdata["sourceSingle"] = "ext_security_id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ext_security_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=100";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
					//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataparadesignup_users1["ext_security_id"] = $fdata;
		$tdataparadesignup_users1[".searchableFields"][] = "ext_security_id";


$tables_data["ParadeSignup_users1"]=&$tdataparadesignup_users1;
$field_labels["ParadeSignup_users1"] = &$fieldLabelsparadesignup_users1;
$fieldToolTips["ParadeSignup_users1"] = &$fieldToolTipsparadesignup_users1;
$placeHolders["ParadeSignup_users1"] = &$placeHoldersparadesignup_users1;
$page_titles["ParadeSignup_users1"] = &$pageTitlesparadesignup_users1;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["ParadeSignup_users1"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["ParadeSignup_users1"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_paradesignup_users1()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	username,  	password,  	email,  	fullname,  	groupid,  	active,  	ext_security_id";
$proto0["m_strFrom"] = "FROM ParadeSignup_users1";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "ParadeSignup_users1",
	"m_srcTableName" => "ParadeSignup_users1"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "ParadeSignup_users1";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "username",
	"m_strTable" => "ParadeSignup_users1",
	"m_srcTableName" => "ParadeSignup_users1"
));

$proto8["m_sql"] = "username";
$proto8["m_srcTableName"] = "ParadeSignup_users1";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "password",
	"m_strTable" => "ParadeSignup_users1",
	"m_srcTableName" => "ParadeSignup_users1"
));

$proto10["m_sql"] = "password";
$proto10["m_srcTableName"] = "ParadeSignup_users1";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "email",
	"m_strTable" => "ParadeSignup_users1",
	"m_srcTableName" => "ParadeSignup_users1"
));

$proto12["m_sql"] = "email";
$proto12["m_srcTableName"] = "ParadeSignup_users1";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "fullname",
	"m_strTable" => "ParadeSignup_users1",
	"m_srcTableName" => "ParadeSignup_users1"
));

$proto14["m_sql"] = "fullname";
$proto14["m_srcTableName"] = "ParadeSignup_users1";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "groupid",
	"m_strTable" => "ParadeSignup_users1",
	"m_srcTableName" => "ParadeSignup_users1"
));

$proto16["m_sql"] = "groupid";
$proto16["m_srcTableName"] = "ParadeSignup_users1";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "active",
	"m_strTable" => "ParadeSignup_users1",
	"m_srcTableName" => "ParadeSignup_users1"
));

$proto18["m_sql"] = "active";
$proto18["m_srcTableName"] = "ParadeSignup_users1";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "ext_security_id",
	"m_strTable" => "ParadeSignup_users1",
	"m_srcTableName" => "ParadeSignup_users1"
));

$proto20["m_sql"] = "ext_security_id";
$proto20["m_srcTableName"] = "ParadeSignup_users1";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto22=array();
$proto22["m_link"] = "SQLL_MAIN";
			$proto23=array();
$proto23["m_strName"] = "ParadeSignup_users1";
$proto23["m_srcTableName"] = "ParadeSignup_users1";
$proto23["m_columns"] = array();
$proto23["m_columns"][] = "ID";
$proto23["m_columns"][] = "username";
$proto23["m_columns"][] = "password";
$proto23["m_columns"][] = "email";
$proto23["m_columns"][] = "fullname";
$proto23["m_columns"][] = "groupid";
$proto23["m_columns"][] = "active";
$proto23["m_columns"][] = "ext_security_id";
$proto23["m_columns"][] = "apikey";
$obj = new SQLTable($proto23);

$proto22["m_table"] = $obj;
$proto22["m_sql"] = "ParadeSignup_users1";
$proto22["m_alias"] = "";
$proto22["m_srcTableName"] = "ParadeSignup_users1";
$proto24=array();
$proto24["m_sql"] = "";
$proto24["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto24["m_column"]=$obj;
$proto24["m_contained"] = array();
$proto24["m_strCase"] = "";
$proto24["m_havingmode"] = false;
$proto24["m_inBrackets"] = false;
$proto24["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto24);

$proto22["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto22);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="ParadeSignup_users1";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_paradesignup_users1 = createSqlQuery_paradesignup_users1();


	
					
;

								

$tdataparadesignup_users1[".sqlquery"] = $queryData_paradesignup_users1;



$tableEvents["ParadeSignup_users1"] = new eventsBase;
$tdataparadesignup_users1[".hasEvents"] = false;

?>