<?php
$tdatayesno2 = array();
$tdatayesno2[".searchableFields"] = array();
$tdatayesno2[".ShortName"] = "yesno2";
$tdatayesno2[".OwnerID"] = "";
$tdatayesno2[".OriginalTable"] = "YesNo2";


$tdatayesno2[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatayesno2[".originalPagesByType"] = $tdatayesno2[".pagesByType"];
$tdatayesno2[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatayesno2[".originalPages"] = $tdatayesno2[".pages"];
$tdatayesno2[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatayesno2[".originalDefaultPages"] = $tdatayesno2[".defaultPages"];

//	field labels
$fieldLabelsyesno2 = array();
$fieldToolTipsyesno2 = array();
$pageTitlesyesno2 = array();
$placeHoldersyesno2 = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsyesno2["English"] = array();
	$fieldToolTipsyesno2["English"] = array();
	$placeHoldersyesno2["English"] = array();
	$pageTitlesyesno2["English"] = array();
	$fieldLabelsyesno2["English"]["ID"] = "ID";
	$fieldToolTipsyesno2["English"]["ID"] = "";
	$placeHoldersyesno2["English"]["ID"] = "";
	$fieldLabelsyesno2["English"]["OptionName"] = "Option Name";
	$fieldToolTipsyesno2["English"]["OptionName"] = "";
	$placeHoldersyesno2["English"]["OptionName"] = "";
	$fieldLabelsyesno2["English"]["OptionValue"] = "Option Value";
	$fieldToolTipsyesno2["English"]["OptionValue"] = "";
	$placeHoldersyesno2["English"]["OptionValue"] = "";
	$fieldLabelsyesno2["English"]["AlternateName"] = "Alternate Name";
	$fieldToolTipsyesno2["English"]["AlternateName"] = "";
	$placeHoldersyesno2["English"]["AlternateName"] = "";
	if (count($fieldToolTipsyesno2["English"]))
		$tdatayesno2[".isUseToolTips"] = true;
}


	$tdatayesno2[".NCSearch"] = true;



$tdatayesno2[".shortTableName"] = "yesno2";
$tdatayesno2[".nSecOptions"] = 0;

$tdatayesno2[".mainTableOwnerID"] = "";
$tdatayesno2[".entityType"] = 0;
$tdatayesno2[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdatayesno2[".strOriginalTableName"] = "YesNo2";

		 



$tdatayesno2[".showAddInPopup"] = false;

$tdatayesno2[".showEditInPopup"] = false;

$tdatayesno2[".showViewInPopup"] = false;

$tdatayesno2[".listAjax"] = false;
//	temporary
//$tdatayesno2[".listAjax"] = false;

	$tdatayesno2[".audit"] = false;

	$tdatayesno2[".locking"] = false;


$pages = $tdatayesno2[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatayesno2[".edit"] = true;
	$tdatayesno2[".afterEditAction"] = 1;
	$tdatayesno2[".closePopupAfterEdit"] = 1;
	$tdatayesno2[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatayesno2[".add"] = true;
$tdatayesno2[".afterAddAction"] = 1;
$tdatayesno2[".closePopupAfterAdd"] = 1;
$tdatayesno2[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatayesno2[".list"] = true;
}



$tdatayesno2[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatayesno2[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatayesno2[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatayesno2[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatayesno2[".printFriendly"] = true;
}



$tdatayesno2[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatayesno2[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatayesno2[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatayesno2[".isUseAjaxSuggest"] = true;

$tdatayesno2[".rowHighlite"] = true;



						

$tdatayesno2[".ajaxCodeSnippetAdded"] = false;

$tdatayesno2[".buttonsAdded"] = false;

$tdatayesno2[".addPageEvents"] = false;

// use timepicker for search panel
$tdatayesno2[".isUseTimeForSearch"] = false;


$tdatayesno2[".badgeColor"] = "3CB371";


$tdatayesno2[".allSearchFields"] = array();
$tdatayesno2[".filterFields"] = array();
$tdatayesno2[".requiredSearchFields"] = array();

$tdatayesno2[".googleLikeFields"] = array();
$tdatayesno2[".googleLikeFields"][] = "ID";
$tdatayesno2[".googleLikeFields"][] = "OptionName";
$tdatayesno2[".googleLikeFields"][] = "OptionValue";
$tdatayesno2[".googleLikeFields"][] = "AlternateName";



$tdatayesno2[".tableType"] = "list";

$tdatayesno2[".printerPageOrientation"] = 0;
$tdatayesno2[".nPrinterPageScale"] = 100;

$tdatayesno2[".nPrinterSplitRecords"] = 40;

$tdatayesno2[".geocodingEnabled"] = false;










$tdatayesno2[".pageSize"] = 20;

$tdatayesno2[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatayesno2[".strOrderBy"] = $tstrOrderBy;

$tdatayesno2[".orderindexes"] = array();


$tdatayesno2[".sqlHead"] = "SELECT ID,  	OptionName,  	OptionValue,  	AlternateName";
$tdatayesno2[".sqlFrom"] = "FROM YesNo2";
$tdatayesno2[".sqlWhereExpr"] = "";
$tdatayesno2[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatayesno2[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatayesno2[".arrGroupsPerPage"] = $arrGPP;

$tdatayesno2[".highlightSearchResults"] = true;

$tableKeysyesno2 = array();
$tableKeysyesno2[] = "ID";
$tableKeysyesno2[] = "OptionValue";
$tdatayesno2[".Keys"] = $tableKeysyesno2;


$tdatayesno2[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "YesNo2";
	$fdata["Label"] = GetFieldLabel("YesNo2","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

		$fdata["sourceSingle"] = "ID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatayesno2["ID"] = $fdata;
		$tdatayesno2[".searchableFields"][] = "ID";
//	OptionName
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "OptionName";
	$fdata["GoodName"] = "OptionName";
	$fdata["ownerTable"] = "YesNo2";
	$fdata["Label"] = GetFieldLabel("YesNo2","OptionName");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "OptionName";

		$fdata["sourceSingle"] = "OptionName";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "OptionName";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatayesno2["OptionName"] = $fdata;
		$tdatayesno2[".searchableFields"][] = "OptionName";
//	OptionValue
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "OptionValue";
	$fdata["GoodName"] = "OptionValue";
	$fdata["ownerTable"] = "YesNo2";
	$fdata["Label"] = GetFieldLabel("YesNo2","OptionValue");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "OptionValue";

		$fdata["sourceSingle"] = "OptionValue";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "OptionValue";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatayesno2["OptionValue"] = $fdata;
		$tdatayesno2[".searchableFields"][] = "OptionValue";
//	AlternateName
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "AlternateName";
	$fdata["GoodName"] = "AlternateName";
	$fdata["ownerTable"] = "YesNo2";
	$fdata["Label"] = GetFieldLabel("YesNo2","AlternateName");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "AlternateName";

		$fdata["sourceSingle"] = "AlternateName";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "AlternateName";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatayesno2["AlternateName"] = $fdata;
		$tdatayesno2[".searchableFields"][] = "AlternateName";


$tables_data["YesNo2"]=&$tdatayesno2;
$field_labels["YesNo2"] = &$fieldLabelsyesno2;
$fieldToolTips["YesNo2"] = &$fieldToolTipsyesno2;
$placeHolders["YesNo2"] = &$placeHoldersyesno2;
$page_titles["YesNo2"] = &$pageTitlesyesno2;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["YesNo2"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["YesNo2"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_yesno2()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	OptionName,  	OptionValue,  	AlternateName";
$proto0["m_strFrom"] = "FROM YesNo2";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "YesNo2",
	"m_srcTableName" => "YesNo2"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "YesNo2";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "OptionName",
	"m_strTable" => "YesNo2",
	"m_srcTableName" => "YesNo2"
));

$proto8["m_sql"] = "OptionName";
$proto8["m_srcTableName"] = "YesNo2";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "OptionValue",
	"m_strTable" => "YesNo2",
	"m_srcTableName" => "YesNo2"
));

$proto10["m_sql"] = "OptionValue";
$proto10["m_srcTableName"] = "YesNo2";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "AlternateName",
	"m_strTable" => "YesNo2",
	"m_srcTableName" => "YesNo2"
));

$proto12["m_sql"] = "AlternateName";
$proto12["m_srcTableName"] = "YesNo2";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto14=array();
$proto14["m_link"] = "SQLL_MAIN";
			$proto15=array();
$proto15["m_strName"] = "YesNo2";
$proto15["m_srcTableName"] = "YesNo2";
$proto15["m_columns"] = array();
$proto15["m_columns"][] = "ID";
$proto15["m_columns"][] = "OptionName";
$proto15["m_columns"][] = "OptionValue";
$proto15["m_columns"][] = "AlternateName";
$obj = new SQLTable($proto15);

$proto14["m_table"] = $obj;
$proto14["m_sql"] = "YesNo2";
$proto14["m_alias"] = "";
$proto14["m_srcTableName"] = "YesNo2";
$proto16=array();
$proto16["m_sql"] = "";
$proto16["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto16["m_column"]=$obj;
$proto16["m_contained"] = array();
$proto16["m_strCase"] = "";
$proto16["m_havingmode"] = false;
$proto16["m_inBrackets"] = false;
$proto16["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto16);

$proto14["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto14);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="YesNo2";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_yesno2 = createSqlQuery_yesno2();


	
					
;

				

$tdatayesno2[".sqlquery"] = $queryData_yesno2;



$tableEvents["YesNo2"] = new eventsBase;
$tdatayesno2[".hasEvents"] = false;

?>