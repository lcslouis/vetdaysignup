<?php
$tdataadminallentry = array();
$tdataadminallentry[".searchableFields"] = array();
$tdataadminallentry[".ShortName"] = "adminallentry";
$tdataadminallentry[".OwnerID"] = "UID";
$tdataadminallentry[".OriginalTable"] = "Form";


$tdataadminallentry[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataadminallentry[".originalPagesByType"] = $tdataadminallentry[".pagesByType"];
$tdataadminallentry[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataadminallentry[".originalPages"] = $tdataadminallentry[".pages"];
$tdataadminallentry[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataadminallentry[".originalDefaultPages"] = $tdataadminallentry[".defaultPages"];

//	field labels
$fieldLabelsadminallentry = array();
$fieldToolTipsadminallentry = array();
$pageTitlesadminallentry = array();
$placeHoldersadminallentry = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsadminallentry["English"] = array();
	$fieldToolTipsadminallentry["English"] = array();
	$placeHoldersadminallentry["English"] = array();
	$pageTitlesadminallentry["English"] = array();
	$fieldLabelsadminallentry["English"]["ID"] = "Entry ID";
	$fieldToolTipsadminallentry["English"]["ID"] = "";
	$placeHoldersadminallentry["English"]["ID"] = "";
	$fieldLabelsadminallentry["English"]["NameOfOrg"] = "Name Of Organization";
	$fieldToolTipsadminallentry["English"]["NameOfOrg"] = "";
	$placeHoldersadminallentry["English"]["NameOfOrg"] = "";
	$fieldLabelsadminallentry["English"]["TypeOfOrg"] = "Type Of Organization";
	$fieldToolTipsadminallentry["English"]["TypeOfOrg"] = "";
	$placeHoldersadminallentry["English"]["TypeOfOrg"] = "";
	$fieldLabelsadminallentry["English"]["SchoolClass"] = "School Class";
	$fieldToolTipsadminallentry["English"]["SchoolClass"] = "";
	$placeHoldersadminallentry["English"]["SchoolClass"] = "";
	$fieldLabelsadminallentry["English"]["EntryType"] = "Entry Type";
	$fieldToolTipsadminallentry["English"]["EntryType"] = "";
	$placeHoldersadminallentry["English"]["EntryType"] = "";
	$fieldLabelsadminallentry["English"]["KeyPeople"] = "Key People";
	$fieldToolTipsadminallentry["English"]["KeyPeople"] = "";
	$placeHoldersadminallentry["English"]["KeyPeople"] = "";
	$fieldLabelsadminallentry["English"]["EntrySize"] = "Entry Size";
	$fieldToolTipsadminallentry["English"]["EntrySize"] = "";
	$placeHoldersadminallentry["English"]["EntrySize"] = "";
	$fieldLabelsadminallentry["English"]["ContactName"] = "Contact Name";
	$fieldToolTipsadminallentry["English"]["ContactName"] = "";
	$placeHoldersadminallentry["English"]["ContactName"] = "";
	$fieldLabelsadminallentry["English"]["ContactAddress"] = "Contact Address";
	$fieldToolTipsadminallentry["English"]["ContactAddress"] = "";
	$placeHoldersadminallentry["English"]["ContactAddress"] = "";
	$fieldLabelsadminallentry["English"]["ContactEmail"] = "Contact Email";
	$fieldToolTipsadminallentry["English"]["ContactEmail"] = "";
	$placeHoldersadminallentry["English"]["ContactEmail"] = "";
	$fieldLabelsadminallentry["English"]["ContactZip"] = "Contact Zip";
	$fieldToolTipsadminallentry["English"]["ContactZip"] = "";
	$placeHoldersadminallentry["English"]["ContactZip"] = "";
	$fieldLabelsadminallentry["English"]["ContactPhone"] = "Contact Phone";
	$fieldToolTipsadminallentry["English"]["ContactPhone"] = "";
	$placeHoldersadminallentry["English"]["ContactPhone"] = "";
	$fieldLabelsadminallentry["English"]["ContactFax"] = "Contact Fax";
	$fieldToolTipsadminallentry["English"]["ContactFax"] = "";
	$placeHoldersadminallentry["English"]["ContactFax"] = "";
	$fieldLabelsadminallentry["English"]["ContactCell"] = "Contact Cell";
	$fieldToolTipsadminallentry["English"]["ContactCell"] = "";
	$placeHoldersadminallentry["English"]["ContactCell"] = "";
	$fieldLabelsadminallentry["English"]["DescriptionOfEntry"] = "Description Of Entry";
	$fieldToolTipsadminallentry["English"]["DescriptionOfEntry"] = "";
	$placeHoldersadminallentry["English"]["DescriptionOfEntry"] = "";
	$fieldLabelsadminallentry["English"]["UID"] = "User ID";
	$fieldToolTipsadminallentry["English"]["UID"] = "";
	$placeHoldersadminallentry["English"]["UID"] = "";
	$fieldLabelsadminallentry["English"]["Approved"] = "Approved";
	$fieldToolTipsadminallentry["English"]["Approved"] = "";
	$placeHoldersadminallentry["English"]["Approved"] = "";
	$fieldLabelsadminallentry["English"]["NumOfHorses"] = "Num Of Horses";
	$fieldToolTipsadminallentry["English"]["NumOfHorses"] = "";
	$placeHoldersadminallentry["English"]["NumOfHorses"] = "";
	$fieldLabelsadminallentry["English"]["LowerThirds"] = "Lower Thirds";
	$fieldToolTipsadminallentry["English"]["LowerThirds"] = "";
	$placeHoldersadminallentry["English"]["LowerThirds"] = "";
	$fieldLabelsadminallentry["English"]["ParadeOrder"] = "Parade Order";
	$fieldToolTipsadminallentry["English"]["ParadeOrder"] = "";
	$placeHoldersadminallentry["English"]["ParadeOrder"] = "";
	$fieldLabelsadminallentry["English"]["GroupOrganizer"] = "Group Organizer Name";
	$fieldToolTipsadminallentry["English"]["GroupOrganizer"] = "";
	$placeHoldersadminallentry["English"]["GroupOrganizer"] = "";
	$fieldLabelsadminallentry["English"]["AgreetoRules"] = "Agreeto Rules";
	$fieldToolTipsadminallentry["English"]["AgreetoRules"] = "";
	$placeHoldersadminallentry["English"]["AgreetoRules"] = "";
	$fieldLabelsadminallentry["English"]["LiabilityWavierSigned"] = "Liability Wavier Signed";
	$fieldToolTipsadminallentry["English"]["LiabilityWavierSigned"] = "";
	$placeHoldersadminallentry["English"]["LiabilityWavierSigned"] = "";
	if (count($fieldToolTipsadminallentry["English"]))
		$tdataadminallentry[".isUseToolTips"] = true;
}


	$tdataadminallentry[".NCSearch"] = true;



$tdataadminallentry[".shortTableName"] = "adminallentry";
$tdataadminallentry[".nSecOptions"] = 0;

$tdataadminallentry[".mainTableOwnerID"] = "UID";
$tdataadminallentry[".entityType"] = 1;
$tdataadminallentry[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdataadminallentry[".strOriginalTableName"] = "Form";

		 



$tdataadminallentry[".showAddInPopup"] = false;

$tdataadminallentry[".showEditInPopup"] = false;

$tdataadminallentry[".showViewInPopup"] = false;

$tdataadminallentry[".listAjax"] = false;
//	temporary
//$tdataadminallentry[".listAjax"] = false;

	$tdataadminallentry[".audit"] = false;

	$tdataadminallentry[".locking"] = false;


$pages = $tdataadminallentry[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataadminallentry[".edit"] = true;
	$tdataadminallentry[".afterEditAction"] = 1;
	$tdataadminallentry[".closePopupAfterEdit"] = 1;
	$tdataadminallentry[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataadminallentry[".add"] = true;
$tdataadminallentry[".afterAddAction"] = 1;
$tdataadminallentry[".closePopupAfterAdd"] = 1;
$tdataadminallentry[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataadminallentry[".list"] = true;
}



$tdataadminallentry[".strSortControlSettingsJSON"] = "";

$tdataadminallentry[".strClickActionJSON"] = "{\"fields\":{\"Approved\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactAddress\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactCell\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactEmail\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactFax\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactName\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactPhone\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactZip\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"DescriptionOfEntry\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"EntrySize\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"EntryType\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ID\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"KeyPeople\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"NameOfOrg\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"SchoolClass\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"TypeOfOrg\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"UID\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}}},\"row\":{\"action\":\"open\",\"codeData\":{},\"gridData\":{\"action\":\"checkbox\",\"table\":null},\"openData\":{\"how\":\"goto\",\"page\":\"view\",\"table\":null,\"url\":\"\"}}}";



if( $pages[PAGE_VIEW] ) {
$tdataadminallentry[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataadminallentry[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataadminallentry[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataadminallentry[".printFriendly"] = true;
}



$tdataadminallentry[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataadminallentry[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataadminallentry[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataadminallentry[".isUseAjaxSuggest"] = true;

$tdataadminallentry[".rowHighlite"] = true;



						

$tdataadminallentry[".ajaxCodeSnippetAdded"] = false;

$tdataadminallentry[".buttonsAdded"] = false;

$tdataadminallentry[".addPageEvents"] = false;

// use timepicker for search panel
$tdataadminallentry[".isUseTimeForSearch"] = false;


$tdataadminallentry[".badgeColor"] = "E07878";


$tdataadminallentry[".allSearchFields"] = array();
$tdataadminallentry[".filterFields"] = array();
$tdataadminallentry[".requiredSearchFields"] = array();

$tdataadminallentry[".googleLikeFields"] = array();
$tdataadminallentry[".googleLikeFields"][] = "ID";
$tdataadminallentry[".googleLikeFields"][] = "NameOfOrg";
$tdataadminallentry[".googleLikeFields"][] = "TypeOfOrg";
$tdataadminallentry[".googleLikeFields"][] = "SchoolClass";
$tdataadminallentry[".googleLikeFields"][] = "EntryType";
$tdataadminallentry[".googleLikeFields"][] = "KeyPeople";
$tdataadminallentry[".googleLikeFields"][] = "EntrySize";
$tdataadminallentry[".googleLikeFields"][] = "ContactName";
$tdataadminallentry[".googleLikeFields"][] = "ContactAddress";
$tdataadminallentry[".googleLikeFields"][] = "ContactEmail";
$tdataadminallentry[".googleLikeFields"][] = "ContactZip";
$tdataadminallentry[".googleLikeFields"][] = "ContactPhone";
$tdataadminallentry[".googleLikeFields"][] = "ContactFax";
$tdataadminallentry[".googleLikeFields"][] = "ContactCell";
$tdataadminallentry[".googleLikeFields"][] = "DescriptionOfEntry";
$tdataadminallentry[".googleLikeFields"][] = "UID";
$tdataadminallentry[".googleLikeFields"][] = "Approved";
$tdataadminallentry[".googleLikeFields"][] = "NumOfHorses";
$tdataadminallentry[".googleLikeFields"][] = "LowerThirds";
$tdataadminallentry[".googleLikeFields"][] = "ParadeOrder";
$tdataadminallentry[".googleLikeFields"][] = "GroupOrganizer";
$tdataadminallentry[".googleLikeFields"][] = "AgreetoRules";
$tdataadminallentry[".googleLikeFields"][] = "LiabilityWavierSigned";



$tdataadminallentry[".tableType"] = "list";

$tdataadminallentry[".printerPageOrientation"] = 0;
$tdataadminallentry[".nPrinterPageScale"] = 100;

$tdataadminallentry[".nPrinterSplitRecords"] = 40;

$tdataadminallentry[".geocodingEnabled"] = false;










$tdataadminallentry[".pageSize"] = 20;

$tdataadminallentry[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdataadminallentry[".strOrderBy"] = $tstrOrderBy;

$tdataadminallentry[".orderindexes"] = array();


$tdataadminallentry[".sqlHead"] = "SELECT Form.ID,  Form.NameOfOrg,  Form.TypeOfOrg,  Form.SchoolClass,  Form.EntryType,  Form.KeyPeople,  Form.EntrySize,  Form.ContactName,  Form.ContactAddress,  Form.ContactEmail,  Form.ContactZip,  Form.ContactPhone,  Form.ContactFax,  Form.ContactCell,  Form.DescriptionOfEntry,  Form.`UID`,  Form.Approved,  Form.NumOfHorses,  Form.LowerThirds,  Form.ParadeOrder,  Form.GroupOrganizer,  Form.AgreetoRules,  Form.LiabilityWavierSigned";
$tdataadminallentry[".sqlFrom"] = "FROM Form  , QRCodeURL";
$tdataadminallentry[".sqlWhereExpr"] = "";
$tdataadminallentry[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataadminallentry[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataadminallentry[".arrGroupsPerPage"] = $arrGPP;

$tdataadminallentry[".highlightSearchResults"] = true;

$tableKeysadminallentry = array();
$tableKeysadminallentry[] = "ID";
$tdataadminallentry[".Keys"] = $tableKeysadminallentry;


$tdataadminallentry[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["ID"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "ID";
//	NameOfOrg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "NameOfOrg";
	$fdata["GoodName"] = "NameOfOrg";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","NameOfOrg");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "NameOfOrg";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.NameOfOrg";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["NameOfOrg"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "NameOfOrg";
//	TypeOfOrg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "TypeOfOrg";
	$fdata["GoodName"] = "TypeOfOrg";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","TypeOfOrg");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "TypeOfOrg";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.TypeOfOrg";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "OrgTypes";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "ID";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "OrgTypeName";

	

	
	$edata["LookupOrderBy"] = "ID";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["TypeOfOrg"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "TypeOfOrg";
//	SchoolClass
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "SchoolClass";
	$fdata["GoodName"] = "SchoolClass";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","SchoolClass");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "SchoolClass";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.SchoolClass";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "SchoolType";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "ID";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "TypeName";

	

	
	$edata["LookupOrderBy"] = "ID";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["SchoolClass"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "SchoolClass";
//	EntryType
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "EntryType";
	$fdata["GoodName"] = "EntryType";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","EntryType");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "EntryType";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.EntryType";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "EntryType";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "ID";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "EntryTypeName";

	

	
	$edata["LookupOrderBy"] = "ID";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["EntryType"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "EntryType";
//	KeyPeople
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "KeyPeople";
	$fdata["GoodName"] = "KeyPeople";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","KeyPeople");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "KeyPeople";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.KeyPeople";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["KeyPeople"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "KeyPeople";
//	EntrySize
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "EntrySize";
	$fdata["GoodName"] = "EntrySize";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","EntrySize");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "EntrySize";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.EntrySize";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["EntrySize"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "EntrySize";
//	ContactName
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "ContactName";
	$fdata["GoodName"] = "ContactName";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","ContactName");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactName";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ContactName";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=200";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["ContactName"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "ContactName";
//	ContactAddress
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "ContactAddress";
	$fdata["GoodName"] = "ContactAddress";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","ContactAddress");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactAddress";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ContactAddress";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["ContactAddress"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "ContactAddress";
//	ContactEmail
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "ContactEmail";
	$fdata["GoodName"] = "ContactEmail";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","ContactEmail");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactEmail";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ContactEmail";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["ContactEmail"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "ContactEmail";
//	ContactZip
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "ContactZip";
	$fdata["GoodName"] = "ContactZip";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","ContactZip");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactZip";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ContactZip";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["ContactZip"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "ContactZip";
//	ContactPhone
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "ContactPhone";
	$fdata["GoodName"] = "ContactPhone";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","ContactPhone");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactPhone";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ContactPhone";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["ContactPhone"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "ContactPhone";
//	ContactFax
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "ContactFax";
	$fdata["GoodName"] = "ContactFax";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","ContactFax");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactFax";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ContactFax";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["ContactFax"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "ContactFax";
//	ContactCell
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "ContactCell";
	$fdata["GoodName"] = "ContactCell";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","ContactCell");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactCell";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ContactCell";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["ContactCell"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "ContactCell";
//	DescriptionOfEntry
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "DescriptionOfEntry";
	$fdata["GoodName"] = "DescriptionOfEntry";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","DescriptionOfEntry");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "DescriptionOfEntry";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.DescriptionOfEntry";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["DescriptionOfEntry"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "DescriptionOfEntry";
//	UID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "UID";
	$fdata["GoodName"] = "UID";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","UID");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "UID";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.`UID`";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["UID"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "UID";
//	Approved
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "Approved";
	$fdata["GoodName"] = "Approved";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","Approved");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "Approved";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.Approved";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "YesNo";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "OptionValue";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "OptionName";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["Approved"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "Approved";
//	NumOfHorses
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "NumOfHorses";
	$fdata["GoodName"] = "NumOfHorses";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","NumOfHorses");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "NumOfHorses";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.NumOfHorses";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["NumOfHorses"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "NumOfHorses";
//	LowerThirds
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "LowerThirds";
	$fdata["GoodName"] = "LowerThirds";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","LowerThirds");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "LowerThirds";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.LowerThirds";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["LowerThirds"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "LowerThirds";
//	ParadeOrder
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "ParadeOrder";
	$fdata["GoodName"] = "ParadeOrder";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","ParadeOrder");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ParadeOrder";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ParadeOrder";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["ParadeOrder"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "ParadeOrder";
//	GroupOrganizer
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "GroupOrganizer";
	$fdata["GoodName"] = "GroupOrganizer";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","GroupOrganizer");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "GroupOrganizer";

		$fdata["sourceSingle"] = "GroupOrganizer";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.GroupOrganizer";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["GroupOrganizer"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "GroupOrganizer";
//	AgreetoRules
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 22;
	$fdata["strName"] = "AgreetoRules";
	$fdata["GoodName"] = "AgreetoRules";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","AgreetoRules");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "AgreetoRules";

		$fdata["sourceSingle"] = "AgreetoRules";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.AgreetoRules";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "YesNo";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
		
	$edata["LinkField"] = "OptionValue";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "AlternateName";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["AgreetoRules"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "AgreetoRules";
//	LiabilityWavierSigned
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 23;
	$fdata["strName"] = "LiabilityWavierSigned";
	$fdata["GoodName"] = "LiabilityWavierSigned";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("AdminAllEntry","LiabilityWavierSigned");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "LiabilityWavierSigned";

		$fdata["sourceSingle"] = "LiabilityWavierSigned";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.LiabilityWavierSigned";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "YesNo";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "OptionValue";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "AlternateName";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataadminallentry["LiabilityWavierSigned"] = $fdata;
		$tdataadminallentry[".searchableFields"][] = "LiabilityWavierSigned";


$tables_data["AdminAllEntry"]=&$tdataadminallentry;
$field_labels["AdminAllEntry"] = &$fieldLabelsadminallentry;
$fieldToolTips["AdminAllEntry"] = &$fieldToolTipsadminallentry;
$placeHolders["AdminAllEntry"] = &$placeHoldersadminallentry;
$page_titles["AdminAllEntry"] = &$pageTitlesadminallentry;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["AdminAllEntry"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["AdminAllEntry"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_adminallentry()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "Form.ID,  Form.NameOfOrg,  Form.TypeOfOrg,  Form.SchoolClass,  Form.EntryType,  Form.KeyPeople,  Form.EntrySize,  Form.ContactName,  Form.ContactAddress,  Form.ContactEmail,  Form.ContactZip,  Form.ContactPhone,  Form.ContactFax,  Form.ContactCell,  Form.DescriptionOfEntry,  Form.`UID`,  Form.Approved,  Form.NumOfHorses,  Form.LowerThirds,  Form.ParadeOrder,  Form.GroupOrganizer,  Form.AgreetoRules,  Form.LiabilityWavierSigned";
$proto0["m_strFrom"] = "FROM Form  , QRCodeURL";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto6["m_sql"] = "Form.ID";
$proto6["m_srcTableName"] = "AdminAllEntry";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "NameOfOrg",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto8["m_sql"] = "Form.NameOfOrg";
$proto8["m_srcTableName"] = "AdminAllEntry";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "TypeOfOrg",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto10["m_sql"] = "Form.TypeOfOrg";
$proto10["m_srcTableName"] = "AdminAllEntry";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "SchoolClass",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto12["m_sql"] = "Form.SchoolClass";
$proto12["m_srcTableName"] = "AdminAllEntry";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "EntryType",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto14["m_sql"] = "Form.EntryType";
$proto14["m_srcTableName"] = "AdminAllEntry";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "KeyPeople",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto16["m_sql"] = "Form.KeyPeople";
$proto16["m_srcTableName"] = "AdminAllEntry";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "EntrySize",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto18["m_sql"] = "Form.EntrySize";
$proto18["m_srcTableName"] = "AdminAllEntry";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactName",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto20["m_sql"] = "Form.ContactName";
$proto20["m_srcTableName"] = "AdminAllEntry";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactAddress",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto22["m_sql"] = "Form.ContactAddress";
$proto22["m_srcTableName"] = "AdminAllEntry";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactEmail",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto24["m_sql"] = "Form.ContactEmail";
$proto24["m_srcTableName"] = "AdminAllEntry";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactZip",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto26["m_sql"] = "Form.ContactZip";
$proto26["m_srcTableName"] = "AdminAllEntry";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactPhone",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto28["m_sql"] = "Form.ContactPhone";
$proto28["m_srcTableName"] = "AdminAllEntry";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactFax",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto30["m_sql"] = "Form.ContactFax";
$proto30["m_srcTableName"] = "AdminAllEntry";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactCell",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto32["m_sql"] = "Form.ContactCell";
$proto32["m_srcTableName"] = "AdminAllEntry";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "DescriptionOfEntry",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto34["m_sql"] = "Form.DescriptionOfEntry";
$proto34["m_srcTableName"] = "AdminAllEntry";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "UID",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto36["m_sql"] = "Form.`UID`";
$proto36["m_srcTableName"] = "AdminAllEntry";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "Approved",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto38["m_sql"] = "Form.Approved";
$proto38["m_srcTableName"] = "AdminAllEntry";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "NumOfHorses",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto40["m_sql"] = "Form.NumOfHorses";
$proto40["m_srcTableName"] = "AdminAllEntry";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "LowerThirds",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto42["m_sql"] = "Form.LowerThirds";
$proto42["m_srcTableName"] = "AdminAllEntry";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "ParadeOrder",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto44["m_sql"] = "Form.ParadeOrder";
$proto44["m_srcTableName"] = "AdminAllEntry";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$obj = new SQLField(array(
	"m_strName" => "GroupOrganizer",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto46["m_sql"] = "Form.GroupOrganizer";
$proto46["m_srcTableName"] = "AdminAllEntry";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
						$proto48=array();
			$obj = new SQLField(array(
	"m_strName" => "AgreetoRules",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto48["m_sql"] = "Form.AgreetoRules";
$proto48["m_srcTableName"] = "AdminAllEntry";
$proto48["m_expr"]=$obj;
$proto48["m_alias"] = "";
$obj = new SQLFieldListItem($proto48);

$proto0["m_fieldlist"][]=$obj;
						$proto50=array();
			$obj = new SQLField(array(
	"m_strName" => "LiabilityWavierSigned",
	"m_strTable" => "Form",
	"m_srcTableName" => "AdminAllEntry"
));

$proto50["m_sql"] = "Form.LiabilityWavierSigned";
$proto50["m_srcTableName"] = "AdminAllEntry";
$proto50["m_expr"]=$obj;
$proto50["m_alias"] = "";
$obj = new SQLFieldListItem($proto50);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto52=array();
$proto52["m_link"] = "SQLL_MAIN";
			$proto53=array();
$proto53["m_strName"] = "Form";
$proto53["m_srcTableName"] = "AdminAllEntry";
$proto53["m_columns"] = array();
$proto53["m_columns"][] = "ID";
$proto53["m_columns"][] = "NameOfOrg";
$proto53["m_columns"][] = "TypeOfOrg";
$proto53["m_columns"][] = "SchoolClass";
$proto53["m_columns"][] = "EntryType";
$proto53["m_columns"][] = "KeyPeople";
$proto53["m_columns"][] = "EntrySize";
$proto53["m_columns"][] = "ContactName";
$proto53["m_columns"][] = "ContactAddress";
$proto53["m_columns"][] = "ContactEmail";
$proto53["m_columns"][] = "ContactZip";
$proto53["m_columns"][] = "ContactPhone";
$proto53["m_columns"][] = "ContactFax";
$proto53["m_columns"][] = "ContactCell";
$proto53["m_columns"][] = "DescriptionOfEntry";
$proto53["m_columns"][] = "UID";
$proto53["m_columns"][] = "NumOfHorses";
$proto53["m_columns"][] = "Approved";
$proto53["m_columns"][] = "LowerThirds";
$proto53["m_columns"][] = "ParadeOrder";
$proto53["m_columns"][] = "GroupOrganizer";
$proto53["m_columns"][] = "AgreetoRules";
$proto53["m_columns"][] = "LiabilityWavierSigned";
$obj = new SQLTable($proto53);

$proto52["m_table"] = $obj;
$proto52["m_sql"] = "Form";
$proto52["m_alias"] = "";
$proto52["m_srcTableName"] = "AdminAllEntry";
$proto54=array();
$proto54["m_sql"] = "";
$proto54["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto54["m_column"]=$obj;
$proto54["m_contained"] = array();
$proto54["m_strCase"] = "";
$proto54["m_havingmode"] = false;
$proto54["m_inBrackets"] = false;
$proto54["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto54);

$proto52["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto52);

$proto0["m_fromlist"][]=$obj;
												$proto56=array();
$proto56["m_link"] = "SQLL_CROSSJOIN";
			$proto57=array();
$proto57["m_strName"] = "QRCodeURL";
$proto57["m_srcTableName"] = "AdminAllEntry";
$proto57["m_columns"] = array();
$proto57["m_columns"][] = "ID";
$proto57["m_columns"][] = "ProgramURL";
$proto57["m_columns"][] = "PageName";
$obj = new SQLTable($proto57);

$proto56["m_table"] = $obj;
$proto56["m_sql"] = "QRCodeURL";
$proto56["m_alias"] = "";
$proto56["m_srcTableName"] = "AdminAllEntry";
$proto58=array();
$proto58["m_sql"] = "";
$proto58["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto58["m_column"]=$obj;
$proto58["m_contained"] = array();
$proto58["m_strCase"] = "";
$proto58["m_havingmode"] = false;
$proto58["m_inBrackets"] = false;
$proto58["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto58);

$proto56["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto56);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="AdminAllEntry";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_adminallentry = createSqlQuery_adminallentry();


	
					
;

																							

$tdataadminallentry[".sqlquery"] = $queryData_adminallentry;



$tableEvents["AdminAllEntry"] = new eventsBase;
$tdataadminallentry[".hasEvents"] = false;

?>