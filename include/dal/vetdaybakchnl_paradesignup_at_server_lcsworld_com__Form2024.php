<?php
$dalTableForm2024 = array();
$dalTableForm2024["ID"] = array("type"=>3,"varname"=>"ID", "name" => "ID");
$dalTableForm2024["NameOfOrg"] = array("type"=>201,"varname"=>"NameOfOrg", "name" => "NameOfOrg");
$dalTableForm2024["TypeOfOrg"] = array("type"=>3,"varname"=>"TypeOfOrg", "name" => "TypeOfOrg");
$dalTableForm2024["SchoolClass"] = array("type"=>3,"varname"=>"SchoolClass", "name" => "SchoolClass");
$dalTableForm2024["EntryType"] = array("type"=>200,"varname"=>"EntryType", "name" => "EntryType");
$dalTableForm2024["KeyPeople"] = array("type"=>201,"varname"=>"KeyPeople", "name" => "KeyPeople");
$dalTableForm2024["EntrySize"] = array("type"=>200,"varname"=>"EntrySize", "name" => "EntrySize");
$dalTableForm2024["ContactName"] = array("type"=>200,"varname"=>"ContactName", "name" => "ContactName");
$dalTableForm2024["ContactAddress"] = array("type"=>200,"varname"=>"ContactAddress", "name" => "ContactAddress");
$dalTableForm2024["ContactEmail"] = array("type"=>200,"varname"=>"ContactEmail", "name" => "ContactEmail");
$dalTableForm2024["ContactZip"] = array("type"=>200,"varname"=>"ContactZip", "name" => "ContactZip");
$dalTableForm2024["ContactPhone"] = array("type"=>200,"varname"=>"ContactPhone", "name" => "ContactPhone");
$dalTableForm2024["ContactFax"] = array("type"=>200,"varname"=>"ContactFax", "name" => "ContactFax");
$dalTableForm2024["ContactCell"] = array("type"=>200,"varname"=>"ContactCell", "name" => "ContactCell");
$dalTableForm2024["DescriptionOfEntry"] = array("type"=>201,"varname"=>"DescriptionOfEntry", "name" => "DescriptionOfEntry");
$dalTableForm2024["UID"] = array("type"=>3,"varname"=>"UID", "name" => "UID");
$dalTableForm2024["NumOfHorses"] = array("type"=>3,"varname"=>"NumOfHorses", "name" => "NumOfHorses");
$dalTableForm2024["Approved"] = array("type"=>3,"varname"=>"Approved", "name" => "Approved");
$dalTableForm2024["LowerThirds"] = array("type"=>200,"varname"=>"LowerThirds", "name" => "LowerThirds");
$dalTableForm2024["ParadeOrder"] = array("type"=>200,"varname"=>"ParadeOrder", "name" => "ParadeOrder");
$dalTableForm2024["GroupOrganizer"] = array("type"=>200,"varname"=>"GroupOrganizer", "name" => "GroupOrganizer");
$dalTableForm2024["AgreetoRules"] = array("type"=>3,"varname"=>"AgreetoRules", "name" => "AgreetoRules");
$dalTableForm2024["LiabilityWavierSigned"] = array("type"=>3,"varname"=>"LiabilityWavierSigned", "name" => "LiabilityWavierSigned");
	$dalTableForm2024["ID"]["key"]=true;

$dal_info["vetdaybakchnl_paradesignup_at_server_lcsworld_com__Form2024"] = &$dalTableForm2024;
?>