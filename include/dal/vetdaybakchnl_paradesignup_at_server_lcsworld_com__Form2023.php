<?php
$dalTableForm2023 = array();
$dalTableForm2023["ID"] = array("type"=>3,"varname"=>"ID", "name" => "ID");
$dalTableForm2023["NameOfOrg"] = array("type"=>201,"varname"=>"NameOfOrg", "name" => "NameOfOrg");
$dalTableForm2023["TypeOfOrg"] = array("type"=>3,"varname"=>"TypeOfOrg", "name" => "TypeOfOrg");
$dalTableForm2023["SchoolClass"] = array("type"=>3,"varname"=>"SchoolClass", "name" => "SchoolClass");
$dalTableForm2023["EntryType"] = array("type"=>200,"varname"=>"EntryType", "name" => "EntryType");
$dalTableForm2023["KeyPeople"] = array("type"=>201,"varname"=>"KeyPeople", "name" => "KeyPeople");
$dalTableForm2023["EntrySize"] = array("type"=>200,"varname"=>"EntrySize", "name" => "EntrySize");
$dalTableForm2023["ContactName"] = array("type"=>200,"varname"=>"ContactName", "name" => "ContactName");
$dalTableForm2023["ContactAddress"] = array("type"=>200,"varname"=>"ContactAddress", "name" => "ContactAddress");
$dalTableForm2023["ContactEmail"] = array("type"=>200,"varname"=>"ContactEmail", "name" => "ContactEmail");
$dalTableForm2023["ContactZip"] = array("type"=>200,"varname"=>"ContactZip", "name" => "ContactZip");
$dalTableForm2023["ContactPhone"] = array("type"=>200,"varname"=>"ContactPhone", "name" => "ContactPhone");
$dalTableForm2023["ContactFax"] = array("type"=>200,"varname"=>"ContactFax", "name" => "ContactFax");
$dalTableForm2023["ContactCell"] = array("type"=>200,"varname"=>"ContactCell", "name" => "ContactCell");
$dalTableForm2023["DescriptionOfEntry"] = array("type"=>201,"varname"=>"DescriptionOfEntry", "name" => "DescriptionOfEntry");
$dalTableForm2023["UID"] = array("type"=>3,"varname"=>"UID", "name" => "UID");
$dalTableForm2023["NumOfHorses"] = array("type"=>3,"varname"=>"NumOfHorses", "name" => "NumOfHorses");
$dalTableForm2023["Approved"] = array("type"=>3,"varname"=>"Approved", "name" => "Approved");
$dalTableForm2023["LowerThirds"] = array("type"=>200,"varname"=>"LowerThirds", "name" => "LowerThirds");
$dalTableForm2023["ParadeOrder"] = array("type"=>200,"varname"=>"ParadeOrder", "name" => "ParadeOrder");
$dalTableForm2023["GroupOrganizer"] = array("type"=>200,"varname"=>"GroupOrganizer", "name" => "GroupOrganizer");
$dalTableForm2023["AgreetoRules"] = array("type"=>3,"varname"=>"AgreetoRules", "name" => "AgreetoRules");
$dalTableForm2023["LiabilityWavierSigned"] = array("type"=>3,"varname"=>"LiabilityWavierSigned", "name" => "LiabilityWavierSigned");
	$dalTableForm2023["ID"]["key"]=true;

$dal_info["vetdaybakchnl_paradesignup_at_server_lcsworld_com__Form2023"] = &$dalTableForm2023;
?>