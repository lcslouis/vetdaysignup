<?php
$dalTableForm_2019 = array();
$dalTableForm_2019["ID"] = array("type"=>3,"varname"=>"ID", "name" => "ID");
$dalTableForm_2019["NameOfOrg"] = array("type"=>201,"varname"=>"NameOfOrg", "name" => "NameOfOrg");
$dalTableForm_2019["TypeOfOrg"] = array("type"=>3,"varname"=>"TypeOfOrg", "name" => "TypeOfOrg");
$dalTableForm_2019["SchoolClass"] = array("type"=>3,"varname"=>"SchoolClass", "name" => "SchoolClass");
$dalTableForm_2019["EntryType"] = array("type"=>200,"varname"=>"EntryType", "name" => "EntryType");
$dalTableForm_2019["KeyPeople"] = array("type"=>201,"varname"=>"KeyPeople", "name" => "KeyPeople");
$dalTableForm_2019["EntrySize"] = array("type"=>200,"varname"=>"EntrySize", "name" => "EntrySize");
$dalTableForm_2019["ContactName"] = array("type"=>200,"varname"=>"ContactName", "name" => "ContactName");
$dalTableForm_2019["ContactAddress"] = array("type"=>200,"varname"=>"ContactAddress", "name" => "ContactAddress");
$dalTableForm_2019["ContactEmail"] = array("type"=>200,"varname"=>"ContactEmail", "name" => "ContactEmail");
$dalTableForm_2019["ContactZip"] = array("type"=>200,"varname"=>"ContactZip", "name" => "ContactZip");
$dalTableForm_2019["ContactPhone"] = array("type"=>200,"varname"=>"ContactPhone", "name" => "ContactPhone");
$dalTableForm_2019["ContactFax"] = array("type"=>200,"varname"=>"ContactFax", "name" => "ContactFax");
$dalTableForm_2019["ContactCell"] = array("type"=>200,"varname"=>"ContactCell", "name" => "ContactCell");
$dalTableForm_2019["DescriptionOfEntry"] = array("type"=>201,"varname"=>"DescriptionOfEntry", "name" => "DescriptionOfEntry");
$dalTableForm_2019["UID"] = array("type"=>3,"varname"=>"UID", "name" => "UID");
$dalTableForm_2019["Approved"] = array("type"=>16,"varname"=>"Approved", "name" => "Approved");
$dalTableForm_2019["NumOfHorses"] = array("type"=>3,"varname"=>"NumOfHorses", "name" => "NumOfHorses");
$dalTableForm_2019["LowerThirds"] = array("type"=>200,"varname"=>"LowerThirds", "name" => "LowerThirds");
$dalTableForm_2019["ParadeOrder"] = array("type"=>200,"varname"=>"ParadeOrder", "name" => "ParadeOrder");
	$dalTableForm_2019["ID"]["key"]=true;

$dal_info["vetdaybakchnl_paradesignup_at_server_lcsworld_com__Form_2019"] = &$dalTableForm_2019;
?>