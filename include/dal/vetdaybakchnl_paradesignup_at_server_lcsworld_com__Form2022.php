<?php
$dalTableForm2022 = array();
$dalTableForm2022["ID"] = array("type"=>3,"varname"=>"ID", "name" => "ID");
$dalTableForm2022["NameOfOrg"] = array("type"=>201,"varname"=>"NameOfOrg", "name" => "NameOfOrg");
$dalTableForm2022["TypeOfOrg"] = array("type"=>3,"varname"=>"TypeOfOrg", "name" => "TypeOfOrg");
$dalTableForm2022["SchoolClass"] = array("type"=>3,"varname"=>"SchoolClass", "name" => "SchoolClass");
$dalTableForm2022["EntryType"] = array("type"=>200,"varname"=>"EntryType", "name" => "EntryType");
$dalTableForm2022["KeyPeople"] = array("type"=>201,"varname"=>"KeyPeople", "name" => "KeyPeople");
$dalTableForm2022["EntrySize"] = array("type"=>200,"varname"=>"EntrySize", "name" => "EntrySize");
$dalTableForm2022["ContactName"] = array("type"=>200,"varname"=>"ContactName", "name" => "ContactName");
$dalTableForm2022["ContactAddress"] = array("type"=>200,"varname"=>"ContactAddress", "name" => "ContactAddress");
$dalTableForm2022["ContactEmail"] = array("type"=>200,"varname"=>"ContactEmail", "name" => "ContactEmail");
$dalTableForm2022["ContactZip"] = array("type"=>200,"varname"=>"ContactZip", "name" => "ContactZip");
$dalTableForm2022["ContactPhone"] = array("type"=>200,"varname"=>"ContactPhone", "name" => "ContactPhone");
$dalTableForm2022["ContactFax"] = array("type"=>200,"varname"=>"ContactFax", "name" => "ContactFax");
$dalTableForm2022["ContactCell"] = array("type"=>200,"varname"=>"ContactCell", "name" => "ContactCell");
$dalTableForm2022["DescriptionOfEntry"] = array("type"=>201,"varname"=>"DescriptionOfEntry", "name" => "DescriptionOfEntry");
$dalTableForm2022["UID"] = array("type"=>3,"varname"=>"UID", "name" => "UID");
$dalTableForm2022["NumOfHorses"] = array("type"=>3,"varname"=>"NumOfHorses", "name" => "NumOfHorses");
$dalTableForm2022["Approved"] = array("type"=>3,"varname"=>"Approved", "name" => "Approved");
$dalTableForm2022["LowerThirds"] = array("type"=>200,"varname"=>"LowerThirds", "name" => "LowerThirds");
$dalTableForm2022["ParadeOrder"] = array("type"=>200,"varname"=>"ParadeOrder", "name" => "ParadeOrder");
$dalTableForm2022["GroupOrganizer"] = array("type"=>200,"varname"=>"GroupOrganizer", "name" => "GroupOrganizer");
$dalTableForm2022["AgreetoRules"] = array("type"=>3,"varname"=>"AgreetoRules", "name" => "AgreetoRules");
$dalTableForm2022["LiabilityWavierSigned"] = array("type"=>3,"varname"=>"LiabilityWavierSigned", "name" => "LiabilityWavierSigned");

$dal_info["vetdaybakchnl_paradesignup_at_server_lcsworld_com__Form2022"] = &$dalTableForm2022;
?>