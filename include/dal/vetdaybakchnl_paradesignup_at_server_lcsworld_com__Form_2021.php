<?php
$dalTableForm_2021 = array();
$dalTableForm_2021["ID"] = array("type"=>3,"varname"=>"ID", "name" => "ID");
$dalTableForm_2021["NameOfOrg"] = array("type"=>201,"varname"=>"NameOfOrg", "name" => "NameOfOrg");
$dalTableForm_2021["TypeOfOrg"] = array("type"=>3,"varname"=>"TypeOfOrg", "name" => "TypeOfOrg");
$dalTableForm_2021["SchoolClass"] = array("type"=>3,"varname"=>"SchoolClass", "name" => "SchoolClass");
$dalTableForm_2021["EntryType"] = array("type"=>200,"varname"=>"EntryType", "name" => "EntryType");
$dalTableForm_2021["KeyPeople"] = array("type"=>201,"varname"=>"KeyPeople", "name" => "KeyPeople");
$dalTableForm_2021["EntrySize"] = array("type"=>200,"varname"=>"EntrySize", "name" => "EntrySize");
$dalTableForm_2021["ContactName"] = array("type"=>200,"varname"=>"ContactName", "name" => "ContactName");
$dalTableForm_2021["ContactAddress"] = array("type"=>200,"varname"=>"ContactAddress", "name" => "ContactAddress");
$dalTableForm_2021["ContactEmail"] = array("type"=>200,"varname"=>"ContactEmail", "name" => "ContactEmail");
$dalTableForm_2021["ContactZip"] = array("type"=>200,"varname"=>"ContactZip", "name" => "ContactZip");
$dalTableForm_2021["ContactPhone"] = array("type"=>200,"varname"=>"ContactPhone", "name" => "ContactPhone");
$dalTableForm_2021["ContactFax"] = array("type"=>200,"varname"=>"ContactFax", "name" => "ContactFax");
$dalTableForm_2021["ContactCell"] = array("type"=>200,"varname"=>"ContactCell", "name" => "ContactCell");
$dalTableForm_2021["DescriptionOfEntry"] = array("type"=>201,"varname"=>"DescriptionOfEntry", "name" => "DescriptionOfEntry");
$dalTableForm_2021["UID"] = array("type"=>3,"varname"=>"UID", "name" => "UID");
$dalTableForm_2021["NumOfHorses"] = array("type"=>3,"varname"=>"NumOfHorses", "name" => "NumOfHorses");
$dalTableForm_2021["Approved"] = array("type"=>3,"varname"=>"Approved", "name" => "Approved");
$dalTableForm_2021["LowerThirds"] = array("type"=>200,"varname"=>"LowerThirds", "name" => "LowerThirds");
$dalTableForm_2021["ParadeOrder"] = array("type"=>200,"varname"=>"ParadeOrder", "name" => "ParadeOrder");
$dalTableForm_2021["GroupOrganizer"] = array("type"=>200,"varname"=>"GroupOrganizer", "name" => "GroupOrganizer");
	$dalTableForm_2021["ID"]["key"]=true;

$dal_info["vetdaybakchnl_paradesignup_at_server_lcsworld_com__Form_2021"] = &$dalTableForm_2021;
?>