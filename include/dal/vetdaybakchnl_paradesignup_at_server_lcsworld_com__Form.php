<?php
$dalTableForm = array();
$dalTableForm["ID"] = array("type"=>3,"varname"=>"ID", "name" => "ID");
$dalTableForm["NameOfOrg"] = array("type"=>201,"varname"=>"NameOfOrg", "name" => "NameOfOrg");
$dalTableForm["TypeOfOrg"] = array("type"=>3,"varname"=>"TypeOfOrg", "name" => "TypeOfOrg");
$dalTableForm["SchoolClass"] = array("type"=>3,"varname"=>"SchoolClass", "name" => "SchoolClass");
$dalTableForm["EntryType"] = array("type"=>200,"varname"=>"EntryType", "name" => "EntryType");
$dalTableForm["KeyPeople"] = array("type"=>201,"varname"=>"KeyPeople", "name" => "KeyPeople");
$dalTableForm["EntrySize"] = array("type"=>200,"varname"=>"EntrySize", "name" => "EntrySize");
$dalTableForm["ContactName"] = array("type"=>200,"varname"=>"ContactName", "name" => "ContactName");
$dalTableForm["ContactAddress"] = array("type"=>200,"varname"=>"ContactAddress", "name" => "ContactAddress");
$dalTableForm["ContactEmail"] = array("type"=>200,"varname"=>"ContactEmail", "name" => "ContactEmail");
$dalTableForm["ContactZip"] = array("type"=>200,"varname"=>"ContactZip", "name" => "ContactZip");
$dalTableForm["ContactPhone"] = array("type"=>200,"varname"=>"ContactPhone", "name" => "ContactPhone");
$dalTableForm["ContactFax"] = array("type"=>200,"varname"=>"ContactFax", "name" => "ContactFax");
$dalTableForm["ContactCell"] = array("type"=>200,"varname"=>"ContactCell", "name" => "ContactCell");
$dalTableForm["DescriptionOfEntry"] = array("type"=>201,"varname"=>"DescriptionOfEntry", "name" => "DescriptionOfEntry");
$dalTableForm["UID"] = array("type"=>3,"varname"=>"UID", "name" => "UID");
$dalTableForm["NumOfHorses"] = array("type"=>3,"varname"=>"NumOfHorses", "name" => "NumOfHorses");
$dalTableForm["Approved"] = array("type"=>3,"varname"=>"Approved", "name" => "Approved");
$dalTableForm["LowerThirds"] = array("type"=>200,"varname"=>"LowerThirds", "name" => "LowerThirds");
$dalTableForm["ParadeOrder"] = array("type"=>200,"varname"=>"ParadeOrder", "name" => "ParadeOrder");
$dalTableForm["GroupOrganizer"] = array("type"=>200,"varname"=>"GroupOrganizer", "name" => "GroupOrganizer");
$dalTableForm["AgreetoRules"] = array("type"=>3,"varname"=>"AgreetoRules", "name" => "AgreetoRules");
$dalTableForm["LiabilityWavierSigned"] = array("type"=>3,"varname"=>"LiabilityWavierSigned", "name" => "LiabilityWavierSigned");
	$dalTableForm["ID"]["key"]=true;

$dal_info["vetdaybakchnl_paradesignup_at_server_lcsworld_com__Form"] = &$dalTableForm;
?>