<?php
$tdataannouncer2021 = array();
$tdataannouncer2021[".searchableFields"] = array();
$tdataannouncer2021[".ShortName"] = "announcer2021";
$tdataannouncer2021[".OwnerID"] = "";
$tdataannouncer2021[".OriginalTable"] = "announcer2021";


$tdataannouncer2021[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataannouncer2021[".originalPagesByType"] = $tdataannouncer2021[".pagesByType"];
$tdataannouncer2021[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataannouncer2021[".originalPages"] = $tdataannouncer2021[".pages"];
$tdataannouncer2021[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataannouncer2021[".originalDefaultPages"] = $tdataannouncer2021[".defaultPages"];

//	field labels
$fieldLabelsannouncer2021 = array();
$fieldToolTipsannouncer2021 = array();
$pageTitlesannouncer2021 = array();
$placeHoldersannouncer2021 = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsannouncer2021["English"] = array();
	$fieldToolTipsannouncer2021["English"] = array();
	$placeHoldersannouncer2021["English"] = array();
	$pageTitlesannouncer2021["English"] = array();
	$fieldLabelsannouncer2021["English"]["DescriptionOfEntry"] = "Description Of Entry";
	$fieldToolTipsannouncer2021["English"]["DescriptionOfEntry"] = "";
	$placeHoldersannouncer2021["English"]["DescriptionOfEntry"] = "";
	$fieldLabelsannouncer2021["English"]["FormID"] = "Form ID";
	$fieldToolTipsannouncer2021["English"]["FormID"] = "";
	$placeHoldersannouncer2021["English"]["FormID"] = "";
	$fieldLabelsannouncer2021["English"]["ID"] = "ID";
	$fieldToolTipsannouncer2021["English"]["ID"] = "";
	$placeHoldersannouncer2021["English"]["ID"] = "";
	$fieldLabelsannouncer2021["English"]["KeyPeople"] = "Key People";
	$fieldToolTipsannouncer2021["English"]["KeyPeople"] = "";
	$placeHoldersannouncer2021["English"]["KeyPeople"] = "";
	$fieldLabelsannouncer2021["English"]["LowerThirds"] = "Lower Thirds";
	$fieldToolTipsannouncer2021["English"]["LowerThirds"] = "";
	$placeHoldersannouncer2021["English"]["LowerThirds"] = "";
	$fieldLabelsannouncer2021["English"]["NameOfOrg"] = "Name Of Org";
	$fieldToolTipsannouncer2021["English"]["NameOfOrg"] = "";
	$placeHoldersannouncer2021["English"]["NameOfOrg"] = "";
	if (count($fieldToolTipsannouncer2021["English"]))
		$tdataannouncer2021[".isUseToolTips"] = true;
}


	$tdataannouncer2021[".NCSearch"] = true;



$tdataannouncer2021[".shortTableName"] = "announcer2021";
$tdataannouncer2021[".nSecOptions"] = 0;

$tdataannouncer2021[".mainTableOwnerID"] = "";
$tdataannouncer2021[".entityType"] = 0;
$tdataannouncer2021[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdataannouncer2021[".strOriginalTableName"] = "announcer2021";

		 



$tdataannouncer2021[".showAddInPopup"] = false;

$tdataannouncer2021[".showEditInPopup"] = false;

$tdataannouncer2021[".showViewInPopup"] = false;

$tdataannouncer2021[".listAjax"] = false;
//	temporary
//$tdataannouncer2021[".listAjax"] = false;

	$tdataannouncer2021[".audit"] = false;

	$tdataannouncer2021[".locking"] = false;


$pages = $tdataannouncer2021[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataannouncer2021[".edit"] = true;
	$tdataannouncer2021[".afterEditAction"] = 1;
	$tdataannouncer2021[".closePopupAfterEdit"] = 1;
	$tdataannouncer2021[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataannouncer2021[".add"] = true;
$tdataannouncer2021[".afterAddAction"] = 1;
$tdataannouncer2021[".closePopupAfterAdd"] = 1;
$tdataannouncer2021[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataannouncer2021[".list"] = true;
}



$tdataannouncer2021[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataannouncer2021[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataannouncer2021[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataannouncer2021[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataannouncer2021[".printFriendly"] = true;
}



$tdataannouncer2021[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataannouncer2021[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataannouncer2021[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataannouncer2021[".isUseAjaxSuggest"] = true;

$tdataannouncer2021[".rowHighlite"] = true;



						

$tdataannouncer2021[".ajaxCodeSnippetAdded"] = false;

$tdataannouncer2021[".buttonsAdded"] = false;

$tdataannouncer2021[".addPageEvents"] = false;

// use timepicker for search panel
$tdataannouncer2021[".isUseTimeForSearch"] = false;


$tdataannouncer2021[".badgeColor"] = "BC8F8F";


$tdataannouncer2021[".allSearchFields"] = array();
$tdataannouncer2021[".filterFields"] = array();
$tdataannouncer2021[".requiredSearchFields"] = array();

$tdataannouncer2021[".googleLikeFields"] = array();
$tdataannouncer2021[".googleLikeFields"][] = "ID";
$tdataannouncer2021[".googleLikeFields"][] = "FormID";
$tdataannouncer2021[".googleLikeFields"][] = "NameOfOrg";
$tdataannouncer2021[".googleLikeFields"][] = "KeyPeople";
$tdataannouncer2021[".googleLikeFields"][] = "DescriptionOfEntry";
$tdataannouncer2021[".googleLikeFields"][] = "LowerThirds";



$tdataannouncer2021[".tableType"] = "list";

$tdataannouncer2021[".printerPageOrientation"] = 0;
$tdataannouncer2021[".nPrinterPageScale"] = 100;

$tdataannouncer2021[".nPrinterSplitRecords"] = 40;

$tdataannouncer2021[".geocodingEnabled"] = false;










$tdataannouncer2021[".pageSize"] = 20;

$tdataannouncer2021[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdataannouncer2021[".strOrderBy"] = $tstrOrderBy;

$tdataannouncer2021[".orderindexes"] = array();


$tdataannouncer2021[".sqlHead"] = "SELECT ID,  	FormID,  	NameOfOrg,  	KeyPeople,  	DescriptionOfEntry,  	LowerThirds";
$tdataannouncer2021[".sqlFrom"] = "FROM announcer2021";
$tdataannouncer2021[".sqlWhereExpr"] = "";
$tdataannouncer2021[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataannouncer2021[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataannouncer2021[".arrGroupsPerPage"] = $arrGPP;

$tdataannouncer2021[".highlightSearchResults"] = true;

$tableKeysannouncer2021 = array();
$tableKeysannouncer2021[] = "ID";
$tdataannouncer2021[".Keys"] = $tableKeysannouncer2021;


$tdataannouncer2021[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "announcer2021";
	$fdata["Label"] = GetFieldLabel("announcer2021","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

		$fdata["sourceSingle"] = "ID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2021["ID"] = $fdata;
		$tdataannouncer2021[".searchableFields"][] = "ID";
//	FormID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "FormID";
	$fdata["GoodName"] = "FormID";
	$fdata["ownerTable"] = "announcer2021";
	$fdata["Label"] = GetFieldLabel("announcer2021","FormID");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "FormID";

		$fdata["sourceSingle"] = "FormID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "FormID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2021["FormID"] = $fdata;
		$tdataannouncer2021[".searchableFields"][] = "FormID";
//	NameOfOrg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "NameOfOrg";
	$fdata["GoodName"] = "NameOfOrg";
	$fdata["ownerTable"] = "announcer2021";
	$fdata["Label"] = GetFieldLabel("announcer2021","NameOfOrg");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "NameOfOrg";

		$fdata["sourceSingle"] = "NameOfOrg";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NameOfOrg";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2021["NameOfOrg"] = $fdata;
		$tdataannouncer2021[".searchableFields"][] = "NameOfOrg";
//	KeyPeople
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "KeyPeople";
	$fdata["GoodName"] = "KeyPeople";
	$fdata["ownerTable"] = "announcer2021";
	$fdata["Label"] = GetFieldLabel("announcer2021","KeyPeople");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "KeyPeople";

		$fdata["sourceSingle"] = "KeyPeople";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "KeyPeople";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2021["KeyPeople"] = $fdata;
		$tdataannouncer2021[".searchableFields"][] = "KeyPeople";
//	DescriptionOfEntry
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "DescriptionOfEntry";
	$fdata["GoodName"] = "DescriptionOfEntry";
	$fdata["ownerTable"] = "announcer2021";
	$fdata["Label"] = GetFieldLabel("announcer2021","DescriptionOfEntry");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "DescriptionOfEntry";

		$fdata["sourceSingle"] = "DescriptionOfEntry";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "DescriptionOfEntry";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2021["DescriptionOfEntry"] = $fdata;
		$tdataannouncer2021[".searchableFields"][] = "DescriptionOfEntry";
//	LowerThirds
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "LowerThirds";
	$fdata["GoodName"] = "LowerThirds";
	$fdata["ownerTable"] = "announcer2021";
	$fdata["Label"] = GetFieldLabel("announcer2021","LowerThirds");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "LowerThirds";

		$fdata["sourceSingle"] = "LowerThirds";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "LowerThirds";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2021["LowerThirds"] = $fdata;
		$tdataannouncer2021[".searchableFields"][] = "LowerThirds";


$tables_data["announcer2021"]=&$tdataannouncer2021;
$field_labels["announcer2021"] = &$fieldLabelsannouncer2021;
$fieldToolTips["announcer2021"] = &$fieldToolTipsannouncer2021;
$placeHolders["announcer2021"] = &$placeHoldersannouncer2021;
$page_titles["announcer2021"] = &$pageTitlesannouncer2021;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["announcer2021"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["announcer2021"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_announcer2021()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	FormID,  	NameOfOrg,  	KeyPeople,  	DescriptionOfEntry,  	LowerThirds";
$proto0["m_strFrom"] = "FROM announcer2021";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "announcer2021",
	"m_srcTableName" => "announcer2021"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "announcer2021";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "FormID",
	"m_strTable" => "announcer2021",
	"m_srcTableName" => "announcer2021"
));

$proto8["m_sql"] = "FormID";
$proto8["m_srcTableName"] = "announcer2021";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "NameOfOrg",
	"m_strTable" => "announcer2021",
	"m_srcTableName" => "announcer2021"
));

$proto10["m_sql"] = "NameOfOrg";
$proto10["m_srcTableName"] = "announcer2021";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "KeyPeople",
	"m_strTable" => "announcer2021",
	"m_srcTableName" => "announcer2021"
));

$proto12["m_sql"] = "KeyPeople";
$proto12["m_srcTableName"] = "announcer2021";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "DescriptionOfEntry",
	"m_strTable" => "announcer2021",
	"m_srcTableName" => "announcer2021"
));

$proto14["m_sql"] = "DescriptionOfEntry";
$proto14["m_srcTableName"] = "announcer2021";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "LowerThirds",
	"m_strTable" => "announcer2021",
	"m_srcTableName" => "announcer2021"
));

$proto16["m_sql"] = "LowerThirds";
$proto16["m_srcTableName"] = "announcer2021";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto18=array();
$proto18["m_link"] = "SQLL_MAIN";
			$proto19=array();
$proto19["m_strName"] = "announcer2021";
$proto19["m_srcTableName"] = "announcer2021";
$proto19["m_columns"] = array();
$proto19["m_columns"][] = "ID";
$proto19["m_columns"][] = "FormID";
$proto19["m_columns"][] = "NameOfOrg";
$proto19["m_columns"][] = "KeyPeople";
$proto19["m_columns"][] = "DescriptionOfEntry";
$proto19["m_columns"][] = "LowerThirds";
$obj = new SQLTable($proto19);

$proto18["m_table"] = $obj;
$proto18["m_sql"] = "announcer2021";
$proto18["m_alias"] = "";
$proto18["m_srcTableName"] = "announcer2021";
$proto20=array();
$proto20["m_sql"] = "";
$proto20["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto20["m_column"]=$obj;
$proto20["m_contained"] = array();
$proto20["m_strCase"] = "";
$proto20["m_havingmode"] = false;
$proto20["m_inBrackets"] = false;
$proto20["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto20);

$proto18["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto18);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="announcer2021";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_announcer2021 = createSqlQuery_announcer2021();


	
					
;

						

$tdataannouncer2021[".sqlquery"] = $queryData_announcer2021;



$tableEvents["announcer2021"] = new eventsBase;
$tdataannouncer2021[".hasEvents"] = false;

?>