<?php
$tdatafrmparadeorder = array();
$tdatafrmparadeorder[".searchableFields"] = array();
$tdatafrmparadeorder[".ShortName"] = "frmparadeorder";
$tdatafrmparadeorder[".OwnerID"] = "UID";
$tdatafrmparadeorder[".OriginalTable"] = "Form";


$tdatafrmparadeorder[".pagesByType"] = my_json_decode( "{\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatafrmparadeorder[".originalPagesByType"] = $tdatafrmparadeorder[".pagesByType"];
$tdatafrmparadeorder[".pages"] = types2pages( my_json_decode( "{\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatafrmparadeorder[".originalPages"] = $tdatafrmparadeorder[".pages"];
$tdatafrmparadeorder[".defaultPages"] = my_json_decode( "{\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatafrmparadeorder[".originalDefaultPages"] = $tdatafrmparadeorder[".defaultPages"];

//	field labels
$fieldLabelsfrmparadeorder = array();
$fieldToolTipsfrmparadeorder = array();
$pageTitlesfrmparadeorder = array();
$placeHoldersfrmparadeorder = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsfrmparadeorder["English"] = array();
	$fieldToolTipsfrmparadeorder["English"] = array();
	$placeHoldersfrmparadeorder["English"] = array();
	$pageTitlesfrmparadeorder["English"] = array();
	$fieldLabelsfrmparadeorder["English"]["ID"] = "Entry ID: ";
	$fieldToolTipsfrmparadeorder["English"]["ID"] = "";
	$placeHoldersfrmparadeorder["English"]["ID"] = "";
	$fieldLabelsfrmparadeorder["English"]["NameOfOrg"] = "Organization";
	$fieldToolTipsfrmparadeorder["English"]["NameOfOrg"] = "";
	$placeHoldersfrmparadeorder["English"]["NameOfOrg"] = "";
	$fieldLabelsfrmparadeorder["English"]["ParadeOrder"] = "Parade Order";
	$fieldToolTipsfrmparadeorder["English"]["ParadeOrder"] = "";
	$placeHoldersfrmparadeorder["English"]["ParadeOrder"] = "";
	if (count($fieldToolTipsfrmparadeorder["English"]))
		$tdatafrmparadeorder[".isUseToolTips"] = true;
}


	$tdatafrmparadeorder[".NCSearch"] = true;



$tdatafrmparadeorder[".shortTableName"] = "frmparadeorder";
$tdatafrmparadeorder[".nSecOptions"] = 0;

$tdatafrmparadeorder[".mainTableOwnerID"] = "UID";
$tdatafrmparadeorder[".entityType"] = 1;
$tdatafrmparadeorder[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdatafrmparadeorder[".strOriginalTableName"] = "Form";

		 



$tdatafrmparadeorder[".showAddInPopup"] = false;

$tdatafrmparadeorder[".showEditInPopup"] = false;

$tdatafrmparadeorder[".showViewInPopup"] = false;

$tdatafrmparadeorder[".listAjax"] = false;
//	temporary
//$tdatafrmparadeorder[".listAjax"] = false;

	$tdatafrmparadeorder[".audit"] = false;

	$tdatafrmparadeorder[".locking"] = false;


$pages = $tdatafrmparadeorder[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatafrmparadeorder[".edit"] = true;
	$tdatafrmparadeorder[".afterEditAction"] = 1;
	$tdatafrmparadeorder[".closePopupAfterEdit"] = 1;
	$tdatafrmparadeorder[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatafrmparadeorder[".add"] = true;
$tdatafrmparadeorder[".afterAddAction"] = 1;
$tdatafrmparadeorder[".closePopupAfterAdd"] = 1;
$tdatafrmparadeorder[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatafrmparadeorder[".list"] = true;
}



$tdatafrmparadeorder[".strSortControlSettingsJSON"] = "";

$tdatafrmparadeorder[".strClickActionJSON"] = "{\"fields\":{\"Approved\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactAddress\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactCell\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactEmail\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactFax\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactName\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactPhone\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactZip\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"DescriptionOfEntry\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"EntrySize\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"EntryType\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ID\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"KeyPeople\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"NameOfOrg\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"OnSiteCheckIn\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"SchoolClass\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"TypeOfOrg\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"UID\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}}},\"row\":{\"action\":\"open\",\"codeData\":{},\"gridData\":{\"action\":\"checkbox\",\"table\":null},\"openData\":{\"how\":\"goto\",\"page\":\"view\",\"table\":null,\"url\":\"\"}}}";



if( $pages[PAGE_VIEW] ) {
$tdatafrmparadeorder[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatafrmparadeorder[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatafrmparadeorder[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatafrmparadeorder[".printFriendly"] = true;
}



$tdatafrmparadeorder[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatafrmparadeorder[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatafrmparadeorder[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatafrmparadeorder[".isUseAjaxSuggest"] = true;

$tdatafrmparadeorder[".rowHighlite"] = true;



						

$tdatafrmparadeorder[".ajaxCodeSnippetAdded"] = false;

$tdatafrmparadeorder[".buttonsAdded"] = false;

$tdatafrmparadeorder[".addPageEvents"] = false;

// use timepicker for search panel
$tdatafrmparadeorder[".isUseTimeForSearch"] = false;


$tdatafrmparadeorder[".badgeColor"] = "CD853F";


$tdatafrmparadeorder[".allSearchFields"] = array();
$tdatafrmparadeorder[".filterFields"] = array();
$tdatafrmparadeorder[".requiredSearchFields"] = array();

$tdatafrmparadeorder[".googleLikeFields"] = array();
$tdatafrmparadeorder[".googleLikeFields"][] = "ParadeOrder";
$tdatafrmparadeorder[".googleLikeFields"][] = "NameOfOrg";
$tdatafrmparadeorder[".googleLikeFields"][] = "ID";



$tdatafrmparadeorder[".tableType"] = "list";

$tdatafrmparadeorder[".printerPageOrientation"] = 0;
$tdatafrmparadeorder[".nPrinterPageScale"] = 100;

$tdatafrmparadeorder[".nPrinterSplitRecords"] = 16;

$tdatafrmparadeorder[".geocodingEnabled"] = false;










$tdatafrmparadeorder[".pageSize"] = 20;

$tdatafrmparadeorder[".warnLeavingPages"] = true;

$tdatafrmparadeorder[".hideEmptyFieldsOnView"] = true;


$tstrOrderBy = "ORDER BY ParadeOrder";
$tdatafrmparadeorder[".strOrderBy"] = $tstrOrderBy;

$tdatafrmparadeorder[".orderindexes"] = array();
	$tdatafrmparadeorder[".orderindexes"][] = array(1, (1 ? "ASC" : "DESC"), "ParadeOrder");



$tdatafrmparadeorder[".sqlHead"] = "SELECT ParadeOrder,  NameOfOrg,  ID";
$tdatafrmparadeorder[".sqlFrom"] = "FROM Form";
$tdatafrmparadeorder[".sqlWhereExpr"] = "(Approved =1)";
$tdatafrmparadeorder[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatafrmparadeorder[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatafrmparadeorder[".arrGroupsPerPage"] = $arrGPP;

$tdatafrmparadeorder[".highlightSearchResults"] = true;

$tableKeysfrmparadeorder = array();
$tableKeysfrmparadeorder[] = "ID";
$tdatafrmparadeorder[".Keys"] = $tableKeysfrmparadeorder;


$tdatafrmparadeorder[".hideMobileList"] = array();




//	ParadeOrder
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ParadeOrder";
	$fdata["GoodName"] = "ParadeOrder";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("FrmParadeOrder","ParadeOrder");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ParadeOrder";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ParadeOrder";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatafrmparadeorder["ParadeOrder"] = $fdata;
		$tdatafrmparadeorder[".searchableFields"][] = "ParadeOrder";
//	NameOfOrg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "NameOfOrg";
	$fdata["GoodName"] = "NameOfOrg";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("FrmParadeOrder","NameOfOrg");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "NameOfOrg";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NameOfOrg";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatafrmparadeorder["NameOfOrg"] = $fdata;
		$tdatafrmparadeorder[".searchableFields"][] = "NameOfOrg";
//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("FrmParadeOrder","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatafrmparadeorder["ID"] = $fdata;
		$tdatafrmparadeorder[".searchableFields"][] = "ID";


$tables_data["FrmParadeOrder"]=&$tdatafrmparadeorder;
$field_labels["FrmParadeOrder"] = &$fieldLabelsfrmparadeorder;
$fieldToolTips["FrmParadeOrder"] = &$fieldToolTipsfrmparadeorder;
$placeHolders["FrmParadeOrder"] = &$placeHoldersfrmparadeorder;
$page_titles["FrmParadeOrder"] = &$pageTitlesfrmparadeorder;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["FrmParadeOrder"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["FrmParadeOrder"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_frmparadeorder()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ParadeOrder,  NameOfOrg,  ID";
$proto0["m_strFrom"] = "FROM Form";
$proto0["m_strWhere"] = "(Approved =1)";
$proto0["m_strOrderBy"] = "ORDER BY ParadeOrder";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "Approved =1";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "Approved",
	"m_strTable" => "Form",
	"m_srcTableName" => "FrmParadeOrder"
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "=1";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ParadeOrder",
	"m_strTable" => "Form",
	"m_srcTableName" => "FrmParadeOrder"
));

$proto6["m_sql"] = "ParadeOrder";
$proto6["m_srcTableName"] = "FrmParadeOrder";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "NameOfOrg",
	"m_strTable" => "Form",
	"m_srcTableName" => "FrmParadeOrder"
));

$proto8["m_sql"] = "NameOfOrg";
$proto8["m_srcTableName"] = "FrmParadeOrder";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "Form",
	"m_srcTableName" => "FrmParadeOrder"
));

$proto10["m_sql"] = "ID";
$proto10["m_srcTableName"] = "FrmParadeOrder";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto12=array();
$proto12["m_link"] = "SQLL_MAIN";
			$proto13=array();
$proto13["m_strName"] = "Form";
$proto13["m_srcTableName"] = "FrmParadeOrder";
$proto13["m_columns"] = array();
$proto13["m_columns"][] = "ID";
$proto13["m_columns"][] = "NameOfOrg";
$proto13["m_columns"][] = "TypeOfOrg";
$proto13["m_columns"][] = "SchoolClass";
$proto13["m_columns"][] = "EntryType";
$proto13["m_columns"][] = "KeyPeople";
$proto13["m_columns"][] = "EntrySize";
$proto13["m_columns"][] = "ContactName";
$proto13["m_columns"][] = "ContactAddress";
$proto13["m_columns"][] = "ContactEmail";
$proto13["m_columns"][] = "ContactZip";
$proto13["m_columns"][] = "ContactPhone";
$proto13["m_columns"][] = "ContactFax";
$proto13["m_columns"][] = "ContactCell";
$proto13["m_columns"][] = "DescriptionOfEntry";
$proto13["m_columns"][] = "UID";
$proto13["m_columns"][] = "NumOfHorses";
$proto13["m_columns"][] = "Approved";
$proto13["m_columns"][] = "LowerThirds";
$proto13["m_columns"][] = "ParadeOrder";
$proto13["m_columns"][] = "GroupOrganizer";
$proto13["m_columns"][] = "AgreetoRules";
$proto13["m_columns"][] = "LiabilityWavierSigned";
$obj = new SQLTable($proto13);

$proto12["m_table"] = $obj;
$proto12["m_sql"] = "Form";
$proto12["m_alias"] = "";
$proto12["m_srcTableName"] = "FrmParadeOrder";
$proto14=array();
$proto14["m_sql"] = "";
$proto14["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto14["m_column"]=$obj;
$proto14["m_contained"] = array();
$proto14["m_strCase"] = "";
$proto14["m_havingmode"] = false;
$proto14["m_inBrackets"] = false;
$proto14["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto14);

$proto12["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto12);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto16=array();
						$obj = new SQLField(array(
	"m_strName" => "ParadeOrder",
	"m_strTable" => "Form",
	"m_srcTableName" => "FrmParadeOrder"
));

$proto16["m_column"]=$obj;
$proto16["m_bAsc"] = 1;
$proto16["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto16);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="FrmParadeOrder";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_frmparadeorder = createSqlQuery_frmparadeorder();


	
					
;

			

$tdatafrmparadeorder[".sqlquery"] = $queryData_frmparadeorder;



include_once(getabspath("include/frmparadeorder_events.php"));
$tableEvents["FrmParadeOrder"] = new eventclass_frmparadeorder;
$tdatafrmparadeorder[".hasEvents"] = true;

?>