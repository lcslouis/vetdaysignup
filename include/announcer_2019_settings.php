<?php
$tdataannouncer_2019 = array();
$tdataannouncer_2019[".searchableFields"] = array();
$tdataannouncer_2019[".ShortName"] = "announcer_2019";
$tdataannouncer_2019[".OwnerID"] = "";
$tdataannouncer_2019[".OriginalTable"] = "announcer_2019";


$tdataannouncer_2019[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataannouncer_2019[".originalPagesByType"] = $tdataannouncer_2019[".pagesByType"];
$tdataannouncer_2019[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataannouncer_2019[".originalPages"] = $tdataannouncer_2019[".pages"];
$tdataannouncer_2019[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataannouncer_2019[".originalDefaultPages"] = $tdataannouncer_2019[".defaultPages"];

//	field labels
$fieldLabelsannouncer_2019 = array();
$fieldToolTipsannouncer_2019 = array();
$pageTitlesannouncer_2019 = array();
$placeHoldersannouncer_2019 = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsannouncer_2019["English"] = array();
	$fieldToolTipsannouncer_2019["English"] = array();
	$placeHoldersannouncer_2019["English"] = array();
	$pageTitlesannouncer_2019["English"] = array();
	$fieldLabelsannouncer_2019["English"]["DescriptionOfEntry"] = "Description Of Entry";
	$fieldToolTipsannouncer_2019["English"]["DescriptionOfEntry"] = "";
	$placeHoldersannouncer_2019["English"]["DescriptionOfEntry"] = "";
	$fieldLabelsannouncer_2019["English"]["FormID"] = "Form ID";
	$fieldToolTipsannouncer_2019["English"]["FormID"] = "";
	$placeHoldersannouncer_2019["English"]["FormID"] = "";
	$fieldLabelsannouncer_2019["English"]["ID"] = "ID";
	$fieldToolTipsannouncer_2019["English"]["ID"] = "";
	$placeHoldersannouncer_2019["English"]["ID"] = "";
	$fieldLabelsannouncer_2019["English"]["KeyPeople"] = "Key People";
	$fieldToolTipsannouncer_2019["English"]["KeyPeople"] = "";
	$placeHoldersannouncer_2019["English"]["KeyPeople"] = "";
	$fieldLabelsannouncer_2019["English"]["LowerThirds"] = "Lower Thirds";
	$fieldToolTipsannouncer_2019["English"]["LowerThirds"] = "";
	$placeHoldersannouncer_2019["English"]["LowerThirds"] = "";
	$fieldLabelsannouncer_2019["English"]["NameOfOrg"] = "Name Of Org";
	$fieldToolTipsannouncer_2019["English"]["NameOfOrg"] = "";
	$placeHoldersannouncer_2019["English"]["NameOfOrg"] = "";
	if (count($fieldToolTipsannouncer_2019["English"]))
		$tdataannouncer_2019[".isUseToolTips"] = true;
}


	$tdataannouncer_2019[".NCSearch"] = true;



$tdataannouncer_2019[".shortTableName"] = "announcer_2019";
$tdataannouncer_2019[".nSecOptions"] = 0;

$tdataannouncer_2019[".mainTableOwnerID"] = "";
$tdataannouncer_2019[".entityType"] = 0;
$tdataannouncer_2019[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdataannouncer_2019[".strOriginalTableName"] = "announcer_2019";

		 



$tdataannouncer_2019[".showAddInPopup"] = false;

$tdataannouncer_2019[".showEditInPopup"] = false;

$tdataannouncer_2019[".showViewInPopup"] = false;

$tdataannouncer_2019[".listAjax"] = false;
//	temporary
//$tdataannouncer_2019[".listAjax"] = false;

	$tdataannouncer_2019[".audit"] = false;

	$tdataannouncer_2019[".locking"] = false;


$pages = $tdataannouncer_2019[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataannouncer_2019[".edit"] = true;
	$tdataannouncer_2019[".afterEditAction"] = 1;
	$tdataannouncer_2019[".closePopupAfterEdit"] = 1;
	$tdataannouncer_2019[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataannouncer_2019[".add"] = true;
$tdataannouncer_2019[".afterAddAction"] = 1;
$tdataannouncer_2019[".closePopupAfterAdd"] = 1;
$tdataannouncer_2019[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataannouncer_2019[".list"] = true;
}



$tdataannouncer_2019[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataannouncer_2019[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataannouncer_2019[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataannouncer_2019[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataannouncer_2019[".printFriendly"] = true;
}



$tdataannouncer_2019[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataannouncer_2019[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataannouncer_2019[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataannouncer_2019[".isUseAjaxSuggest"] = true;

$tdataannouncer_2019[".rowHighlite"] = true;



						

$tdataannouncer_2019[".ajaxCodeSnippetAdded"] = false;

$tdataannouncer_2019[".buttonsAdded"] = false;

$tdataannouncer_2019[".addPageEvents"] = false;

// use timepicker for search panel
$tdataannouncer_2019[".isUseTimeForSearch"] = false;


$tdataannouncer_2019[".badgeColor"] = "7B68EE";


$tdataannouncer_2019[".allSearchFields"] = array();
$tdataannouncer_2019[".filterFields"] = array();
$tdataannouncer_2019[".requiredSearchFields"] = array();

$tdataannouncer_2019[".googleLikeFields"] = array();
$tdataannouncer_2019[".googleLikeFields"][] = "ID";
$tdataannouncer_2019[".googleLikeFields"][] = "FormID";
$tdataannouncer_2019[".googleLikeFields"][] = "NameOfOrg";
$tdataannouncer_2019[".googleLikeFields"][] = "KeyPeople";
$tdataannouncer_2019[".googleLikeFields"][] = "DescriptionOfEntry";
$tdataannouncer_2019[".googleLikeFields"][] = "LowerThirds";



$tdataannouncer_2019[".tableType"] = "list";

$tdataannouncer_2019[".printerPageOrientation"] = 0;
$tdataannouncer_2019[".nPrinterPageScale"] = 100;

$tdataannouncer_2019[".nPrinterSplitRecords"] = 40;

$tdataannouncer_2019[".geocodingEnabled"] = false;










$tdataannouncer_2019[".pageSize"] = 20;

$tdataannouncer_2019[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdataannouncer_2019[".strOrderBy"] = $tstrOrderBy;

$tdataannouncer_2019[".orderindexes"] = array();


$tdataannouncer_2019[".sqlHead"] = "SELECT ID,  	FormID,  	NameOfOrg,  	KeyPeople,  	DescriptionOfEntry,  	LowerThirds";
$tdataannouncer_2019[".sqlFrom"] = "FROM announcer_2019";
$tdataannouncer_2019[".sqlWhereExpr"] = "";
$tdataannouncer_2019[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataannouncer_2019[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataannouncer_2019[".arrGroupsPerPage"] = $arrGPP;

$tdataannouncer_2019[".highlightSearchResults"] = true;

$tableKeysannouncer_2019 = array();
$tableKeysannouncer_2019[] = "ID";
$tdataannouncer_2019[".Keys"] = $tableKeysannouncer_2019;


$tdataannouncer_2019[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "announcer_2019";
	$fdata["Label"] = GetFieldLabel("announcer_2019","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

		$fdata["sourceSingle"] = "ID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer_2019["ID"] = $fdata;
		$tdataannouncer_2019[".searchableFields"][] = "ID";
//	FormID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "FormID";
	$fdata["GoodName"] = "FormID";
	$fdata["ownerTable"] = "announcer_2019";
	$fdata["Label"] = GetFieldLabel("announcer_2019","FormID");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "FormID";

		$fdata["sourceSingle"] = "FormID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "FormID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer_2019["FormID"] = $fdata;
		$tdataannouncer_2019[".searchableFields"][] = "FormID";
//	NameOfOrg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "NameOfOrg";
	$fdata["GoodName"] = "NameOfOrg";
	$fdata["ownerTable"] = "announcer_2019";
	$fdata["Label"] = GetFieldLabel("announcer_2019","NameOfOrg");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "NameOfOrg";

		$fdata["sourceSingle"] = "NameOfOrg";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NameOfOrg";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer_2019["NameOfOrg"] = $fdata;
		$tdataannouncer_2019[".searchableFields"][] = "NameOfOrg";
//	KeyPeople
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "KeyPeople";
	$fdata["GoodName"] = "KeyPeople";
	$fdata["ownerTable"] = "announcer_2019";
	$fdata["Label"] = GetFieldLabel("announcer_2019","KeyPeople");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "KeyPeople";

		$fdata["sourceSingle"] = "KeyPeople";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "KeyPeople";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer_2019["KeyPeople"] = $fdata;
		$tdataannouncer_2019[".searchableFields"][] = "KeyPeople";
//	DescriptionOfEntry
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "DescriptionOfEntry";
	$fdata["GoodName"] = "DescriptionOfEntry";
	$fdata["ownerTable"] = "announcer_2019";
	$fdata["Label"] = GetFieldLabel("announcer_2019","DescriptionOfEntry");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "DescriptionOfEntry";

		$fdata["sourceSingle"] = "DescriptionOfEntry";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "DescriptionOfEntry";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer_2019["DescriptionOfEntry"] = $fdata;
		$tdataannouncer_2019[".searchableFields"][] = "DescriptionOfEntry";
//	LowerThirds
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "LowerThirds";
	$fdata["GoodName"] = "LowerThirds";
	$fdata["ownerTable"] = "announcer_2019";
	$fdata["Label"] = GetFieldLabel("announcer_2019","LowerThirds");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "LowerThirds";

		$fdata["sourceSingle"] = "LowerThirds";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "LowerThirds";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer_2019["LowerThirds"] = $fdata;
		$tdataannouncer_2019[".searchableFields"][] = "LowerThirds";


$tables_data["announcer_2019"]=&$tdataannouncer_2019;
$field_labels["announcer_2019"] = &$fieldLabelsannouncer_2019;
$fieldToolTips["announcer_2019"] = &$fieldToolTipsannouncer_2019;
$placeHolders["announcer_2019"] = &$placeHoldersannouncer_2019;
$page_titles["announcer_2019"] = &$pageTitlesannouncer_2019;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["announcer_2019"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["announcer_2019"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_announcer_2019()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	FormID,  	NameOfOrg,  	KeyPeople,  	DescriptionOfEntry,  	LowerThirds";
$proto0["m_strFrom"] = "FROM announcer_2019";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "announcer_2019",
	"m_srcTableName" => "announcer_2019"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "announcer_2019";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "FormID",
	"m_strTable" => "announcer_2019",
	"m_srcTableName" => "announcer_2019"
));

$proto8["m_sql"] = "FormID";
$proto8["m_srcTableName"] = "announcer_2019";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "NameOfOrg",
	"m_strTable" => "announcer_2019",
	"m_srcTableName" => "announcer_2019"
));

$proto10["m_sql"] = "NameOfOrg";
$proto10["m_srcTableName"] = "announcer_2019";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "KeyPeople",
	"m_strTable" => "announcer_2019",
	"m_srcTableName" => "announcer_2019"
));

$proto12["m_sql"] = "KeyPeople";
$proto12["m_srcTableName"] = "announcer_2019";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "DescriptionOfEntry",
	"m_strTable" => "announcer_2019",
	"m_srcTableName" => "announcer_2019"
));

$proto14["m_sql"] = "DescriptionOfEntry";
$proto14["m_srcTableName"] = "announcer_2019";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "LowerThirds",
	"m_strTable" => "announcer_2019",
	"m_srcTableName" => "announcer_2019"
));

$proto16["m_sql"] = "LowerThirds";
$proto16["m_srcTableName"] = "announcer_2019";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto18=array();
$proto18["m_link"] = "SQLL_MAIN";
			$proto19=array();
$proto19["m_strName"] = "announcer_2019";
$proto19["m_srcTableName"] = "announcer_2019";
$proto19["m_columns"] = array();
$proto19["m_columns"][] = "ID";
$proto19["m_columns"][] = "FormID";
$proto19["m_columns"][] = "NameOfOrg";
$proto19["m_columns"][] = "KeyPeople";
$proto19["m_columns"][] = "DescriptionOfEntry";
$proto19["m_columns"][] = "LowerThirds";
$obj = new SQLTable($proto19);

$proto18["m_table"] = $obj;
$proto18["m_sql"] = "announcer_2019";
$proto18["m_alias"] = "";
$proto18["m_srcTableName"] = "announcer_2019";
$proto20=array();
$proto20["m_sql"] = "";
$proto20["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto20["m_column"]=$obj;
$proto20["m_contained"] = array();
$proto20["m_strCase"] = "";
$proto20["m_havingmode"] = false;
$proto20["m_inBrackets"] = false;
$proto20["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto20);

$proto18["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto18);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="announcer_2019";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_announcer_2019 = createSqlQuery_announcer_2019();


	
					
;

						

$tdataannouncer_2019[".sqlquery"] = $queryData_announcer_2019;



$tableEvents["announcer_2019"] = new eventsBase;
$tdataannouncer_2019[".hasEvents"] = false;

?>