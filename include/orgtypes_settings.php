<?php
$tdataorgtypes = array();
$tdataorgtypes[".searchableFields"] = array();
$tdataorgtypes[".ShortName"] = "orgtypes";
$tdataorgtypes[".OwnerID"] = "";
$tdataorgtypes[".OriginalTable"] = "OrgTypes";


$tdataorgtypes[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataorgtypes[".originalPagesByType"] = $tdataorgtypes[".pagesByType"];
$tdataorgtypes[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataorgtypes[".originalPages"] = $tdataorgtypes[".pages"];
$tdataorgtypes[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataorgtypes[".originalDefaultPages"] = $tdataorgtypes[".defaultPages"];

//	field labels
$fieldLabelsorgtypes = array();
$fieldToolTipsorgtypes = array();
$pageTitlesorgtypes = array();
$placeHoldersorgtypes = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsorgtypes["English"] = array();
	$fieldToolTipsorgtypes["English"] = array();
	$placeHoldersorgtypes["English"] = array();
	$pageTitlesorgtypes["English"] = array();
	$fieldLabelsorgtypes["English"]["ID"] = "ID";
	$fieldToolTipsorgtypes["English"]["ID"] = "";
	$placeHoldersorgtypes["English"]["ID"] = "";
	$fieldLabelsorgtypes["English"]["OrgTypeName"] = "Org Type Name";
	$fieldToolTipsorgtypes["English"]["OrgTypeName"] = "";
	$placeHoldersorgtypes["English"]["OrgTypeName"] = "";
	$fieldLabelsorgtypes["English"]["form_master_id"] = "Form Master Id";
	$fieldToolTipsorgtypes["English"]["form_master_id"] = "";
	$placeHoldersorgtypes["English"]["form_master_id"] = "";
	if (count($fieldToolTipsorgtypes["English"]))
		$tdataorgtypes[".isUseToolTips"] = true;
}


	$tdataorgtypes[".NCSearch"] = true;



$tdataorgtypes[".shortTableName"] = "orgtypes";
$tdataorgtypes[".nSecOptions"] = 0;

$tdataorgtypes[".mainTableOwnerID"] = "";
$tdataorgtypes[".entityType"] = 0;
$tdataorgtypes[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdataorgtypes[".strOriginalTableName"] = "OrgTypes";

		 



$tdataorgtypes[".showAddInPopup"] = false;

$tdataorgtypes[".showEditInPopup"] = false;

$tdataorgtypes[".showViewInPopup"] = false;

$tdataorgtypes[".listAjax"] = false;
//	temporary
//$tdataorgtypes[".listAjax"] = false;

	$tdataorgtypes[".audit"] = false;

	$tdataorgtypes[".locking"] = false;


$pages = $tdataorgtypes[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataorgtypes[".edit"] = true;
	$tdataorgtypes[".afterEditAction"] = 1;
	$tdataorgtypes[".closePopupAfterEdit"] = 1;
	$tdataorgtypes[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataorgtypes[".add"] = true;
$tdataorgtypes[".afterAddAction"] = 1;
$tdataorgtypes[".closePopupAfterAdd"] = 1;
$tdataorgtypes[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataorgtypes[".list"] = true;
}



$tdataorgtypes[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataorgtypes[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataorgtypes[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataorgtypes[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataorgtypes[".printFriendly"] = true;
}



$tdataorgtypes[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataorgtypes[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataorgtypes[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataorgtypes[".isUseAjaxSuggest"] = true;

$tdataorgtypes[".rowHighlite"] = true;



						

$tdataorgtypes[".ajaxCodeSnippetAdded"] = false;

$tdataorgtypes[".buttonsAdded"] = false;

$tdataorgtypes[".addPageEvents"] = false;

// use timepicker for search panel
$tdataorgtypes[".isUseTimeForSearch"] = false;


$tdataorgtypes[".badgeColor"] = "CD853F";


$tdataorgtypes[".allSearchFields"] = array();
$tdataorgtypes[".filterFields"] = array();
$tdataorgtypes[".requiredSearchFields"] = array();

$tdataorgtypes[".googleLikeFields"] = array();
$tdataorgtypes[".googleLikeFields"][] = "ID";
$tdataorgtypes[".googleLikeFields"][] = "OrgTypeName";
$tdataorgtypes[".googleLikeFields"][] = "form_master_id";



$tdataorgtypes[".tableType"] = "list";

$tdataorgtypes[".printerPageOrientation"] = 0;
$tdataorgtypes[".nPrinterPageScale"] = 100;

$tdataorgtypes[".nPrinterSplitRecords"] = 40;

$tdataorgtypes[".geocodingEnabled"] = false;










$tdataorgtypes[".pageSize"] = 20;

$tdataorgtypes[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdataorgtypes[".strOrderBy"] = $tstrOrderBy;

$tdataorgtypes[".orderindexes"] = array();


$tdataorgtypes[".sqlHead"] = "SELECT ID,  	OrgTypeName,  	form_master_id";
$tdataorgtypes[".sqlFrom"] = "FROM OrgTypes";
$tdataorgtypes[".sqlWhereExpr"] = "";
$tdataorgtypes[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataorgtypes[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataorgtypes[".arrGroupsPerPage"] = $arrGPP;

$tdataorgtypes[".highlightSearchResults"] = true;

$tableKeysorgtypes = array();
$tableKeysorgtypes[] = "ID";
$tdataorgtypes[".Keys"] = $tableKeysorgtypes;


$tdataorgtypes[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "OrgTypes";
	$fdata["Label"] = GetFieldLabel("OrgTypes","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataorgtypes["ID"] = $fdata;
		$tdataorgtypes[".searchableFields"][] = "ID";
//	OrgTypeName
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "OrgTypeName";
	$fdata["GoodName"] = "OrgTypeName";
	$fdata["ownerTable"] = "OrgTypes";
	$fdata["Label"] = GetFieldLabel("OrgTypes","OrgTypeName");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "OrgTypeName";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "OrgTypeName";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=100";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataorgtypes["OrgTypeName"] = $fdata;
		$tdataorgtypes[".searchableFields"][] = "OrgTypeName";
//	form_master_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "form_master_id";
	$fdata["GoodName"] = "form_master_id";
	$fdata["ownerTable"] = "OrgTypes";
	$fdata["Label"] = GetFieldLabel("OrgTypes","form_master_id");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "form_master_id";

		$fdata["sourceSingle"] = "form_master_id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "form_master_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataorgtypes["form_master_id"] = $fdata;
		$tdataorgtypes[".searchableFields"][] = "form_master_id";


$tables_data["OrgTypes"]=&$tdataorgtypes;
$field_labels["OrgTypes"] = &$fieldLabelsorgtypes;
$fieldToolTips["OrgTypes"] = &$fieldToolTipsorgtypes;
$placeHolders["OrgTypes"] = &$placeHoldersorgtypes;
$page_titles["OrgTypes"] = &$pageTitlesorgtypes;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["OrgTypes"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["OrgTypes"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_orgtypes()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	OrgTypeName,  	form_master_id";
$proto0["m_strFrom"] = "FROM OrgTypes";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "OrgTypes",
	"m_srcTableName" => "OrgTypes"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "OrgTypes";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "OrgTypeName",
	"m_strTable" => "OrgTypes",
	"m_srcTableName" => "OrgTypes"
));

$proto8["m_sql"] = "OrgTypeName";
$proto8["m_srcTableName"] = "OrgTypes";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "form_master_id",
	"m_strTable" => "OrgTypes",
	"m_srcTableName" => "OrgTypes"
));

$proto10["m_sql"] = "form_master_id";
$proto10["m_srcTableName"] = "OrgTypes";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto12=array();
$proto12["m_link"] = "SQLL_MAIN";
			$proto13=array();
$proto13["m_strName"] = "OrgTypes";
$proto13["m_srcTableName"] = "OrgTypes";
$proto13["m_columns"] = array();
$proto13["m_columns"][] = "ID";
$proto13["m_columns"][] = "OrgTypeName";
$proto13["m_columns"][] = "form_master_id";
$obj = new SQLTable($proto13);

$proto12["m_table"] = $obj;
$proto12["m_sql"] = "OrgTypes";
$proto12["m_alias"] = "";
$proto12["m_srcTableName"] = "OrgTypes";
$proto14=array();
$proto14["m_sql"] = "";
$proto14["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto14["m_column"]=$obj;
$proto14["m_contained"] = array();
$proto14["m_strCase"] = "";
$proto14["m_havingmode"] = false;
$proto14["m_inBrackets"] = false;
$proto14["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto14);

$proto12["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto12);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="OrgTypes";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_orgtypes = createSqlQuery_orgtypes();


	
					
;

			

$tdataorgtypes[".sqlquery"] = $queryData_orgtypes;



$tableEvents["OrgTypes"] = new eventsBase;
$tdataorgtypes[".hasEvents"] = false;

?>