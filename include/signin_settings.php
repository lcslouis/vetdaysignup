<?php
$tdatasignin = array();
$tdatasignin[".searchableFields"] = array();
$tdatasignin[".ShortName"] = "signin";
$tdatasignin[".OwnerID"] = "";
$tdatasignin[".OriginalTable"] = "SignIn";


$tdatasignin[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatasignin[".originalPagesByType"] = $tdatasignin[".pagesByType"];
$tdatasignin[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatasignin[".originalPages"] = $tdatasignin[".pages"];
$tdatasignin[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatasignin[".originalDefaultPages"] = $tdatasignin[".defaultPages"];

//	field labels
$fieldLabelssignin = array();
$fieldToolTipssignin = array();
$pageTitlessignin = array();
$placeHolderssignin = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelssignin["English"] = array();
	$fieldToolTipssignin["English"] = array();
	$placeHolderssignin["English"] = array();
	$pageTitlessignin["English"] = array();
	$fieldLabelssignin["English"]["ID"] = "ID";
	$fieldToolTipssignin["English"]["ID"] = "";
	$placeHolderssignin["English"]["ID"] = "";
	$fieldLabelssignin["English"]["EntryID"] = "Entry ID";
	$fieldToolTipssignin["English"]["EntryID"] = "";
	$placeHolderssignin["English"]["EntryID"] = "";
	$fieldLabelssignin["English"]["ScannedTime"] = "Scanned Time";
	$fieldToolTipssignin["English"]["ScannedTime"] = "";
	$placeHolderssignin["English"]["ScannedTime"] = "";
	if (count($fieldToolTipssignin["English"]))
		$tdatasignin[".isUseToolTips"] = true;
}


	$tdatasignin[".NCSearch"] = true;



$tdatasignin[".shortTableName"] = "signin";
$tdatasignin[".nSecOptions"] = 0;

$tdatasignin[".mainTableOwnerID"] = "";
$tdatasignin[".entityType"] = 0;
$tdatasignin[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdatasignin[".strOriginalTableName"] = "SignIn";

		 



$tdatasignin[".showAddInPopup"] = false;

$tdatasignin[".showEditInPopup"] = false;

$tdatasignin[".showViewInPopup"] = false;

$tdatasignin[".listAjax"] = false;
//	temporary
//$tdatasignin[".listAjax"] = false;

	$tdatasignin[".audit"] = false;

	$tdatasignin[".locking"] = false;


$pages = $tdatasignin[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatasignin[".edit"] = true;
	$tdatasignin[".afterEditAction"] = 1;
	$tdatasignin[".closePopupAfterEdit"] = 1;
	$tdatasignin[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatasignin[".add"] = true;
$tdatasignin[".afterAddAction"] = 1;
$tdatasignin[".closePopupAfterAdd"] = 1;
$tdatasignin[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatasignin[".list"] = true;
}



$tdatasignin[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdatasignin[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatasignin[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatasignin[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatasignin[".printFriendly"] = true;
}



$tdatasignin[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatasignin[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatasignin[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatasignin[".isUseAjaxSuggest"] = true;

$tdatasignin[".rowHighlite"] = true;



						

$tdatasignin[".ajaxCodeSnippetAdded"] = false;

$tdatasignin[".buttonsAdded"] = false;

$tdatasignin[".addPageEvents"] = false;

// use timepicker for search panel
$tdatasignin[".isUseTimeForSearch"] = false;


$tdatasignin[".badgeColor"] = "4682B4";


$tdatasignin[".allSearchFields"] = array();
$tdatasignin[".filterFields"] = array();
$tdatasignin[".requiredSearchFields"] = array();

$tdatasignin[".googleLikeFields"] = array();
$tdatasignin[".googleLikeFields"][] = "ID";
$tdatasignin[".googleLikeFields"][] = "EntryID";
$tdatasignin[".googleLikeFields"][] = "ScannedTime";



$tdatasignin[".tableType"] = "list";

$tdatasignin[".printerPageOrientation"] = 0;
$tdatasignin[".nPrinterPageScale"] = 100;

$tdatasignin[".nPrinterSplitRecords"] = 40;

$tdatasignin[".geocodingEnabled"] = false;










$tdatasignin[".pageSize"] = 20;

$tdatasignin[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatasignin[".strOrderBy"] = $tstrOrderBy;

$tdatasignin[".orderindexes"] = array();


$tdatasignin[".sqlHead"] = "SELECT ID,  	EntryID,  	ScannedTime";
$tdatasignin[".sqlFrom"] = "FROM SignIn";
$tdatasignin[".sqlWhereExpr"] = "";
$tdatasignin[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatasignin[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatasignin[".arrGroupsPerPage"] = $arrGPP;

$tdatasignin[".highlightSearchResults"] = true;

$tableKeyssignin = array();
$tableKeyssignin[] = "ID";
$tdatasignin[".Keys"] = $tableKeyssignin;


$tdatasignin[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "SignIn";
	$fdata["Label"] = GetFieldLabel("SignIn","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatasignin["ID"] = $fdata;
		$tdatasignin[".searchableFields"][] = "ID";
//	EntryID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "EntryID";
	$fdata["GoodName"] = "EntryID";
	$fdata["ownerTable"] = "SignIn";
	$fdata["Label"] = GetFieldLabel("SignIn","EntryID");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "EntryID";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "EntryID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Hyperlink");

	
		$vdata["LinkPrefix"] ="http://vetday.bakchnl.com/Entry_view.php?editid1=";

	
	
				$vdata["hlType"] = 0;
	$vdata["hlLinkWordNameType"] = "Text";
	$vdata["hlLinkWordText"] = "Link";
	$vdata["hlTitleField"] = "";

	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatasignin["EntryID"] = $fdata;
		$tdatasignin[".searchableFields"][] = "EntryID";
//	ScannedTime
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "ScannedTime";
	$fdata["GoodName"] = "ScannedTime";
	$fdata["ownerTable"] = "SignIn";
	$fdata["Label"] = GetFieldLabel("SignIn","ScannedTime");
	$fdata["FieldType"] = 135;


	
	
										

		$fdata["strField"] = "ScannedTime";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ScannedTime";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "Invalid week day", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
		$edata["autoUpdatable"] = true;

	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatasignin["ScannedTime"] = $fdata;
		$tdatasignin[".searchableFields"][] = "ScannedTime";


$tables_data["SignIn"]=&$tdatasignin;
$field_labels["SignIn"] = &$fieldLabelssignin;
$fieldToolTips["SignIn"] = &$fieldToolTipssignin;
$placeHolders["SignIn"] = &$placeHolderssignin;
$page_titles["SignIn"] = &$pageTitlessignin;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["SignIn"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["SignIn"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_signin()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	EntryID,  	ScannedTime";
$proto0["m_strFrom"] = "FROM SignIn";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "SignIn",
	"m_srcTableName" => "SignIn"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "SignIn";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "EntryID",
	"m_strTable" => "SignIn",
	"m_srcTableName" => "SignIn"
));

$proto8["m_sql"] = "EntryID";
$proto8["m_srcTableName"] = "SignIn";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "ScannedTime",
	"m_strTable" => "SignIn",
	"m_srcTableName" => "SignIn"
));

$proto10["m_sql"] = "ScannedTime";
$proto10["m_srcTableName"] = "SignIn";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto12=array();
$proto12["m_link"] = "SQLL_MAIN";
			$proto13=array();
$proto13["m_strName"] = "SignIn";
$proto13["m_srcTableName"] = "SignIn";
$proto13["m_columns"] = array();
$proto13["m_columns"][] = "ID";
$proto13["m_columns"][] = "EntryID";
$proto13["m_columns"][] = "ScannedTime";
$obj = new SQLTable($proto13);

$proto12["m_table"] = $obj;
$proto12["m_sql"] = "SignIn";
$proto12["m_alias"] = "";
$proto12["m_srcTableName"] = "SignIn";
$proto14=array();
$proto14["m_sql"] = "";
$proto14["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto14["m_column"]=$obj;
$proto14["m_contained"] = array();
$proto14["m_strCase"] = "";
$proto14["m_havingmode"] = false;
$proto14["m_inBrackets"] = false;
$proto14["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto14);

$proto12["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto12);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="SignIn";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_signin = createSqlQuery_signin();


	
					
;

			

$tdatasignin[".sqlquery"] = $queryData_signin;



include_once(getabspath("include/signin_events.php"));
$tableEvents["SignIn"] = new eventclass_signin;
$tdatasignin[".hasEvents"] = true;

?>