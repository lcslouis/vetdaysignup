<?php
$tdataentrytype = array();
$tdataentrytype[".searchableFields"] = array();
$tdataentrytype[".ShortName"] = "entrytype";
$tdataentrytype[".OwnerID"] = "";
$tdataentrytype[".OriginalTable"] = "EntryType";


$tdataentrytype[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataentrytype[".originalPagesByType"] = $tdataentrytype[".pagesByType"];
$tdataentrytype[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataentrytype[".originalPages"] = $tdataentrytype[".pages"];
$tdataentrytype[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataentrytype[".originalDefaultPages"] = $tdataentrytype[".defaultPages"];

//	field labels
$fieldLabelsentrytype = array();
$fieldToolTipsentrytype = array();
$pageTitlesentrytype = array();
$placeHoldersentrytype = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsentrytype["English"] = array();
	$fieldToolTipsentrytype["English"] = array();
	$placeHoldersentrytype["English"] = array();
	$pageTitlesentrytype["English"] = array();
	$fieldLabelsentrytype["English"]["ID"] = "ID";
	$fieldToolTipsentrytype["English"]["ID"] = "";
	$placeHoldersentrytype["English"]["ID"] = "";
	$fieldLabelsentrytype["English"]["EntryTypeName"] = "Entry Type Name";
	$fieldToolTipsentrytype["English"]["EntryTypeName"] = "";
	$placeHoldersentrytype["English"]["EntryTypeName"] = "";
	$fieldLabelsentrytype["English"]["form_master_id"] = "Form Master Id";
	$fieldToolTipsentrytype["English"]["form_master_id"] = "";
	$placeHoldersentrytype["English"]["form_master_id"] = "";
	$fieldLabelsentrytype["English"]["IDnum"] = "IDnum";
	$fieldToolTipsentrytype["English"]["IDnum"] = "";
	$placeHoldersentrytype["English"]["IDnum"] = "";
	if (count($fieldToolTipsentrytype["English"]))
		$tdataentrytype[".isUseToolTips"] = true;
}


	$tdataentrytype[".NCSearch"] = true;



$tdataentrytype[".shortTableName"] = "entrytype";
$tdataentrytype[".nSecOptions"] = 0;

$tdataentrytype[".mainTableOwnerID"] = "";
$tdataentrytype[".entityType"] = 0;
$tdataentrytype[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdataentrytype[".strOriginalTableName"] = "EntryType";

		 



$tdataentrytype[".showAddInPopup"] = false;

$tdataentrytype[".showEditInPopup"] = false;

$tdataentrytype[".showViewInPopup"] = false;

$tdataentrytype[".listAjax"] = false;
//	temporary
//$tdataentrytype[".listAjax"] = false;

	$tdataentrytype[".audit"] = false;

	$tdataentrytype[".locking"] = false;


$pages = $tdataentrytype[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataentrytype[".edit"] = true;
	$tdataentrytype[".afterEditAction"] = 1;
	$tdataentrytype[".closePopupAfterEdit"] = 1;
	$tdataentrytype[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataentrytype[".add"] = true;
$tdataentrytype[".afterAddAction"] = 1;
$tdataentrytype[".closePopupAfterAdd"] = 1;
$tdataentrytype[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataentrytype[".list"] = true;
}



$tdataentrytype[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataentrytype[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataentrytype[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataentrytype[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataentrytype[".printFriendly"] = true;
}



$tdataentrytype[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataentrytype[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataentrytype[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataentrytype[".isUseAjaxSuggest"] = true;

$tdataentrytype[".rowHighlite"] = true;



						

$tdataentrytype[".ajaxCodeSnippetAdded"] = false;

$tdataentrytype[".buttonsAdded"] = false;

$tdataentrytype[".addPageEvents"] = false;

// use timepicker for search panel
$tdataentrytype[".isUseTimeForSearch"] = false;


$tdataentrytype[".badgeColor"] = "2F4F4F";


$tdataentrytype[".allSearchFields"] = array();
$tdataentrytype[".filterFields"] = array();
$tdataentrytype[".requiredSearchFields"] = array();

$tdataentrytype[".googleLikeFields"] = array();
$tdataentrytype[".googleLikeFields"][] = "ID";
$tdataentrytype[".googleLikeFields"][] = "EntryTypeName";
$tdataentrytype[".googleLikeFields"][] = "form_master_id";
$tdataentrytype[".googleLikeFields"][] = "IDnum";



$tdataentrytype[".tableType"] = "list";

$tdataentrytype[".printerPageOrientation"] = 0;
$tdataentrytype[".nPrinterPageScale"] = 100;

$tdataentrytype[".nPrinterSplitRecords"] = 40;

$tdataentrytype[".geocodingEnabled"] = false;










$tdataentrytype[".pageSize"] = 20;

$tdataentrytype[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdataentrytype[".strOrderBy"] = $tstrOrderBy;

$tdataentrytype[".orderindexes"] = array();


$tdataentrytype[".sqlHead"] = "SELECT ID,  	EntryTypeName,  	form_master_id,  	IDnum";
$tdataentrytype[".sqlFrom"] = "FROM EntryType";
$tdataentrytype[".sqlWhereExpr"] = "";
$tdataentrytype[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataentrytype[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataentrytype[".arrGroupsPerPage"] = $arrGPP;

$tdataentrytype[".highlightSearchResults"] = true;

$tableKeysentrytype = array();
$tableKeysentrytype[] = "ID";
$tdataentrytype[".Keys"] = $tableKeysentrytype;


$tdataentrytype[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "EntryType";
	$fdata["Label"] = GetFieldLabel("EntryType","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataentrytype["ID"] = $fdata;
		$tdataentrytype[".searchableFields"][] = "ID";
//	EntryTypeName
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "EntryTypeName";
	$fdata["GoodName"] = "EntryTypeName";
	$fdata["ownerTable"] = "EntryType";
	$fdata["Label"] = GetFieldLabel("EntryType","EntryTypeName");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "EntryTypeName";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "EntryTypeName";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=200";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataentrytype["EntryTypeName"] = $fdata;
		$tdataentrytype[".searchableFields"][] = "EntryTypeName";
//	form_master_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "form_master_id";
	$fdata["GoodName"] = "form_master_id";
	$fdata["ownerTable"] = "EntryType";
	$fdata["Label"] = GetFieldLabel("EntryType","form_master_id");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "form_master_id";

		$fdata["sourceSingle"] = "form_master_id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "form_master_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataentrytype["form_master_id"] = $fdata;
		$tdataentrytype[".searchableFields"][] = "form_master_id";
//	IDnum
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "IDnum";
	$fdata["GoodName"] = "IDnum";
	$fdata["ownerTable"] = "EntryType";
	$fdata["Label"] = GetFieldLabel("EntryType","IDnum");
	$fdata["FieldType"] = 20;


	
	
										

		$fdata["strField"] = "IDnum";

		$fdata["sourceSingle"] = "IDnum";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "IDnum";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataentrytype["IDnum"] = $fdata;
		$tdataentrytype[".searchableFields"][] = "IDnum";


$tables_data["EntryType"]=&$tdataentrytype;
$field_labels["EntryType"] = &$fieldLabelsentrytype;
$fieldToolTips["EntryType"] = &$fieldToolTipsentrytype;
$placeHolders["EntryType"] = &$placeHoldersentrytype;
$page_titles["EntryType"] = &$pageTitlesentrytype;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["EntryType"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["EntryType"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_entrytype()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	EntryTypeName,  	form_master_id,  	IDnum";
$proto0["m_strFrom"] = "FROM EntryType";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "EntryType",
	"m_srcTableName" => "EntryType"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "EntryType";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "EntryTypeName",
	"m_strTable" => "EntryType",
	"m_srcTableName" => "EntryType"
));

$proto8["m_sql"] = "EntryTypeName";
$proto8["m_srcTableName"] = "EntryType";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "form_master_id",
	"m_strTable" => "EntryType",
	"m_srcTableName" => "EntryType"
));

$proto10["m_sql"] = "form_master_id";
$proto10["m_srcTableName"] = "EntryType";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "IDnum",
	"m_strTable" => "EntryType",
	"m_srcTableName" => "EntryType"
));

$proto12["m_sql"] = "IDnum";
$proto12["m_srcTableName"] = "EntryType";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto14=array();
$proto14["m_link"] = "SQLL_MAIN";
			$proto15=array();
$proto15["m_strName"] = "EntryType";
$proto15["m_srcTableName"] = "EntryType";
$proto15["m_columns"] = array();
$proto15["m_columns"][] = "ID";
$proto15["m_columns"][] = "EntryTypeName";
$proto15["m_columns"][] = "form_master_id";
$proto15["m_columns"][] = "IDnum";
$obj = new SQLTable($proto15);

$proto14["m_table"] = $obj;
$proto14["m_sql"] = "EntryType";
$proto14["m_alias"] = "";
$proto14["m_srcTableName"] = "EntryType";
$proto16=array();
$proto16["m_sql"] = "";
$proto16["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto16["m_column"]=$obj;
$proto16["m_contained"] = array();
$proto16["m_strCase"] = "";
$proto16["m_havingmode"] = false;
$proto16["m_inBrackets"] = false;
$proto16["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto16);

$proto14["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto14);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="EntryType";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_entrytype = createSqlQuery_entrytype();


	
					
;

				

$tdataentrytype[".sqlquery"] = $queryData_entrytype;



$tableEvents["EntryType"] = new eventsBase;
$tdataentrytype[".hasEvents"] = false;

?>