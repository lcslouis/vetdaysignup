<?php
$tdatacarsign = array();
$tdatacarsign[".searchableFields"] = array();
$tdatacarsign[".ShortName"] = "carsign";
$tdatacarsign[".OwnerID"] = "UID";
$tdatacarsign[".OriginalTable"] = "Form";


$tdatacarsign[".pagesByType"] = my_json_decode( "{\"list\":[\"list\"],\"print\":[\"print\",\"print1\"],\"search\":[\"search\"],\"view\":[\"view\",\"view1\"]}" );
$tdatacarsign[".originalPagesByType"] = $tdatacarsign[".pagesByType"];
$tdatacarsign[".pages"] = types2pages( my_json_decode( "{\"list\":[\"list\"],\"print\":[\"print\",\"print1\"],\"search\":[\"search\"],\"view\":[\"view\",\"view1\"]}" ) );
$tdatacarsign[".originalPages"] = $tdatacarsign[".pages"];
$tdatacarsign[".defaultPages"] = my_json_decode( "{\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatacarsign[".originalDefaultPages"] = $tdatacarsign[".defaultPages"];

//	field labels
$fieldLabelscarsign = array();
$fieldToolTipscarsign = array();
$pageTitlescarsign = array();
$placeHolderscarsign = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelscarsign["English"] = array();
	$fieldToolTipscarsign["English"] = array();
	$placeHolderscarsign["English"] = array();
	$pageTitlescarsign["English"] = array();
	$fieldLabelscarsign["English"]["ID"] = "Entry ID: ";
	$fieldToolTipscarsign["English"]["ID"] = "";
	$placeHolderscarsign["English"]["ID"] = "";
	$fieldLabelscarsign["English"]["NameOfOrg"] = "Name Of Organization";
	$fieldToolTipscarsign["English"]["NameOfOrg"] = "";
	$placeHolderscarsign["English"]["NameOfOrg"] = "";
	$fieldLabelscarsign["English"]["ParadeOrder"] = "Parade Order";
	$fieldToolTipscarsign["English"]["ParadeOrder"] = "";
	$placeHolderscarsign["English"]["ParadeOrder"] = "";
	if (count($fieldToolTipscarsign["English"]))
		$tdatacarsign[".isUseToolTips"] = true;
}


	$tdatacarsign[".NCSearch"] = true;



$tdatacarsign[".shortTableName"] = "carsign";
$tdatacarsign[".nSecOptions"] = 0;

$tdatacarsign[".mainTableOwnerID"] = "UID";
$tdatacarsign[".entityType"] = 1;
$tdatacarsign[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdatacarsign[".strOriginalTableName"] = "Form";

		 



$tdatacarsign[".showAddInPopup"] = false;

$tdatacarsign[".showEditInPopup"] = false;

$tdatacarsign[".showViewInPopup"] = false;

$tdatacarsign[".listAjax"] = false;
//	temporary
//$tdatacarsign[".listAjax"] = false;

	$tdatacarsign[".audit"] = false;

	$tdatacarsign[".locking"] = false;


$pages = $tdatacarsign[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatacarsign[".edit"] = true;
	$tdatacarsign[".afterEditAction"] = 1;
	$tdatacarsign[".closePopupAfterEdit"] = 1;
	$tdatacarsign[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatacarsign[".add"] = true;
$tdatacarsign[".afterAddAction"] = 1;
$tdatacarsign[".closePopupAfterAdd"] = 1;
$tdatacarsign[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatacarsign[".list"] = true;
}



$tdatacarsign[".strSortControlSettingsJSON"] = "";

$tdatacarsign[".strClickActionJSON"] = "{\"fields\":{\"Approved\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactAddress\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactCell\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactEmail\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactFax\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactName\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactPhone\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactZip\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"DescriptionOfEntry\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"EntrySize\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"EntryType\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ID\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"KeyPeople\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"NameOfOrg\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"OnSiteCheckIn\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"SchoolClass\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"TypeOfOrg\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"UID\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}}},\"row\":{\"action\":\"open\",\"codeData\":{},\"gridData\":{\"action\":\"checkbox\",\"table\":null},\"openData\":{\"how\":\"goto\",\"page\":\"view\",\"table\":null,\"url\":\"\"}}}";



if( $pages[PAGE_VIEW] ) {
$tdatacarsign[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatacarsign[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatacarsign[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatacarsign[".printFriendly"] = true;
}



$tdatacarsign[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatacarsign[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatacarsign[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatacarsign[".isUseAjaxSuggest"] = true;

$tdatacarsign[".rowHighlite"] = true;



						

$tdatacarsign[".ajaxCodeSnippetAdded"] = false;

$tdatacarsign[".buttonsAdded"] = false;

$tdatacarsign[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacarsign[".isUseTimeForSearch"] = false;


$tdatacarsign[".badgeColor"] = "7B68EE";


$tdatacarsign[".allSearchFields"] = array();
$tdatacarsign[".filterFields"] = array();
$tdatacarsign[".requiredSearchFields"] = array();

$tdatacarsign[".googleLikeFields"] = array();
$tdatacarsign[".googleLikeFields"][] = "ID";
$tdatacarsign[".googleLikeFields"][] = "NameOfOrg";
$tdatacarsign[".googleLikeFields"][] = "ParadeOrder";



$tdatacarsign[".tableType"] = "list";

$tdatacarsign[".printerPageOrientation"] = 1;
$tdatacarsign[".nPrinterPageScale"] = 100;

$tdatacarsign[".nPrinterSplitRecords"] = 1;

$tdatacarsign[".geocodingEnabled"] = false;










$tdatacarsign[".pageSize"] = 500;

$tdatacarsign[".warnLeavingPages"] = true;

$tdatacarsign[".hideEmptyFieldsOnView"] = true;


$tstrOrderBy = "ORDER BY Form.ParadeOrder";
$tdatacarsign[".strOrderBy"] = $tstrOrderBy;

$tdatacarsign[".orderindexes"] = array();
	$tdatacarsign[".orderindexes"][] = array(3, (1 ? "ASC" : "DESC"), "Form.ParadeOrder");



$tdatacarsign[".sqlHead"] = "SELECT Form.ID,  Form.NameOfOrg,  Form.ParadeOrder";
$tdatacarsign[".sqlFrom"] = "FROM Form  , QRCodeURL";
$tdatacarsign[".sqlWhereExpr"] = "(Form.Approved =1)";
$tdatacarsign[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = -1;
$tdatacarsign[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacarsign[".arrGroupsPerPage"] = $arrGPP;

$tdatacarsign[".highlightSearchResults"] = true;

$tableKeyscarsign = array();
$tableKeyscarsign[] = "ID";
$tdatacarsign[".Keys"] = $tableKeyscarsign;


$tdatacarsign[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("CarSign","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacarsign["ID"] = $fdata;
		$tdatacarsign[".searchableFields"][] = "ID";
//	NameOfOrg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "NameOfOrg";
	$fdata["GoodName"] = "NameOfOrg";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("CarSign","NameOfOrg");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "NameOfOrg";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.NameOfOrg";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacarsign["NameOfOrg"] = $fdata;
		$tdatacarsign[".searchableFields"][] = "NameOfOrg";
//	ParadeOrder
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "ParadeOrder";
	$fdata["GoodName"] = "ParadeOrder";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("CarSign","ParadeOrder");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ParadeOrder";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ParadeOrder";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacarsign["ParadeOrder"] = $fdata;
		$tdatacarsign[".searchableFields"][] = "ParadeOrder";


$tables_data["CarSign"]=&$tdatacarsign;
$field_labels["CarSign"] = &$fieldLabelscarsign;
$fieldToolTips["CarSign"] = &$fieldToolTipscarsign;
$placeHolders["CarSign"] = &$placeHolderscarsign;
$page_titles["CarSign"] = &$pageTitlescarsign;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["CarSign"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["CarSign"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_carsign()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "Form.ID,  Form.NameOfOrg,  Form.ParadeOrder";
$proto0["m_strFrom"] = "FROM Form  , QRCodeURL";
$proto0["m_strWhere"] = "(Form.Approved =1)";
$proto0["m_strOrderBy"] = "ORDER BY Form.ParadeOrder";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "Form.Approved =1";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "Approved",
	"m_strTable" => "Form",
	"m_srcTableName" => "CarSign"
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "=1";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "Form",
	"m_srcTableName" => "CarSign"
));

$proto6["m_sql"] = "Form.ID";
$proto6["m_srcTableName"] = "CarSign";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "NameOfOrg",
	"m_strTable" => "Form",
	"m_srcTableName" => "CarSign"
));

$proto8["m_sql"] = "Form.NameOfOrg";
$proto8["m_srcTableName"] = "CarSign";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "ParadeOrder",
	"m_strTable" => "Form",
	"m_srcTableName" => "CarSign"
));

$proto10["m_sql"] = "Form.ParadeOrder";
$proto10["m_srcTableName"] = "CarSign";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto12=array();
$proto12["m_link"] = "SQLL_MAIN";
			$proto13=array();
$proto13["m_strName"] = "Form";
$proto13["m_srcTableName"] = "CarSign";
$proto13["m_columns"] = array();
$proto13["m_columns"][] = "ID";
$proto13["m_columns"][] = "NameOfOrg";
$proto13["m_columns"][] = "TypeOfOrg";
$proto13["m_columns"][] = "SchoolClass";
$proto13["m_columns"][] = "EntryType";
$proto13["m_columns"][] = "KeyPeople";
$proto13["m_columns"][] = "EntrySize";
$proto13["m_columns"][] = "ContactName";
$proto13["m_columns"][] = "ContactAddress";
$proto13["m_columns"][] = "ContactEmail";
$proto13["m_columns"][] = "ContactZip";
$proto13["m_columns"][] = "ContactPhone";
$proto13["m_columns"][] = "ContactFax";
$proto13["m_columns"][] = "ContactCell";
$proto13["m_columns"][] = "DescriptionOfEntry";
$proto13["m_columns"][] = "UID";
$proto13["m_columns"][] = "NumOfHorses";
$proto13["m_columns"][] = "Approved";
$proto13["m_columns"][] = "LowerThirds";
$proto13["m_columns"][] = "ParadeOrder";
$proto13["m_columns"][] = "GroupOrganizer";
$proto13["m_columns"][] = "AgreetoRules";
$proto13["m_columns"][] = "LiabilityWavierSigned";
$obj = new SQLTable($proto13);

$proto12["m_table"] = $obj;
$proto12["m_sql"] = "Form";
$proto12["m_alias"] = "";
$proto12["m_srcTableName"] = "CarSign";
$proto14=array();
$proto14["m_sql"] = "";
$proto14["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto14["m_column"]=$obj;
$proto14["m_contained"] = array();
$proto14["m_strCase"] = "";
$proto14["m_havingmode"] = false;
$proto14["m_inBrackets"] = false;
$proto14["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto14);

$proto12["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto12);

$proto0["m_fromlist"][]=$obj;
												$proto16=array();
$proto16["m_link"] = "SQLL_CROSSJOIN";
			$proto17=array();
$proto17["m_strName"] = "QRCodeURL";
$proto17["m_srcTableName"] = "CarSign";
$proto17["m_columns"] = array();
$proto17["m_columns"][] = "ID";
$proto17["m_columns"][] = "ProgramURL";
$proto17["m_columns"][] = "PageName";
$obj = new SQLTable($proto17);

$proto16["m_table"] = $obj;
$proto16["m_sql"] = "QRCodeURL";
$proto16["m_alias"] = "";
$proto16["m_srcTableName"] = "CarSign";
$proto18=array();
$proto18["m_sql"] = "";
$proto18["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto18["m_column"]=$obj;
$proto18["m_contained"] = array();
$proto18["m_strCase"] = "";
$proto18["m_havingmode"] = false;
$proto18["m_inBrackets"] = false;
$proto18["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto18);

$proto16["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto16);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto20=array();
						$obj = new SQLField(array(
	"m_strName" => "ParadeOrder",
	"m_strTable" => "Form",
	"m_srcTableName" => "CarSign"
));

$proto20["m_column"]=$obj;
$proto20["m_bAsc"] = 1;
$proto20["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto20);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="CarSign";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_carsign = createSqlQuery_carsign();


	
					
;

			

$tdatacarsign[".sqlquery"] = $queryData_carsign;



include_once(getabspath("include/carsign_events.php"));
$tableEvents["CarSign"] = new eventclass_carsign;
$tdatacarsign[".hasEvents"] = true;

?>