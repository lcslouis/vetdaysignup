<?php
$tdataschooltype = array();
$tdataschooltype[".searchableFields"] = array();
$tdataschooltype[".ShortName"] = "schooltype";
$tdataschooltype[".OwnerID"] = "";
$tdataschooltype[".OriginalTable"] = "SchoolType";


$tdataschooltype[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataschooltype[".originalPagesByType"] = $tdataschooltype[".pagesByType"];
$tdataschooltype[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataschooltype[".originalPages"] = $tdataschooltype[".pages"];
$tdataschooltype[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataschooltype[".originalDefaultPages"] = $tdataschooltype[".defaultPages"];

//	field labels
$fieldLabelsschooltype = array();
$fieldToolTipsschooltype = array();
$pageTitlesschooltype = array();
$placeHoldersschooltype = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsschooltype["English"] = array();
	$fieldToolTipsschooltype["English"] = array();
	$placeHoldersschooltype["English"] = array();
	$pageTitlesschooltype["English"] = array();
	$fieldLabelsschooltype["English"]["ID"] = "ID";
	$fieldToolTipsschooltype["English"]["ID"] = "";
	$placeHoldersschooltype["English"]["ID"] = "";
	$fieldLabelsschooltype["English"]["TypeName"] = "Type Name";
	$fieldToolTipsschooltype["English"]["TypeName"] = "";
	$placeHoldersschooltype["English"]["TypeName"] = "";
	$fieldLabelsschooltype["English"]["form_master_id"] = "Form Master Id";
	$fieldToolTipsschooltype["English"]["form_master_id"] = "";
	$placeHoldersschooltype["English"]["form_master_id"] = "";
	if (count($fieldToolTipsschooltype["English"]))
		$tdataschooltype[".isUseToolTips"] = true;
}


	$tdataschooltype[".NCSearch"] = true;



$tdataschooltype[".shortTableName"] = "schooltype";
$tdataschooltype[".nSecOptions"] = 0;

$tdataschooltype[".mainTableOwnerID"] = "";
$tdataschooltype[".entityType"] = 0;
$tdataschooltype[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdataschooltype[".strOriginalTableName"] = "SchoolType";

		 



$tdataschooltype[".showAddInPopup"] = false;

$tdataschooltype[".showEditInPopup"] = false;

$tdataschooltype[".showViewInPopup"] = false;

$tdataschooltype[".listAjax"] = false;
//	temporary
//$tdataschooltype[".listAjax"] = false;

	$tdataschooltype[".audit"] = false;

	$tdataschooltype[".locking"] = false;


$pages = $tdataschooltype[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataschooltype[".edit"] = true;
	$tdataschooltype[".afterEditAction"] = 1;
	$tdataschooltype[".closePopupAfterEdit"] = 1;
	$tdataschooltype[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataschooltype[".add"] = true;
$tdataschooltype[".afterAddAction"] = 1;
$tdataschooltype[".closePopupAfterAdd"] = 1;
$tdataschooltype[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataschooltype[".list"] = true;
}



$tdataschooltype[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataschooltype[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataschooltype[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataschooltype[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataschooltype[".printFriendly"] = true;
}



$tdataschooltype[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataschooltype[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataschooltype[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataschooltype[".isUseAjaxSuggest"] = true;

$tdataschooltype[".rowHighlite"] = true;



						

$tdataschooltype[".ajaxCodeSnippetAdded"] = false;

$tdataschooltype[".buttonsAdded"] = false;

$tdataschooltype[".addPageEvents"] = false;

// use timepicker for search panel
$tdataschooltype[".isUseTimeForSearch"] = false;


$tdataschooltype[".badgeColor"] = "CFAE83";


$tdataschooltype[".allSearchFields"] = array();
$tdataschooltype[".filterFields"] = array();
$tdataschooltype[".requiredSearchFields"] = array();

$tdataschooltype[".googleLikeFields"] = array();
$tdataschooltype[".googleLikeFields"][] = "ID";
$tdataschooltype[".googleLikeFields"][] = "TypeName";
$tdataschooltype[".googleLikeFields"][] = "form_master_id";



$tdataschooltype[".tableType"] = "list";

$tdataschooltype[".printerPageOrientation"] = 0;
$tdataschooltype[".nPrinterPageScale"] = 100;

$tdataschooltype[".nPrinterSplitRecords"] = 40;

$tdataschooltype[".geocodingEnabled"] = false;










$tdataschooltype[".pageSize"] = 20;

$tdataschooltype[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdataschooltype[".strOrderBy"] = $tstrOrderBy;

$tdataschooltype[".orderindexes"] = array();


$tdataschooltype[".sqlHead"] = "SELECT ID,  	TypeName,  	form_master_id";
$tdataschooltype[".sqlFrom"] = "FROM SchoolType";
$tdataschooltype[".sqlWhereExpr"] = "";
$tdataschooltype[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataschooltype[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataschooltype[".arrGroupsPerPage"] = $arrGPP;

$tdataschooltype[".highlightSearchResults"] = true;

$tableKeysschooltype = array();
$tableKeysschooltype[] = "ID";
$tdataschooltype[".Keys"] = $tableKeysschooltype;


$tdataschooltype[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "SchoolType";
	$fdata["Label"] = GetFieldLabel("SchoolType","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataschooltype["ID"] = $fdata;
		$tdataschooltype[".searchableFields"][] = "ID";
//	TypeName
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "TypeName";
	$fdata["GoodName"] = "TypeName";
	$fdata["ownerTable"] = "SchoolType";
	$fdata["Label"] = GetFieldLabel("SchoolType","TypeName");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "TypeName";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "TypeName";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=100";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataschooltype["TypeName"] = $fdata;
		$tdataschooltype[".searchableFields"][] = "TypeName";
//	form_master_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "form_master_id";
	$fdata["GoodName"] = "form_master_id";
	$fdata["ownerTable"] = "SchoolType";
	$fdata["Label"] = GetFieldLabel("SchoolType","form_master_id");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "form_master_id";

		$fdata["sourceSingle"] = "form_master_id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "form_master_id";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataschooltype["form_master_id"] = $fdata;
		$tdataschooltype[".searchableFields"][] = "form_master_id";


$tables_data["SchoolType"]=&$tdataschooltype;
$field_labels["SchoolType"] = &$fieldLabelsschooltype;
$fieldToolTips["SchoolType"] = &$fieldToolTipsschooltype;
$placeHolders["SchoolType"] = &$placeHoldersschooltype;
$page_titles["SchoolType"] = &$pageTitlesschooltype;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["SchoolType"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["SchoolType"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_schooltype()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	TypeName,  	form_master_id";
$proto0["m_strFrom"] = "FROM SchoolType";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "SchoolType",
	"m_srcTableName" => "SchoolType"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "SchoolType";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "TypeName",
	"m_strTable" => "SchoolType",
	"m_srcTableName" => "SchoolType"
));

$proto8["m_sql"] = "TypeName";
$proto8["m_srcTableName"] = "SchoolType";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "form_master_id",
	"m_strTable" => "SchoolType",
	"m_srcTableName" => "SchoolType"
));

$proto10["m_sql"] = "form_master_id";
$proto10["m_srcTableName"] = "SchoolType";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto12=array();
$proto12["m_link"] = "SQLL_MAIN";
			$proto13=array();
$proto13["m_strName"] = "SchoolType";
$proto13["m_srcTableName"] = "SchoolType";
$proto13["m_columns"] = array();
$proto13["m_columns"][] = "ID";
$proto13["m_columns"][] = "TypeName";
$proto13["m_columns"][] = "form_master_id";
$obj = new SQLTable($proto13);

$proto12["m_table"] = $obj;
$proto12["m_sql"] = "SchoolType";
$proto12["m_alias"] = "";
$proto12["m_srcTableName"] = "SchoolType";
$proto14=array();
$proto14["m_sql"] = "";
$proto14["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto14["m_column"]=$obj;
$proto14["m_contained"] = array();
$proto14["m_strCase"] = "";
$proto14["m_havingmode"] = false;
$proto14["m_inBrackets"] = false;
$proto14["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto14);

$proto12["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto12);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="SchoolType";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_schooltype = createSqlQuery_schooltype();


	
					
;

			

$tdataschooltype[".sqlquery"] = $queryData_schooltype;



$tableEvents["SchoolType"] = new eventsBase;
$tdataschooltype[".hasEvents"] = false;

?>