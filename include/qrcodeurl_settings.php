<?php
$tdataqrcodeurl = array();
$tdataqrcodeurl[".searchableFields"] = array();
$tdataqrcodeurl[".ShortName"] = "qrcodeurl";
$tdataqrcodeurl[".OwnerID"] = "";
$tdataqrcodeurl[".OriginalTable"] = "QRCodeURL";


$tdataqrcodeurl[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataqrcodeurl[".originalPagesByType"] = $tdataqrcodeurl[".pagesByType"];
$tdataqrcodeurl[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataqrcodeurl[".originalPages"] = $tdataqrcodeurl[".pages"];
$tdataqrcodeurl[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataqrcodeurl[".originalDefaultPages"] = $tdataqrcodeurl[".defaultPages"];

//	field labels
$fieldLabelsqrcodeurl = array();
$fieldToolTipsqrcodeurl = array();
$pageTitlesqrcodeurl = array();
$placeHoldersqrcodeurl = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsqrcodeurl["English"] = array();
	$fieldToolTipsqrcodeurl["English"] = array();
	$placeHoldersqrcodeurl["English"] = array();
	$pageTitlesqrcodeurl["English"] = array();
	$fieldLabelsqrcodeurl["English"]["ID"] = "ID";
	$fieldToolTipsqrcodeurl["English"]["ID"] = "";
	$placeHoldersqrcodeurl["English"]["ID"] = "";
	$fieldLabelsqrcodeurl["English"]["ProgramURL"] = "Program URL";
	$fieldToolTipsqrcodeurl["English"]["ProgramURL"] = "";
	$placeHoldersqrcodeurl["English"]["ProgramURL"] = "";
	$fieldLabelsqrcodeurl["English"]["PageName"] = "Page Name";
	$fieldToolTipsqrcodeurl["English"]["PageName"] = "";
	$placeHoldersqrcodeurl["English"]["PageName"] = "";
	if (count($fieldToolTipsqrcodeurl["English"]))
		$tdataqrcodeurl[".isUseToolTips"] = true;
}


	$tdataqrcodeurl[".NCSearch"] = true;



$tdataqrcodeurl[".shortTableName"] = "qrcodeurl";
$tdataqrcodeurl[".nSecOptions"] = 0;

$tdataqrcodeurl[".mainTableOwnerID"] = "";
$tdataqrcodeurl[".entityType"] = 0;
$tdataqrcodeurl[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdataqrcodeurl[".strOriginalTableName"] = "QRCodeURL";

		 



$tdataqrcodeurl[".showAddInPopup"] = false;

$tdataqrcodeurl[".showEditInPopup"] = false;

$tdataqrcodeurl[".showViewInPopup"] = false;

$tdataqrcodeurl[".listAjax"] = false;
//	temporary
//$tdataqrcodeurl[".listAjax"] = false;

	$tdataqrcodeurl[".audit"] = false;

	$tdataqrcodeurl[".locking"] = false;


$pages = $tdataqrcodeurl[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataqrcodeurl[".edit"] = true;
	$tdataqrcodeurl[".afterEditAction"] = 1;
	$tdataqrcodeurl[".closePopupAfterEdit"] = 1;
	$tdataqrcodeurl[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataqrcodeurl[".add"] = true;
$tdataqrcodeurl[".afterAddAction"] = 1;
$tdataqrcodeurl[".closePopupAfterAdd"] = 1;
$tdataqrcodeurl[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataqrcodeurl[".list"] = true;
}



$tdataqrcodeurl[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataqrcodeurl[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataqrcodeurl[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataqrcodeurl[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataqrcodeurl[".printFriendly"] = true;
}



$tdataqrcodeurl[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataqrcodeurl[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataqrcodeurl[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataqrcodeurl[".isUseAjaxSuggest"] = true;

$tdataqrcodeurl[".rowHighlite"] = true;



						

$tdataqrcodeurl[".ajaxCodeSnippetAdded"] = false;

$tdataqrcodeurl[".buttonsAdded"] = false;

$tdataqrcodeurl[".addPageEvents"] = false;

// use timepicker for search panel
$tdataqrcodeurl[".isUseTimeForSearch"] = false;


$tdataqrcodeurl[".badgeColor"] = "4682B4";


$tdataqrcodeurl[".allSearchFields"] = array();
$tdataqrcodeurl[".filterFields"] = array();
$tdataqrcodeurl[".requiredSearchFields"] = array();

$tdataqrcodeurl[".googleLikeFields"] = array();
$tdataqrcodeurl[".googleLikeFields"][] = "ID";
$tdataqrcodeurl[".googleLikeFields"][] = "ProgramURL";
$tdataqrcodeurl[".googleLikeFields"][] = "PageName";



$tdataqrcodeurl[".tableType"] = "list";

$tdataqrcodeurl[".printerPageOrientation"] = 0;
$tdataqrcodeurl[".nPrinterPageScale"] = 100;

$tdataqrcodeurl[".nPrinterSplitRecords"] = 40;

$tdataqrcodeurl[".geocodingEnabled"] = false;










$tdataqrcodeurl[".pageSize"] = 20;

$tdataqrcodeurl[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdataqrcodeurl[".strOrderBy"] = $tstrOrderBy;

$tdataqrcodeurl[".orderindexes"] = array();


$tdataqrcodeurl[".sqlHead"] = "SELECT ID,  	ProgramURL,  	PageName";
$tdataqrcodeurl[".sqlFrom"] = "FROM QRCodeURL";
$tdataqrcodeurl[".sqlWhereExpr"] = "";
$tdataqrcodeurl[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataqrcodeurl[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataqrcodeurl[".arrGroupsPerPage"] = $arrGPP;

$tdataqrcodeurl[".highlightSearchResults"] = true;

$tableKeysqrcodeurl = array();
$tableKeysqrcodeurl[] = "ID";
$tdataqrcodeurl[".Keys"] = $tableKeysqrcodeurl;


$tdataqrcodeurl[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "QRCodeURL";
	$fdata["Label"] = GetFieldLabel("QRCodeURL","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataqrcodeurl["ID"] = $fdata;
		$tdataqrcodeurl[".searchableFields"][] = "ID";
//	ProgramURL
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "ProgramURL";
	$fdata["GoodName"] = "ProgramURL";
	$fdata["ownerTable"] = "QRCodeURL";
	$fdata["Label"] = GetFieldLabel("QRCodeURL","ProgramURL");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ProgramURL";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ProgramURL";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataqrcodeurl["ProgramURL"] = $fdata;
		$tdataqrcodeurl[".searchableFields"][] = "ProgramURL";
//	PageName
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "PageName";
	$fdata["GoodName"] = "PageName";
	$fdata["ownerTable"] = "QRCodeURL";
	$fdata["Label"] = GetFieldLabel("QRCodeURL","PageName");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "PageName";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "PageName";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataqrcodeurl["PageName"] = $fdata;
		$tdataqrcodeurl[".searchableFields"][] = "PageName";


$tables_data["QRCodeURL"]=&$tdataqrcodeurl;
$field_labels["QRCodeURL"] = &$fieldLabelsqrcodeurl;
$fieldToolTips["QRCodeURL"] = &$fieldToolTipsqrcodeurl;
$placeHolders["QRCodeURL"] = &$placeHoldersqrcodeurl;
$page_titles["QRCodeURL"] = &$pageTitlesqrcodeurl;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["QRCodeURL"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["QRCodeURL"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_qrcodeurl()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	ProgramURL,  	PageName";
$proto0["m_strFrom"] = "FROM QRCodeURL";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "QRCodeURL",
	"m_srcTableName" => "QRCodeURL"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "QRCodeURL";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "ProgramURL",
	"m_strTable" => "QRCodeURL",
	"m_srcTableName" => "QRCodeURL"
));

$proto8["m_sql"] = "ProgramURL";
$proto8["m_srcTableName"] = "QRCodeURL";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "PageName",
	"m_strTable" => "QRCodeURL",
	"m_srcTableName" => "QRCodeURL"
));

$proto10["m_sql"] = "PageName";
$proto10["m_srcTableName"] = "QRCodeURL";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto12=array();
$proto12["m_link"] = "SQLL_MAIN";
			$proto13=array();
$proto13["m_strName"] = "QRCodeURL";
$proto13["m_srcTableName"] = "QRCodeURL";
$proto13["m_columns"] = array();
$proto13["m_columns"][] = "ID";
$proto13["m_columns"][] = "ProgramURL";
$proto13["m_columns"][] = "PageName";
$obj = new SQLTable($proto13);

$proto12["m_table"] = $obj;
$proto12["m_sql"] = "QRCodeURL";
$proto12["m_alias"] = "";
$proto12["m_srcTableName"] = "QRCodeURL";
$proto14=array();
$proto14["m_sql"] = "";
$proto14["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto14["m_column"]=$obj;
$proto14["m_contained"] = array();
$proto14["m_strCase"] = "";
$proto14["m_havingmode"] = false;
$proto14["m_inBrackets"] = false;
$proto14["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto14);

$proto12["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto12);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="QRCodeURL";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_qrcodeurl = createSqlQuery_qrcodeurl();


	
					
;

			

$tdataqrcodeurl[".sqlquery"] = $queryData_qrcodeurl;



$tableEvents["QRCodeURL"] = new eventsBase;
$tdataqrcodeurl[".hasEvents"] = false;

?>