<?php
$tdatafrmhardcopy = array();
$tdatafrmhardcopy[".searchableFields"] = array();
$tdatafrmhardcopy[".ShortName"] = "frmhardcopy";
$tdatafrmhardcopy[".OwnerID"] = "UID";
$tdatafrmhardcopy[".OriginalTable"] = "Form";


$tdatafrmhardcopy[".pagesByType"] = my_json_decode( "{\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdatafrmhardcopy[".originalPagesByType"] = $tdatafrmhardcopy[".pagesByType"];
$tdatafrmhardcopy[".pages"] = types2pages( my_json_decode( "{\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdatafrmhardcopy[".originalPages"] = $tdatafrmhardcopy[".pages"];
$tdatafrmhardcopy[".defaultPages"] = my_json_decode( "{\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatafrmhardcopy[".originalDefaultPages"] = $tdatafrmhardcopy[".defaultPages"];

//	field labels
$fieldLabelsfrmhardcopy = array();
$fieldToolTipsfrmhardcopy = array();
$pageTitlesfrmhardcopy = array();
$placeHoldersfrmhardcopy = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsfrmhardcopy["English"] = array();
	$fieldToolTipsfrmhardcopy["English"] = array();
	$placeHoldersfrmhardcopy["English"] = array();
	$pageTitlesfrmhardcopy["English"] = array();
	$fieldLabelsfrmhardcopy["English"]["ID"] = "Entry ID: ";
	$fieldToolTipsfrmhardcopy["English"]["ID"] = "";
	$placeHoldersfrmhardcopy["English"]["ID"] = "";
	$fieldLabelsfrmhardcopy["English"]["NameOfOrg"] = "Organization";
	$fieldToolTipsfrmhardcopy["English"]["NameOfOrg"] = "";
	$placeHoldersfrmhardcopy["English"]["NameOfOrg"] = "";
	$fieldLabelsfrmhardcopy["English"]["KeyPeople"] = "Key People";
	$fieldToolTipsfrmhardcopy["English"]["KeyPeople"] = "";
	$placeHoldersfrmhardcopy["English"]["KeyPeople"] = "";
	$fieldLabelsfrmhardcopy["English"]["DescriptionOfEntry"] = "Description Of Entry";
	$fieldToolTipsfrmhardcopy["English"]["DescriptionOfEntry"] = "";
	$placeHoldersfrmhardcopy["English"]["DescriptionOfEntry"] = "";
	$fieldLabelsfrmhardcopy["English"]["ParadeOrder"] = "Parade Order";
	$fieldToolTipsfrmhardcopy["English"]["ParadeOrder"] = "";
	$placeHoldersfrmhardcopy["English"]["ParadeOrder"] = "";
	$fieldLabelsfrmhardcopy["English"]["LowerThirds"] = "Lower Thirds";
	$fieldToolTipsfrmhardcopy["English"]["LowerThirds"] = "";
	$placeHoldersfrmhardcopy["English"]["LowerThirds"] = "";
	if (count($fieldToolTipsfrmhardcopy["English"]))
		$tdatafrmhardcopy[".isUseToolTips"] = true;
}


	$tdatafrmhardcopy[".NCSearch"] = true;



$tdatafrmhardcopy[".shortTableName"] = "frmhardcopy";
$tdatafrmhardcopy[".nSecOptions"] = 0;

$tdatafrmhardcopy[".mainTableOwnerID"] = "UID";
$tdatafrmhardcopy[".entityType"] = 1;
$tdatafrmhardcopy[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdatafrmhardcopy[".strOriginalTableName"] = "Form";

		 



$tdatafrmhardcopy[".showAddInPopup"] = false;

$tdatafrmhardcopy[".showEditInPopup"] = false;

$tdatafrmhardcopy[".showViewInPopup"] = false;

$tdatafrmhardcopy[".listAjax"] = false;
//	temporary
//$tdatafrmhardcopy[".listAjax"] = false;

	$tdatafrmhardcopy[".audit"] = false;

	$tdatafrmhardcopy[".locking"] = false;


$pages = $tdatafrmhardcopy[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatafrmhardcopy[".edit"] = true;
	$tdatafrmhardcopy[".afterEditAction"] = 1;
	$tdatafrmhardcopy[".closePopupAfterEdit"] = 1;
	$tdatafrmhardcopy[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatafrmhardcopy[".add"] = true;
$tdatafrmhardcopy[".afterAddAction"] = 1;
$tdatafrmhardcopy[".closePopupAfterAdd"] = 1;
$tdatafrmhardcopy[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatafrmhardcopy[".list"] = true;
}



$tdatafrmhardcopy[".strSortControlSettingsJSON"] = "";

$tdatafrmhardcopy[".strClickActionJSON"] = "{\"fields\":{\"Approved\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactAddress\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactCell\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactEmail\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactFax\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactName\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactPhone\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactZip\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"DescriptionOfEntry\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"EntrySize\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"EntryType\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ID\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"KeyPeople\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"NameOfOrg\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"OnSiteCheckIn\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"SchoolClass\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"TypeOfOrg\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"UID\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}}},\"row\":{\"action\":\"open\",\"codeData\":{},\"gridData\":{\"action\":\"checkbox\",\"table\":null},\"openData\":{\"how\":\"goto\",\"page\":\"view\",\"table\":null,\"url\":\"\"}}}";



if( $pages[PAGE_VIEW] ) {
$tdatafrmhardcopy[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatafrmhardcopy[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatafrmhardcopy[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatafrmhardcopy[".printFriendly"] = true;
}



$tdatafrmhardcopy[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatafrmhardcopy[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatafrmhardcopy[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatafrmhardcopy[".isUseAjaxSuggest"] = true;

$tdatafrmhardcopy[".rowHighlite"] = true;



						

$tdatafrmhardcopy[".ajaxCodeSnippetAdded"] = false;

$tdatafrmhardcopy[".buttonsAdded"] = false;

$tdatafrmhardcopy[".addPageEvents"] = false;

// use timepicker for search panel
$tdatafrmhardcopy[".isUseTimeForSearch"] = false;


$tdatafrmhardcopy[".badgeColor"] = "DB7093";


$tdatafrmhardcopy[".allSearchFields"] = array();
$tdatafrmhardcopy[".filterFields"] = array();
$tdatafrmhardcopy[".requiredSearchFields"] = array();

$tdatafrmhardcopy[".googleLikeFields"] = array();
$tdatafrmhardcopy[".googleLikeFields"][] = "ID";
$tdatafrmhardcopy[".googleLikeFields"][] = "ParadeOrder";
$tdatafrmhardcopy[".googleLikeFields"][] = "NameOfOrg";
$tdatafrmhardcopy[".googleLikeFields"][] = "KeyPeople";
$tdatafrmhardcopy[".googleLikeFields"][] = "DescriptionOfEntry";
$tdatafrmhardcopy[".googleLikeFields"][] = "LowerThirds";



$tdatafrmhardcopy[".tableType"] = "list";

$tdatafrmhardcopy[".printerPageOrientation"] = 0;
$tdatafrmhardcopy[".nPrinterPageScale"] = 100;

$tdatafrmhardcopy[".nPrinterSplitRecords"] = 1;

$tdatafrmhardcopy[".geocodingEnabled"] = false;










$tdatafrmhardcopy[".pageSize"] = 20;

$tdatafrmhardcopy[".warnLeavingPages"] = true;

$tdatafrmhardcopy[".hideEmptyFieldsOnView"] = true;


$tstrOrderBy = "ORDER BY ParadeOrder";
$tdatafrmhardcopy[".strOrderBy"] = $tstrOrderBy;

$tdatafrmhardcopy[".orderindexes"] = array();
	$tdatafrmhardcopy[".orderindexes"][] = array(2, (1 ? "ASC" : "DESC"), "ParadeOrder");



$tdatafrmhardcopy[".sqlHead"] = "SELECT ID,  ParadeOrder,  NameOfOrg,  KeyPeople,  DescriptionOfEntry,  LowerThirds";
$tdatafrmhardcopy[".sqlFrom"] = "FROM Form";
$tdatafrmhardcopy[".sqlWhereExpr"] = "(Approved = 1)";
$tdatafrmhardcopy[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatafrmhardcopy[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatafrmhardcopy[".arrGroupsPerPage"] = $arrGPP;

$tdatafrmhardcopy[".highlightSearchResults"] = true;

$tableKeysfrmhardcopy = array();
$tableKeysfrmhardcopy[] = "ID";
$tdatafrmhardcopy[".Keys"] = $tableKeysfrmhardcopy;


$tdatafrmhardcopy[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("FrmHardCopy","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatafrmhardcopy["ID"] = $fdata;
		$tdatafrmhardcopy[".searchableFields"][] = "ID";
//	ParadeOrder
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "ParadeOrder";
	$fdata["GoodName"] = "ParadeOrder";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("FrmHardCopy","ParadeOrder");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ParadeOrder";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ParadeOrder";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatafrmhardcopy["ParadeOrder"] = $fdata;
		$tdatafrmhardcopy[".searchableFields"][] = "ParadeOrder";
//	NameOfOrg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "NameOfOrg";
	$fdata["GoodName"] = "NameOfOrg";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("FrmHardCopy","NameOfOrg");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "NameOfOrg";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NameOfOrg";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatafrmhardcopy["NameOfOrg"] = $fdata;
		$tdatafrmhardcopy[".searchableFields"][] = "NameOfOrg";
//	KeyPeople
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "KeyPeople";
	$fdata["GoodName"] = "KeyPeople";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("FrmHardCopy","KeyPeople");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "KeyPeople";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "KeyPeople";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatafrmhardcopy["KeyPeople"] = $fdata;
		$tdatafrmhardcopy[".searchableFields"][] = "KeyPeople";
//	DescriptionOfEntry
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "DescriptionOfEntry";
	$fdata["GoodName"] = "DescriptionOfEntry";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("FrmHardCopy","DescriptionOfEntry");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "DescriptionOfEntry";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "DescriptionOfEntry";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatafrmhardcopy["DescriptionOfEntry"] = $fdata;
		$tdatafrmhardcopy[".searchableFields"][] = "DescriptionOfEntry";
//	LowerThirds
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "LowerThirds";
	$fdata["GoodName"] = "LowerThirds";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("FrmHardCopy","LowerThirds");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "LowerThirds";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "LowerThirds";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatafrmhardcopy["LowerThirds"] = $fdata;
		$tdatafrmhardcopy[".searchableFields"][] = "LowerThirds";


$tables_data["FrmHardCopy"]=&$tdatafrmhardcopy;
$field_labels["FrmHardCopy"] = &$fieldLabelsfrmhardcopy;
$fieldToolTips["FrmHardCopy"] = &$fieldToolTipsfrmhardcopy;
$placeHolders["FrmHardCopy"] = &$placeHoldersfrmhardcopy;
$page_titles["FrmHardCopy"] = &$pageTitlesfrmhardcopy;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["FrmHardCopy"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["FrmHardCopy"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_frmhardcopy()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  ParadeOrder,  NameOfOrg,  KeyPeople,  DescriptionOfEntry,  LowerThirds";
$proto0["m_strFrom"] = "FROM Form";
$proto0["m_strWhere"] = "(Approved = 1)";
$proto0["m_strOrderBy"] = "ORDER BY ParadeOrder";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "Approved = 1";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "Approved",
	"m_strTable" => "Form",
	"m_srcTableName" => "FrmHardCopy"
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "= 1";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "Form",
	"m_srcTableName" => "FrmHardCopy"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "FrmHardCopy";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "ParadeOrder",
	"m_strTable" => "Form",
	"m_srcTableName" => "FrmHardCopy"
));

$proto8["m_sql"] = "ParadeOrder";
$proto8["m_srcTableName"] = "FrmHardCopy";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "NameOfOrg",
	"m_strTable" => "Form",
	"m_srcTableName" => "FrmHardCopy"
));

$proto10["m_sql"] = "NameOfOrg";
$proto10["m_srcTableName"] = "FrmHardCopy";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "KeyPeople",
	"m_strTable" => "Form",
	"m_srcTableName" => "FrmHardCopy"
));

$proto12["m_sql"] = "KeyPeople";
$proto12["m_srcTableName"] = "FrmHardCopy";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "DescriptionOfEntry",
	"m_strTable" => "Form",
	"m_srcTableName" => "FrmHardCopy"
));

$proto14["m_sql"] = "DescriptionOfEntry";
$proto14["m_srcTableName"] = "FrmHardCopy";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "LowerThirds",
	"m_strTable" => "Form",
	"m_srcTableName" => "FrmHardCopy"
));

$proto16["m_sql"] = "LowerThirds";
$proto16["m_srcTableName"] = "FrmHardCopy";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto18=array();
$proto18["m_link"] = "SQLL_MAIN";
			$proto19=array();
$proto19["m_strName"] = "Form";
$proto19["m_srcTableName"] = "FrmHardCopy";
$proto19["m_columns"] = array();
$proto19["m_columns"][] = "ID";
$proto19["m_columns"][] = "NameOfOrg";
$proto19["m_columns"][] = "TypeOfOrg";
$proto19["m_columns"][] = "SchoolClass";
$proto19["m_columns"][] = "EntryType";
$proto19["m_columns"][] = "KeyPeople";
$proto19["m_columns"][] = "EntrySize";
$proto19["m_columns"][] = "ContactName";
$proto19["m_columns"][] = "ContactAddress";
$proto19["m_columns"][] = "ContactEmail";
$proto19["m_columns"][] = "ContactZip";
$proto19["m_columns"][] = "ContactPhone";
$proto19["m_columns"][] = "ContactFax";
$proto19["m_columns"][] = "ContactCell";
$proto19["m_columns"][] = "DescriptionOfEntry";
$proto19["m_columns"][] = "UID";
$proto19["m_columns"][] = "NumOfHorses";
$proto19["m_columns"][] = "Approved";
$proto19["m_columns"][] = "LowerThirds";
$proto19["m_columns"][] = "ParadeOrder";
$proto19["m_columns"][] = "GroupOrganizer";
$proto19["m_columns"][] = "AgreetoRules";
$proto19["m_columns"][] = "LiabilityWavierSigned";
$obj = new SQLTable($proto19);

$proto18["m_table"] = $obj;
$proto18["m_sql"] = "Form";
$proto18["m_alias"] = "";
$proto18["m_srcTableName"] = "FrmHardCopy";
$proto20=array();
$proto20["m_sql"] = "";
$proto20["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto20["m_column"]=$obj;
$proto20["m_contained"] = array();
$proto20["m_strCase"] = "";
$proto20["m_havingmode"] = false;
$proto20["m_inBrackets"] = false;
$proto20["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto20);

$proto18["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto18);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto22=array();
						$obj = new SQLField(array(
	"m_strName" => "ParadeOrder",
	"m_strTable" => "Form",
	"m_srcTableName" => "FrmHardCopy"
));

$proto22["m_column"]=$obj;
$proto22["m_bAsc"] = 1;
$proto22["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto22);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="FrmHardCopy";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_frmhardcopy = createSqlQuery_frmhardcopy();


	
					
;

						

$tdatafrmhardcopy[".sqlquery"] = $queryData_frmhardcopy;



include_once(getabspath("include/frmhardcopy_events.php"));
$tableEvents["FrmHardCopy"] = new eventclass_frmhardcopy;
$tdatafrmhardcopy[".hasEvents"] = true;

?>