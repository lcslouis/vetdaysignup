<?php
$tdatayesno = array();
$tdatayesno[".searchableFields"] = array();
$tdatayesno[".ShortName"] = "yesno";
$tdatayesno[".OwnerID"] = "";
$tdatayesno[".OriginalTable"] = "YesNo";


$tdatayesno[".pagesByType"] = my_json_decode( "{\"edit\":[\"edit\"],\"list\":[\"list\"],\"search\":[\"search\"]}" );
$tdatayesno[".originalPagesByType"] = $tdatayesno[".pagesByType"];
$tdatayesno[".pages"] = types2pages( my_json_decode( "{\"edit\":[\"edit\"],\"list\":[\"list\"],\"search\":[\"search\"]}" ) );
$tdatayesno[".originalPages"] = $tdatayesno[".pages"];
$tdatayesno[".defaultPages"] = my_json_decode( "{\"edit\":\"edit\",\"list\":\"list\",\"search\":\"search\"}" );
$tdatayesno[".originalDefaultPages"] = $tdatayesno[".defaultPages"];

//	field labels
$fieldLabelsyesno = array();
$fieldToolTipsyesno = array();
$pageTitlesyesno = array();
$placeHoldersyesno = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsyesno["English"] = array();
	$fieldToolTipsyesno["English"] = array();
	$placeHoldersyesno["English"] = array();
	$pageTitlesyesno["English"] = array();
	$fieldLabelsyesno["English"]["ID"] = "ID";
	$fieldToolTipsyesno["English"]["ID"] = "";
	$placeHoldersyesno["English"]["ID"] = "";
	$fieldLabelsyesno["English"]["OptionName"] = "Option Name";
	$fieldToolTipsyesno["English"]["OptionName"] = "";
	$placeHoldersyesno["English"]["OptionName"] = "";
	$fieldLabelsyesno["English"]["OptionValue"] = "Option Value";
	$fieldToolTipsyesno["English"]["OptionValue"] = "";
	$placeHoldersyesno["English"]["OptionValue"] = "";
	$fieldLabelsyesno["English"]["AlternateName"] = "Alternate Name";
	$fieldToolTipsyesno["English"]["AlternateName"] = "";
	$placeHoldersyesno["English"]["AlternateName"] = "";
	if (count($fieldToolTipsyesno["English"]))
		$tdatayesno[".isUseToolTips"] = true;
}


	$tdatayesno[".NCSearch"] = true;



$tdatayesno[".shortTableName"] = "yesno";
$tdatayesno[".nSecOptions"] = 0;

$tdatayesno[".mainTableOwnerID"] = "";
$tdatayesno[".entityType"] = 0;
$tdatayesno[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdatayesno[".strOriginalTableName"] = "YesNo";

		 



$tdatayesno[".showAddInPopup"] = false;

$tdatayesno[".showEditInPopup"] = false;

$tdatayesno[".showViewInPopup"] = false;

$tdatayesno[".listAjax"] = false;
//	temporary
//$tdatayesno[".listAjax"] = false;

	$tdatayesno[".audit"] = false;

	$tdatayesno[".locking"] = false;


$pages = $tdatayesno[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatayesno[".edit"] = true;
	$tdatayesno[".afterEditAction"] = 1;
	$tdatayesno[".closePopupAfterEdit"] = 1;
	$tdatayesno[".afterEditActionDetTable"] = "Detail tables not found!";
}

if( $pages[PAGE_ADD] ) {
$tdatayesno[".add"] = true;
$tdatayesno[".afterAddAction"] = 1;
$tdatayesno[".closePopupAfterAdd"] = 1;
$tdatayesno[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatayesno[".list"] = true;
}



$tdatayesno[".strSortControlSettingsJSON"] = "";

$tdatayesno[".strClickActionJSON"] = "{\"fields\":{\"ID\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"OptionName\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"OptionValue\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}}},\"row\":{\"action\":\"open\",\"codeData\":{},\"gridData\":{\"action\":\"checkbox\",\"table\":null},\"openData\":{\"how\":\"goto\",\"page\":\"edit\",\"table\":null,\"url\":\"\"}}}";



if( $pages[PAGE_VIEW] ) {
$tdatayesno[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatayesno[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatayesno[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatayesno[".printFriendly"] = true;
}



$tdatayesno[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatayesno[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatayesno[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatayesno[".isUseAjaxSuggest"] = true;

$tdatayesno[".rowHighlite"] = true;



						

$tdatayesno[".ajaxCodeSnippetAdded"] = false;

$tdatayesno[".buttonsAdded"] = false;

$tdatayesno[".addPageEvents"] = false;

// use timepicker for search panel
$tdatayesno[".isUseTimeForSearch"] = false;


$tdatayesno[".badgeColor"] = "7B68EE";


$tdatayesno[".allSearchFields"] = array();
$tdatayesno[".filterFields"] = array();
$tdatayesno[".requiredSearchFields"] = array();

$tdatayesno[".googleLikeFields"] = array();
$tdatayesno[".googleLikeFields"][] = "ID";
$tdatayesno[".googleLikeFields"][] = "OptionName";
$tdatayesno[".googleLikeFields"][] = "OptionValue";
$tdatayesno[".googleLikeFields"][] = "AlternateName";



$tdatayesno[".tableType"] = "list";

$tdatayesno[".printerPageOrientation"] = 0;
$tdatayesno[".nPrinterPageScale"] = 100;

$tdatayesno[".nPrinterSplitRecords"] = 40;

$tdatayesno[".geocodingEnabled"] = false;










$tdatayesno[".pageSize"] = 20;

$tdatayesno[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdatayesno[".strOrderBy"] = $tstrOrderBy;

$tdatayesno[".orderindexes"] = array();


$tdatayesno[".sqlHead"] = "SELECT ID,  	OptionName,  	OptionValue,  	AlternateName";
$tdatayesno[".sqlFrom"] = "FROM YesNo";
$tdatayesno[".sqlWhereExpr"] = "";
$tdatayesno[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatayesno[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatayesno[".arrGroupsPerPage"] = $arrGPP;

$tdatayesno[".highlightSearchResults"] = true;

$tableKeysyesno = array();
$tableKeysyesno[] = "ID";
$tdatayesno[".Keys"] = $tableKeysyesno;


$tdatayesno[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "YesNo";
	$fdata["Label"] = GetFieldLabel("YesNo","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatayesno["ID"] = $fdata;
		$tdatayesno[".searchableFields"][] = "ID";
//	OptionName
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "OptionName";
	$fdata["GoodName"] = "OptionName";
	$fdata["ownerTable"] = "YesNo";
	$fdata["Label"] = GetFieldLabel("YesNo","OptionName");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "OptionName";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "OptionName";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatayesno["OptionName"] = $fdata;
		$tdatayesno[".searchableFields"][] = "OptionName";
//	OptionValue
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "OptionValue";
	$fdata["GoodName"] = "OptionValue";
	$fdata["ownerTable"] = "YesNo";
	$fdata["Label"] = GetFieldLabel("YesNo","OptionValue");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "OptionValue";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "OptionValue";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatayesno["OptionValue"] = $fdata;
		$tdatayesno[".searchableFields"][] = "OptionValue";
//	AlternateName
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "AlternateName";
	$fdata["GoodName"] = "AlternateName";
	$fdata["ownerTable"] = "YesNo";
	$fdata["Label"] = GetFieldLabel("YesNo","AlternateName");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "AlternateName";

		$fdata["sourceSingle"] = "AlternateName";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "AlternateName";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatayesno["AlternateName"] = $fdata;
		$tdatayesno[".searchableFields"][] = "AlternateName";


$tables_data["YesNo"]=&$tdatayesno;
$field_labels["YesNo"] = &$fieldLabelsyesno;
$fieldToolTips["YesNo"] = &$fieldToolTipsyesno;
$placeHolders["YesNo"] = &$placeHoldersyesno;
$page_titles["YesNo"] = &$pageTitlesyesno;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["YesNo"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["YesNo"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_yesno()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	OptionName,  	OptionValue,  	AlternateName";
$proto0["m_strFrom"] = "FROM YesNo";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "YesNo",
	"m_srcTableName" => "YesNo"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "YesNo";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "OptionName",
	"m_strTable" => "YesNo",
	"m_srcTableName" => "YesNo"
));

$proto8["m_sql"] = "OptionName";
$proto8["m_srcTableName"] = "YesNo";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "OptionValue",
	"m_strTable" => "YesNo",
	"m_srcTableName" => "YesNo"
));

$proto10["m_sql"] = "OptionValue";
$proto10["m_srcTableName"] = "YesNo";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "AlternateName",
	"m_strTable" => "YesNo",
	"m_srcTableName" => "YesNo"
));

$proto12["m_sql"] = "AlternateName";
$proto12["m_srcTableName"] = "YesNo";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto14=array();
$proto14["m_link"] = "SQLL_MAIN";
			$proto15=array();
$proto15["m_strName"] = "YesNo";
$proto15["m_srcTableName"] = "YesNo";
$proto15["m_columns"] = array();
$proto15["m_columns"][] = "ID";
$proto15["m_columns"][] = "OptionName";
$proto15["m_columns"][] = "OptionValue";
$proto15["m_columns"][] = "AlternateName";
$obj = new SQLTable($proto15);

$proto14["m_table"] = $obj;
$proto14["m_sql"] = "YesNo";
$proto14["m_alias"] = "";
$proto14["m_srcTableName"] = "YesNo";
$proto16=array();
$proto16["m_sql"] = "";
$proto16["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto16["m_column"]=$obj;
$proto16["m_contained"] = array();
$proto16["m_strCase"] = "";
$proto16["m_havingmode"] = false;
$proto16["m_inBrackets"] = false;
$proto16["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto16);

$proto14["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto14);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="YesNo";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_yesno = createSqlQuery_yesno();


	
					
;

				

$tdatayesno[".sqlquery"] = $queryData_yesno;



$tableEvents["YesNo"] = new eventsBase;
$tdatayesno[".hasEvents"] = false;

?>