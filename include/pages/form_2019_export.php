<?php
			$optionsArray = array( 'totals' => array( 'ID' => array( 'totalsType' => '' ),
'NameOfOrg' => array( 'totalsType' => '' ),
'TypeOfOrg' => array( 'totalsType' => '' ),
'SchoolClass' => array( 'totalsType' => '' ),
'EntryType' => array( 'totalsType' => '' ),
'KeyPeople' => array( 'totalsType' => '' ),
'EntrySize' => array( 'totalsType' => '' ),
'ContactName' => array( 'totalsType' => '' ),
'ContactAddress' => array( 'totalsType' => '' ),
'ContactEmail' => array( 'totalsType' => '' ),
'ContactZip' => array( 'totalsType' => '' ),
'ContactPhone' => array( 'totalsType' => '' ),
'ContactFax' => array( 'totalsType' => '' ),
'ContactCell' => array( 'totalsType' => '' ),
'DescriptionOfEntry' => array( 'totalsType' => '' ),
'UID' => array( 'totalsType' => '' ),
'Approved' => array( 'totalsType' => '' ),
'NumOfHorses' => array( 'totalsType' => '' ),
'LowerThirds' => array( 'totalsType' => '' ),
'ParadeOrder' => array( 'totalsType' => '' ) ),
'fields' => array( 'gridFields' => array( 'Approved',
'ContactAddress',
'ContactCell',
'ContactEmail',
'ContactFax',
'ContactName',
'ContactPhone',
'ContactZip',
'DescriptionOfEntry',
'EntrySize',
'EntryType',
'ID',
'KeyPeople',
'LowerThirds',
'NameOfOrg',
'NumOfHorses',
'ParadeOrder',
'SchoolClass',
'TypeOfOrg',
'UID' ),
'exportFields' => array( 'Approved',
'ContactAddress',
'ContactCell',
'ContactEmail',
'ContactFax',
'ContactName',
'ContactPhone',
'ContactZip',
'DescriptionOfEntry',
'EntrySize',
'EntryType',
'ID',
'KeyPeople',
'LowerThirds',
'NameOfOrg',
'NumOfHorses',
'ParadeOrder',
'SchoolClass',
'TypeOfOrg',
'UID' ),
'searchRequiredFields' => array(  ),
'searchPanelFields' => array(  ),
'fieldItems' => array( 'Approved' => array( 'export_field' ),
'ContactAddress' => array( 'export_field1' ),
'ContactCell' => array( 'export_field2' ),
'ContactEmail' => array( 'export_field3' ),
'ContactFax' => array( 'export_field4' ),
'ContactName' => array( 'export_field5' ),
'ContactPhone' => array( 'export_field6' ),
'ContactZip' => array( 'export_field7' ),
'DescriptionOfEntry' => array( 'export_field8' ),
'EntrySize' => array( 'export_field9' ),
'EntryType' => array( 'export_field10' ),
'ID' => array( 'export_field11' ),
'KeyPeople' => array( 'export_field12' ),
'LowerThirds' => array( 'export_field13' ),
'NameOfOrg' => array( 'export_field14' ),
'NumOfHorses' => array( 'export_field15' ),
'ParadeOrder' => array( 'export_field16' ),
'SchoolClass' => array( 'export_field17' ),
'TypeOfOrg' => array( 'export_field18' ),
'UID' => array( 'export_field19' ) ) ),
'pageLinks' => array( 'edit' => false,
'add' => false,
'view' => false,
'print' => false ),
'layoutHelper' => array( 'formItems' => array( 'formItems' => array( 'supertop' => array(  ),
'top' => array( 'export_header' ),
'grid' => array( 'export_field',
'export_field1',
'export_field2',
'export_field3',
'export_field4',
'export_field5',
'export_field6',
'export_field7',
'export_field8',
'export_field9',
'export_field10',
'export_field11',
'export_field12',
'export_field13',
'export_field14',
'export_field15',
'export_field16',
'export_field17',
'export_field18',
'export_field19' ),
'footer' => array( 'export_export',
'export_cancel' ) ),
'formXtTags' => array( 'supertop' => array(  ) ),
'itemForms' => array( 'export_header' => 'top',
'export_field' => 'grid',
'export_field1' => 'grid',
'export_field2' => 'grid',
'export_field3' => 'grid',
'export_field4' => 'grid',
'export_field5' => 'grid',
'export_field6' => 'grid',
'export_field7' => 'grid',
'export_field8' => 'grid',
'export_field9' => 'grid',
'export_field10' => 'grid',
'export_field11' => 'grid',
'export_field12' => 'grid',
'export_field13' => 'grid',
'export_field14' => 'grid',
'export_field15' => 'grid',
'export_field16' => 'grid',
'export_field17' => 'grid',
'export_field18' => 'grid',
'export_field19' => 'grid',
'export_export' => 'footer',
'export_cancel' => 'footer' ),
'itemLocations' => array(  ),
'itemVisiblity' => array(  ) ),
'itemsByType' => array( 'export_header' => array( 'export_header' ),
'export_export' => array( 'export_export' ),
'export_cancel' => array( 'export_cancel' ),
'export_field' => array( 'export_field',
'export_field1',
'export_field2',
'export_field3',
'export_field4',
'export_field5',
'export_field6',
'export_field7',
'export_field8',
'export_field9',
'export_field10',
'export_field11',
'export_field12',
'export_field13',
'export_field14',
'export_field15',
'export_field16',
'export_field17',
'export_field18',
'export_field19' ) ),
'cellMaps' => array(  ) ),
'loginForm' => array( 'loginForm' => 3 ),
'page' => array( 'verticalBar' => false,
'labeledButtons' => array( 'update_records' => array(  ),
'print_pages' => array(  ),
'register_activate_message' => array(  ),
'details_found' => array(  ) ),
'hasCustomButtons' => false,
'customButtons' => array(  ) ),
'events' => array( 'maps' => array(  ),
'mapsData' => array(  ),
'buttons' => array(  ) ),
'export' => array( 'format' => 2,
'selectFields' => false,
'delimiter' => ',',
'selectDelimiter' => false,
'exportFileTypes' => array( 'excel' => true,
'word' => true,
'csv' => true,
'xml' => false ) ) );
			$pageArray = array( 'id' => 'export',
'type' => 'export',
'layoutId' => 'first',
'disabled' => 0,
'default' => 0,
'forms' => array( 'supertop' => array( 'modelId' => 'panel-top',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array(  ) ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'top' => array( 'modelId' => 'export-header',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'export_header' ) ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'grid' => array( 'modelId' => 'export-grid',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array( 'export_field',
'export_field1',
'export_field2',
'export_field3',
'export_field4',
'export_field5',
'export_field6',
'export_field7',
'export_field8',
'export_field9',
'export_field10',
'export_field11',
'export_field12',
'export_field13',
'export_field14',
'export_field15',
'export_field16',
'export_field17',
'export_field18',
'export_field19' ) ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ),
'footer' => array( 'modelId' => 'export-footer',
'grid' => array( array( 'cells' => array( array( 'cell' => 'c1' ),
array( 'cell' => 'c2' ) ),
'section' => '' ) ),
'cells' => array( 'c1' => array( 'model' => 'c1',
'items' => array(  ) ),
'c2' => array( 'model' => 'c2',
'items' => array( 'export_export',
'export_cancel' ) ) ),
'deferredItems' => array(  ),
'recsPerRow' => 1 ) ),
'items' => array( 'export_header' => array( 'type' => 'export_header' ),
'export_export' => array( 'type' => 'export_export' ),
'export_cancel' => array( 'type' => 'export_cancel' ),
'export_field' => array( 'field' => 'Approved',
'type' => 'export_field' ),
'export_field1' => array( 'field' => 'ContactAddress',
'type' => 'export_field' ),
'export_field2' => array( 'field' => 'ContactCell',
'type' => 'export_field' ),
'export_field3' => array( 'field' => 'ContactEmail',
'type' => 'export_field' ),
'export_field4' => array( 'field' => 'ContactFax',
'type' => 'export_field' ),
'export_field5' => array( 'field' => 'ContactName',
'type' => 'export_field' ),
'export_field6' => array( 'field' => 'ContactPhone',
'type' => 'export_field' ),
'export_field7' => array( 'field' => 'ContactZip',
'type' => 'export_field' ),
'export_field8' => array( 'field' => 'DescriptionOfEntry',
'type' => 'export_field' ),
'export_field9' => array( 'field' => 'EntrySize',
'type' => 'export_field' ),
'export_field10' => array( 'field' => 'EntryType',
'type' => 'export_field' ),
'export_field11' => array( 'field' => 'ID',
'type' => 'export_field' ),
'export_field12' => array( 'field' => 'KeyPeople',
'type' => 'export_field' ),
'export_field13' => array( 'field' => 'LowerThirds',
'type' => 'export_field' ),
'export_field14' => array( 'field' => 'NameOfOrg',
'type' => 'export_field' ),
'export_field15' => array( 'field' => 'NumOfHorses',
'type' => 'export_field' ),
'export_field16' => array( 'field' => 'ParadeOrder',
'type' => 'export_field' ),
'export_field17' => array( 'field' => 'SchoolClass',
'type' => 'export_field' ),
'export_field18' => array( 'field' => 'TypeOfOrg',
'type' => 'export_field' ),
'export_field19' => array( 'field' => 'UID',
'type' => 'export_field' ) ),
'dbProps' => array(  ),
'version' => 7,
'exportFormat' => 2,
'exportDelimiter' => ',',
'exportSelectDelimiter' => false,
'exportSelectFields' => false );
		?>