<?php
$tdataannouncer2023 = array();
$tdataannouncer2023[".searchableFields"] = array();
$tdataannouncer2023[".ShortName"] = "announcer2023";
$tdataannouncer2023[".OwnerID"] = "";
$tdataannouncer2023[".OriginalTable"] = "announcer2023";


$tdataannouncer2023[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" );
$tdataannouncer2023[".originalPagesByType"] = $tdataannouncer2023[".pagesByType"];
$tdataannouncer2023[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\"]}" ) );
$tdataannouncer2023[".originalPages"] = $tdataannouncer2023[".pages"];
$tdataannouncer2023[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataannouncer2023[".originalDefaultPages"] = $tdataannouncer2023[".defaultPages"];

//	field labels
$fieldLabelsannouncer2023 = array();
$fieldToolTipsannouncer2023 = array();
$pageTitlesannouncer2023 = array();
$placeHoldersannouncer2023 = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsannouncer2023["English"] = array();
	$fieldToolTipsannouncer2023["English"] = array();
	$placeHoldersannouncer2023["English"] = array();
	$pageTitlesannouncer2023["English"] = array();
	$fieldLabelsannouncer2023["English"]["ID"] = "ID";
	$fieldToolTipsannouncer2023["English"]["ID"] = "";
	$placeHoldersannouncer2023["English"]["ID"] = "";
	$fieldLabelsannouncer2023["English"]["FormID"] = "Form ID";
	$fieldToolTipsannouncer2023["English"]["FormID"] = "";
	$placeHoldersannouncer2023["English"]["FormID"] = "";
	$fieldLabelsannouncer2023["English"]["NameOfOrg"] = "Name Of Org";
	$fieldToolTipsannouncer2023["English"]["NameOfOrg"] = "";
	$placeHoldersannouncer2023["English"]["NameOfOrg"] = "";
	$fieldLabelsannouncer2023["English"]["KeyPeople"] = "Key People";
	$fieldToolTipsannouncer2023["English"]["KeyPeople"] = "";
	$placeHoldersannouncer2023["English"]["KeyPeople"] = "";
	$fieldLabelsannouncer2023["English"]["DescriptionOfEntry"] = "Description Of Entry";
	$fieldToolTipsannouncer2023["English"]["DescriptionOfEntry"] = "";
	$placeHoldersannouncer2023["English"]["DescriptionOfEntry"] = "";
	$fieldLabelsannouncer2023["English"]["LowerThirds"] = "Lower Thirds";
	$fieldToolTipsannouncer2023["English"]["LowerThirds"] = "";
	$placeHoldersannouncer2023["English"]["LowerThirds"] = "";
	if (count($fieldToolTipsannouncer2023["English"]))
		$tdataannouncer2023[".isUseToolTips"] = true;
}


	$tdataannouncer2023[".NCSearch"] = true;



$tdataannouncer2023[".shortTableName"] = "announcer2023";
$tdataannouncer2023[".nSecOptions"] = 0;

$tdataannouncer2023[".mainTableOwnerID"] = "";
$tdataannouncer2023[".entityType"] = 0;
$tdataannouncer2023[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdataannouncer2023[".strOriginalTableName"] = "announcer2023";

		 



$tdataannouncer2023[".showAddInPopup"] = false;

$tdataannouncer2023[".showEditInPopup"] = false;

$tdataannouncer2023[".showViewInPopup"] = false;

$tdataannouncer2023[".listAjax"] = false;
//	temporary
//$tdataannouncer2023[".listAjax"] = false;

	$tdataannouncer2023[".audit"] = false;

	$tdataannouncer2023[".locking"] = false;


$pages = $tdataannouncer2023[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataannouncer2023[".edit"] = true;
	$tdataannouncer2023[".afterEditAction"] = 1;
	$tdataannouncer2023[".closePopupAfterEdit"] = 1;
	$tdataannouncer2023[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataannouncer2023[".add"] = true;
$tdataannouncer2023[".afterAddAction"] = 1;
$tdataannouncer2023[".closePopupAfterAdd"] = 1;
$tdataannouncer2023[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdataannouncer2023[".list"] = true;
}



$tdataannouncer2023[".strSortControlSettingsJSON"] = "";




if( $pages[PAGE_VIEW] ) {
$tdataannouncer2023[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataannouncer2023[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataannouncer2023[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataannouncer2023[".printFriendly"] = true;
}



$tdataannouncer2023[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataannouncer2023[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataannouncer2023[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataannouncer2023[".isUseAjaxSuggest"] = true;

$tdataannouncer2023[".rowHighlite"] = true;



						

$tdataannouncer2023[".ajaxCodeSnippetAdded"] = false;

$tdataannouncer2023[".buttonsAdded"] = false;

$tdataannouncer2023[".addPageEvents"] = false;

// use timepicker for search panel
$tdataannouncer2023[".isUseTimeForSearch"] = false;


$tdataannouncer2023[".badgeColor"] = "6DA5C8";


$tdataannouncer2023[".allSearchFields"] = array();
$tdataannouncer2023[".filterFields"] = array();
$tdataannouncer2023[".requiredSearchFields"] = array();

$tdataannouncer2023[".googleLikeFields"] = array();
$tdataannouncer2023[".googleLikeFields"][] = "ID";
$tdataannouncer2023[".googleLikeFields"][] = "FormID";
$tdataannouncer2023[".googleLikeFields"][] = "NameOfOrg";
$tdataannouncer2023[".googleLikeFields"][] = "KeyPeople";
$tdataannouncer2023[".googleLikeFields"][] = "DescriptionOfEntry";
$tdataannouncer2023[".googleLikeFields"][] = "LowerThirds";



$tdataannouncer2023[".tableType"] = "list";

$tdataannouncer2023[".printerPageOrientation"] = 0;
$tdataannouncer2023[".nPrinterPageScale"] = 100;

$tdataannouncer2023[".nPrinterSplitRecords"] = 40;

$tdataannouncer2023[".geocodingEnabled"] = false;










$tdataannouncer2023[".pageSize"] = 20;

$tdataannouncer2023[".warnLeavingPages"] = true;



$tstrOrderBy = "";
$tdataannouncer2023[".strOrderBy"] = $tstrOrderBy;

$tdataannouncer2023[".orderindexes"] = array();


$tdataannouncer2023[".sqlHead"] = "SELECT ID,  	FormID,  	NameOfOrg,  	KeyPeople,  	DescriptionOfEntry,  	LowerThirds";
$tdataannouncer2023[".sqlFrom"] = "FROM announcer2023";
$tdataannouncer2023[".sqlWhereExpr"] = "";
$tdataannouncer2023[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataannouncer2023[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataannouncer2023[".arrGroupsPerPage"] = $arrGPP;

$tdataannouncer2023[".highlightSearchResults"] = true;

$tableKeysannouncer2023 = array();
$tableKeysannouncer2023[] = "ID";
$tdataannouncer2023[".Keys"] = $tableKeysannouncer2023;


$tdataannouncer2023[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "announcer2023";
	$fdata["Label"] = GetFieldLabel("announcer2023","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

		$fdata["sourceSingle"] = "ID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2023["ID"] = $fdata;
		$tdataannouncer2023[".searchableFields"][] = "ID";
//	FormID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "FormID";
	$fdata["GoodName"] = "FormID";
	$fdata["ownerTable"] = "announcer2023";
	$fdata["Label"] = GetFieldLabel("announcer2023","FormID");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "FormID";

		$fdata["sourceSingle"] = "FormID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "FormID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2023["FormID"] = $fdata;
		$tdataannouncer2023[".searchableFields"][] = "FormID";
//	NameOfOrg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "NameOfOrg";
	$fdata["GoodName"] = "NameOfOrg";
	$fdata["ownerTable"] = "announcer2023";
	$fdata["Label"] = GetFieldLabel("announcer2023","NameOfOrg");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "NameOfOrg";

		$fdata["sourceSingle"] = "NameOfOrg";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NameOfOrg";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2023["NameOfOrg"] = $fdata;
		$tdataannouncer2023[".searchableFields"][] = "NameOfOrg";
//	KeyPeople
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "KeyPeople";
	$fdata["GoodName"] = "KeyPeople";
	$fdata["ownerTable"] = "announcer2023";
	$fdata["Label"] = GetFieldLabel("announcer2023","KeyPeople");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "KeyPeople";

		$fdata["sourceSingle"] = "KeyPeople";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "KeyPeople";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2023["KeyPeople"] = $fdata;
		$tdataannouncer2023[".searchableFields"][] = "KeyPeople";
//	DescriptionOfEntry
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "DescriptionOfEntry";
	$fdata["GoodName"] = "DescriptionOfEntry";
	$fdata["ownerTable"] = "announcer2023";
	$fdata["Label"] = GetFieldLabel("announcer2023","DescriptionOfEntry");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "DescriptionOfEntry";

		$fdata["sourceSingle"] = "DescriptionOfEntry";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "DescriptionOfEntry";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2023["DescriptionOfEntry"] = $fdata;
		$tdataannouncer2023[".searchableFields"][] = "DescriptionOfEntry";
//	LowerThirds
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "LowerThirds";
	$fdata["GoodName"] = "LowerThirds";
	$fdata["ownerTable"] = "announcer2023";
	$fdata["Label"] = GetFieldLabel("announcer2023","LowerThirds");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "LowerThirds";

		$fdata["sourceSingle"] = "LowerThirds";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "LowerThirds";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataannouncer2023["LowerThirds"] = $fdata;
		$tdataannouncer2023[".searchableFields"][] = "LowerThirds";


$tables_data["announcer2023"]=&$tdataannouncer2023;
$field_labels["announcer2023"] = &$fieldLabelsannouncer2023;
$fieldToolTips["announcer2023"] = &$fieldToolTipsannouncer2023;
$placeHolders["announcer2023"] = &$placeHoldersannouncer2023;
$page_titles["announcer2023"] = &$pageTitlesannouncer2023;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["announcer2023"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["announcer2023"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_announcer2023()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	FormID,  	NameOfOrg,  	KeyPeople,  	DescriptionOfEntry,  	LowerThirds";
$proto0["m_strFrom"] = "FROM announcer2023";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "announcer2023",
	"m_srcTableName" => "announcer2023"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "announcer2023";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "FormID",
	"m_strTable" => "announcer2023",
	"m_srcTableName" => "announcer2023"
));

$proto8["m_sql"] = "FormID";
$proto8["m_srcTableName"] = "announcer2023";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "NameOfOrg",
	"m_strTable" => "announcer2023",
	"m_srcTableName" => "announcer2023"
));

$proto10["m_sql"] = "NameOfOrg";
$proto10["m_srcTableName"] = "announcer2023";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "KeyPeople",
	"m_strTable" => "announcer2023",
	"m_srcTableName" => "announcer2023"
));

$proto12["m_sql"] = "KeyPeople";
$proto12["m_srcTableName"] = "announcer2023";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "DescriptionOfEntry",
	"m_strTable" => "announcer2023",
	"m_srcTableName" => "announcer2023"
));

$proto14["m_sql"] = "DescriptionOfEntry";
$proto14["m_srcTableName"] = "announcer2023";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "LowerThirds",
	"m_strTable" => "announcer2023",
	"m_srcTableName" => "announcer2023"
));

$proto16["m_sql"] = "LowerThirds";
$proto16["m_srcTableName"] = "announcer2023";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto18=array();
$proto18["m_link"] = "SQLL_MAIN";
			$proto19=array();
$proto19["m_strName"] = "announcer2023";
$proto19["m_srcTableName"] = "announcer2023";
$proto19["m_columns"] = array();
$proto19["m_columns"][] = "ID";
$proto19["m_columns"][] = "FormID";
$proto19["m_columns"][] = "NameOfOrg";
$proto19["m_columns"][] = "KeyPeople";
$proto19["m_columns"][] = "DescriptionOfEntry";
$proto19["m_columns"][] = "LowerThirds";
$obj = new SQLTable($proto19);

$proto18["m_table"] = $obj;
$proto18["m_sql"] = "announcer2023";
$proto18["m_alias"] = "";
$proto18["m_srcTableName"] = "announcer2023";
$proto20=array();
$proto20["m_sql"] = "";
$proto20["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto20["m_column"]=$obj;
$proto20["m_contained"] = array();
$proto20["m_strCase"] = "";
$proto20["m_havingmode"] = false;
$proto20["m_inBrackets"] = false;
$proto20["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto20);

$proto18["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto18);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="announcer2023";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_announcer2023 = createSqlQuery_announcer2023();


	
					
;

						

$tdataannouncer2023[".sqlquery"] = $queryData_announcer2023;



$tableEvents["announcer2023"] = new eventsBase;
$tdataannouncer2023[".hasEvents"] = false;

?>