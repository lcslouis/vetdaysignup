<?php
$tdataEntry = array();
$tdataEntry[".searchableFields"] = array();
$tdataEntry[".ShortName"] = "Entry";
$tdataEntry[".OwnerID"] = "UID";
$tdataEntry[".OriginalTable"] = "Form";


$tdataEntry[".pagesByType"] = my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\",\"announcerSelection\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\",\"view1\",\"confirmation\",\"confirmation1\"]}" );
$tdataEntry[".originalPagesByType"] = $tdataEntry[".pagesByType"];
$tdataEntry[".pages"] = types2pages( my_json_decode( "{\"add\":[\"add\"],\"edit\":[\"edit\"],\"export\":[\"export\"],\"import\":[\"import\"],\"list\":[\"list\",\"announcerSelection\"],\"print\":[\"print\"],\"search\":[\"search\"],\"view\":[\"view\",\"view1\",\"confirmation\",\"confirmation1\"]}" ) );
$tdataEntry[".originalPages"] = $tdataEntry[".pages"];
$tdataEntry[".defaultPages"] = my_json_decode( "{\"add\":\"add\",\"edit\":\"edit\",\"export\":\"export\",\"import\":\"import\",\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdataEntry[".originalDefaultPages"] = $tdataEntry[".defaultPages"];

//	field labels
$fieldLabelsEntry = array();
$fieldToolTipsEntry = array();
$pageTitlesEntry = array();
$placeHoldersEntry = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsEntry["English"] = array();
	$fieldToolTipsEntry["English"] = array();
	$placeHoldersEntry["English"] = array();
	$pageTitlesEntry["English"] = array();
	$fieldLabelsEntry["English"]["ID"] = "Entry ID: ";
	$fieldToolTipsEntry["English"]["ID"] = "";
	$placeHoldersEntry["English"]["ID"] = "";
	$fieldLabelsEntry["English"]["NameOfOrg"] = "Name Of Organization";
	$fieldToolTipsEntry["English"]["NameOfOrg"] = "";
	$placeHoldersEntry["English"]["NameOfOrg"] = "";
	$fieldLabelsEntry["English"]["TypeOfOrg"] = "Type Of Organization";
	$fieldToolTipsEntry["English"]["TypeOfOrg"] = "";
	$placeHoldersEntry["English"]["TypeOfOrg"] = "";
	$fieldLabelsEntry["English"]["SchoolClass"] = "School Class";
	$fieldToolTipsEntry["English"]["SchoolClass"] = "";
	$placeHoldersEntry["English"]["SchoolClass"] = "";
	$fieldLabelsEntry["English"]["EntryType"] = "Entry Type";
	$fieldToolTipsEntry["English"]["EntryType"] = "";
	$placeHoldersEntry["English"]["EntryType"] = "";
	$fieldLabelsEntry["English"]["KeyPeople"] = "Person to Mention";
	$fieldToolTipsEntry["English"]["KeyPeople"] = "";
	$placeHoldersEntry["English"]["KeyPeople"] = "";
	$fieldLabelsEntry["English"]["EntrySize"] = "Entry Size";
	$fieldToolTipsEntry["English"]["EntrySize"] = "";
	$placeHoldersEntry["English"]["EntrySize"] = "";
	$fieldLabelsEntry["English"]["ContactName"] = "Parade Day Contact Name";
	$fieldToolTipsEntry["English"]["ContactName"] = "Director ON SITE with the group at the parade";
	$placeHoldersEntry["English"]["ContactName"] = "Director ON SITE with the group at the parade";
	$fieldLabelsEntry["English"]["ContactAddress"] = "Group Organizer Address";
	$fieldToolTipsEntry["English"]["ContactAddress"] = "";
	$placeHoldersEntry["English"]["ContactAddress"] = "";
	$fieldLabelsEntry["English"]["ContactEmail"] = "Group Organizer Email";
	$fieldToolTipsEntry["English"]["ContactEmail"] = "";
	$placeHoldersEntry["English"]["ContactEmail"] = "";
	$fieldLabelsEntry["English"]["ContactZip"] = "Group Contact Zip";
	$fieldToolTipsEntry["English"]["ContactZip"] = "";
	$placeHoldersEntry["English"]["ContactZip"] = "";
	$fieldLabelsEntry["English"]["ContactPhone"] = "Group Organizer Phone";
	$fieldToolTipsEntry["English"]["ContactPhone"] = "";
	$placeHoldersEntry["English"]["ContactPhone"] = "";
	$fieldLabelsEntry["English"]["ContactFax"] = "Group Contact Fax";
	$fieldToolTipsEntry["English"]["ContactFax"] = "";
	$placeHoldersEntry["English"]["ContactFax"] = "";
	$fieldLabelsEntry["English"]["ContactCell"] = "Parade Day Contact Cell";
	$fieldToolTipsEntry["English"]["ContactCell"] = "Direct contact with your ON SITE director as you prepare to march.";
	$placeHoldersEntry["English"]["ContactCell"] = "Direct contact with your ON SITE director as you prepare to march.";
	$fieldLabelsEntry["English"]["DescriptionOfEntry"] = "Brief Info for Announcers";
	$fieldToolTipsEntry["English"]["DescriptionOfEntry"] = "";
	$placeHoldersEntry["English"]["DescriptionOfEntry"] = "Please type a few sentences about the purpose of your group, for our video announcers to read as you pass by.";
	$fieldLabelsEntry["English"]["UID"] = "User ID";
	$fieldToolTipsEntry["English"]["UID"] = "";
	$placeHoldersEntry["English"]["UID"] = "";
	$fieldLabelsEntry["English"]["Approved"] = "Approved";
	$fieldToolTipsEntry["English"]["Approved"] = "";
	$placeHoldersEntry["English"]["Approved"] = "";
	$fieldLabelsEntry["English"]["OnSiteCheckIn"] = "On Site Check In";
	$fieldToolTipsEntry["English"]["OnSiteCheckIn"] = "";
	$placeHoldersEntry["English"]["OnSiteCheckIn"] = "";
	$fieldLabelsEntry["English"]["NumOfHorses"] = "Number Of Horses";
	$fieldToolTipsEntry["English"]["NumOfHorses"] = "";
	$placeHoldersEntry["English"]["NumOfHorses"] = "0";
	$fieldLabelsEntry["English"]["ParadeOrder"] = "Parade Order";
	$fieldToolTipsEntry["English"]["ParadeOrder"] = "";
	$placeHoldersEntry["English"]["ParadeOrder"] = "";
	$fieldLabelsEntry["English"]["GroupOrganizer"] = "Group Organizer Name";
	$fieldToolTipsEntry["English"]["GroupOrganizer"] = "";
	$placeHoldersEntry["English"]["GroupOrganizer"] = "";
	$fieldLabelsEntry["English"]["AgreetoRules"] = "Agree to the Hold Harmless Clause and Rules.";
	$fieldToolTipsEntry["English"]["AgreetoRules"] = "";
	$placeHoldersEntry["English"]["AgreetoRules"] = "";
	$fieldLabelsEntry["English"]["LiabilityWavierSigned"] = "Liability Wavier Signed";
	$fieldToolTipsEntry["English"]["LiabilityWavierSigned"] = "";
	$placeHoldersEntry["English"]["LiabilityWavierSigned"] = "";
	$pageTitlesEntry["English"]["add"] = "Entry Submission";
	$pageTitlesEntry["English"]["edit"] = "Edit Submission [{%ID}]";
	if (count($fieldToolTipsEntry["English"]))
		$tdataEntry[".isUseToolTips"] = true;
}


	$tdataEntry[".NCSearch"] = true;



$tdataEntry[".shortTableName"] = "Entry";
$tdataEntry[".nSecOptions"] = 1;

$tdataEntry[".mainTableOwnerID"] = "UID";
$tdataEntry[".entityType"] = 0;
$tdataEntry[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdataEntry[".strOriginalTableName"] = "Form";

		 



$tdataEntry[".showAddInPopup"] = false;

$tdataEntry[".showEditInPopup"] = false;

$tdataEntry[".showViewInPopup"] = false;

$tdataEntry[".listAjax"] = false;
//	temporary
//$tdataEntry[".listAjax"] = false;

	$tdataEntry[".audit"] = false;

	$tdataEntry[".locking"] = false;


$pages = $tdataEntry[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdataEntry[".edit"] = true;
	$tdataEntry[".afterEditAction"] = 1;
	$tdataEntry[".closePopupAfterEdit"] = 1;
	$tdataEntry[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdataEntry[".add"] = true;
$tdataEntry[".afterAddAction"] = 2;
$tdataEntry[".closePopupAfterAdd"] = 1;
$tdataEntry[".afterAddActionDetTable"] = "Detail tables not found!";
}

if( $pages[PAGE_LIST] ) {
	$tdataEntry[".list"] = true;
}



$tdataEntry[".strSortControlSettingsJSON"] = "";

$tdataEntry[".strClickActionJSON"] = "{\"fields\":{\"Approved\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactAddress\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactCell\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactEmail\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactFax\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactName\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactPhone\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactZip\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"DescriptionOfEntry\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"EntrySize\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"EntryType\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ID\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"KeyPeople\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"NameOfOrg\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"OnSiteCheckIn\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"SchoolClass\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"TypeOfOrg\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"UID\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}}},\"row\":{\"action\":\"open\",\"codeData\":{},\"gridData\":{\"action\":\"checkbox\",\"table\":null},\"openData\":{\"how\":\"goto\",\"page\":\"view\",\"table\":null,\"url\":\"\"}}}";



if( $pages[PAGE_VIEW] ) {
$tdataEntry[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdataEntry[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdataEntry[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdataEntry[".printFriendly"] = true;
}



$tdataEntry[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdataEntry[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdataEntry[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdataEntry[".isUseAjaxSuggest"] = true;

$tdataEntry[".rowHighlite"] = true;



						

$tdataEntry[".ajaxCodeSnippetAdded"] = false;

$tdataEntry[".buttonsAdded"] = false;

$tdataEntry[".addPageEvents"] = true;

// use timepicker for search panel
$tdataEntry[".isUseTimeForSearch"] = false;


$tdataEntry[".badgeColor"] = "00C2C5";


$tdataEntry[".allSearchFields"] = array();
$tdataEntry[".filterFields"] = array();
$tdataEntry[".requiredSearchFields"] = array();

$tdataEntry[".googleLikeFields"] = array();
$tdataEntry[".googleLikeFields"][] = "ID";
$tdataEntry[".googleLikeFields"][] = "NameOfOrg";
$tdataEntry[".googleLikeFields"][] = "TypeOfOrg";
$tdataEntry[".googleLikeFields"][] = "SchoolClass";
$tdataEntry[".googleLikeFields"][] = "EntryType";
$tdataEntry[".googleLikeFields"][] = "KeyPeople";
$tdataEntry[".googleLikeFields"][] = "EntrySize";
$tdataEntry[".googleLikeFields"][] = "ContactName";
$tdataEntry[".googleLikeFields"][] = "ContactAddress";
$tdataEntry[".googleLikeFields"][] = "ContactEmail";
$tdataEntry[".googleLikeFields"][] = "ContactZip";
$tdataEntry[".googleLikeFields"][] = "ContactPhone";
$tdataEntry[".googleLikeFields"][] = "ContactFax";
$tdataEntry[".googleLikeFields"][] = "ContactCell";
$tdataEntry[".googleLikeFields"][] = "DescriptionOfEntry";
$tdataEntry[".googleLikeFields"][] = "UID";
$tdataEntry[".googleLikeFields"][] = "Approved";
$tdataEntry[".googleLikeFields"][] = "OnSiteCheckIn";
$tdataEntry[".googleLikeFields"][] = "NumOfHorses";
$tdataEntry[".googleLikeFields"][] = "ParadeOrder";
$tdataEntry[".googleLikeFields"][] = "GroupOrganizer";
$tdataEntry[".googleLikeFields"][] = "AgreetoRules";
$tdataEntry[".googleLikeFields"][] = "LiabilityWavierSigned";



$tdataEntry[".tableType"] = "list";

$tdataEntry[".printerPageOrientation"] = 1;
$tdataEntry[".nPrinterPageScale"] = 100;

$tdataEntry[".nPrinterSplitRecords"] = 1;

$tdataEntry[".geocodingEnabled"] = false;










$tdataEntry[".pageSize"] = 20;

$tdataEntry[".warnLeavingPages"] = true;

$tdataEntry[".hideEmptyFieldsOnView"] = true;


$tstrOrderBy = "";
$tdataEntry[".strOrderBy"] = $tstrOrderBy;

$tdataEntry[".orderindexes"] = array();


$tdataEntry[".sqlHead"] = "SELECT Form.ID,  Form.NameOfOrg,  Form.TypeOfOrg,  Form.SchoolClass,  Form.EntryType,  Form.KeyPeople,  Form.EntrySize,  Form.ContactName,  Form.ContactAddress,  Form.ContactEmail,  Form.ContactZip,  Form.ContactPhone,  Form.ContactFax,  Form.ContactCell,  Form.DescriptionOfEntry,  Form.`UID`,  Form.Approved,  Concat(QRCodeURL.ProgramURL, Form.ID) AS OnSiteCheckIn,  Form.NumOfHorses,  Form.ParadeOrder,  Form.GroupOrganizer,  Form.AgreetoRules,  Form.LiabilityWavierSigned";
$tdataEntry[".sqlFrom"] = "FROM Form  , QRCodeURL";
$tdataEntry[".sqlWhereExpr"] = "(QRCodeURL.PageName = 'Entry Submit')";
$tdataEntry[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataEntry[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataEntry[".arrGroupsPerPage"] = $arrGPP;

$tdataEntry[".highlightSearchResults"] = true;

$tableKeysEntry = array();
$tableKeysEntry[] = "ID";
$tdataEntry[".Keys"] = $tableKeysEntry;


$tdataEntry[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["ID"] = $fdata;
		$tdataEntry[".searchableFields"][] = "ID";
//	NameOfOrg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "NameOfOrg";
	$fdata["GoodName"] = "NameOfOrg";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","NameOfOrg");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "NameOfOrg";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.NameOfOrg";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["NameOfOrg"] = $fdata;
		$tdataEntry[".searchableFields"][] = "NameOfOrg";
//	TypeOfOrg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "TypeOfOrg";
	$fdata["GoodName"] = "TypeOfOrg";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","TypeOfOrg");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "TypeOfOrg";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.TypeOfOrg";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "OrgTypes";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "ID";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "OrgTypeName";

	

	
	$edata["LookupOrderBy"] = "ID";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["TypeOfOrg"] = $fdata;
		$tdataEntry[".searchableFields"][] = "TypeOfOrg";
//	SchoolClass
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "SchoolClass";
	$fdata["GoodName"] = "SchoolClass";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","SchoolClass");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "SchoolClass";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.SchoolClass";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "SchoolType";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "ID";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "TypeName";

	

	
	$edata["LookupOrderBy"] = "ID";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["SchoolClass"] = $fdata;
		$tdataEntry[".searchableFields"][] = "SchoolClass";
//	EntryType
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "EntryType";
	$fdata["GoodName"] = "EntryType";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","EntryType");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "EntryType";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.EntryType";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "EntryType";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "ID";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "EntryTypeName";

	

	
	$edata["LookupOrderBy"] = "ID";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["EntryType"] = $fdata;
		$tdataEntry[".searchableFields"][] = "EntryType";
//	KeyPeople
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "KeyPeople";
	$fdata["GoodName"] = "KeyPeople";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","KeyPeople");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "KeyPeople";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.KeyPeople";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
	
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["KeyPeople"] = $fdata;
		$tdataEntry[".searchableFields"][] = "KeyPeople";
//	EntrySize
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "EntrySize";
	$fdata["GoodName"] = "EntrySize";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","EntrySize");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "EntrySize";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.EntrySize";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["EntrySize"] = $fdata;
		$tdataEntry[".searchableFields"][] = "EntrySize";
//	ContactName
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "ContactName";
	$fdata["GoodName"] = "ContactName";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","ContactName");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactName";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ContactName";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=200";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["ContactName"] = $fdata;
		$tdataEntry[".searchableFields"][] = "ContactName";
//	ContactAddress
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "ContactAddress";
	$fdata["GoodName"] = "ContactAddress";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","ContactAddress");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactAddress";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ContactAddress";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["ContactAddress"] = $fdata;
		$tdataEntry[".searchableFields"][] = "ContactAddress";
//	ContactEmail
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "ContactEmail";
	$fdata["GoodName"] = "ContactEmail";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","ContactEmail");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactEmail";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ContactEmail";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Email");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["ContactEmail"] = $fdata;
		$tdataEntry[".searchableFields"][] = "ContactEmail";
//	ContactZip
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "ContactZip";
	$fdata["GoodName"] = "ContactZip";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","ContactZip");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactZip";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ContactZip";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("US ZIP Code");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["ContactZip"] = $fdata;
		$tdataEntry[".searchableFields"][] = "ContactZip";
//	ContactPhone
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "ContactPhone";
	$fdata["GoodName"] = "ContactPhone";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","ContactPhone");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactPhone";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ContactPhone";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


		$edata["strEditMask"] = "999-999-9999";

	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("US Phone Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["ContactPhone"] = $fdata;
		$tdataEntry[".searchableFields"][] = "ContactPhone";
//	ContactFax
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "ContactFax";
	$fdata["GoodName"] = "ContactFax";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","ContactFax");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactFax";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ContactFax";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


		$edata["strEditMask"] = "999-999-9999";

	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("US Phone Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["ContactFax"] = $fdata;
		$tdataEntry[".searchableFields"][] = "ContactFax";
//	ContactCell
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "ContactCell";
	$fdata["GoodName"] = "ContactCell";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","ContactCell");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ContactCell";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ContactCell";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


		$edata["strEditMask"] = "999-999-9999";

	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("US Phone Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["ContactCell"] = $fdata;
		$tdataEntry[".searchableFields"][] = "ContactCell";
//	DescriptionOfEntry
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "DescriptionOfEntry";
	$fdata["GoodName"] = "DescriptionOfEntry";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","DescriptionOfEntry");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "DescriptionOfEntry";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.DescriptionOfEntry";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "HTML");

	
	
	
	
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;

	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 600;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["DescriptionOfEntry"] = $fdata;
		$tdataEntry[".searchableFields"][] = "DescriptionOfEntry";
//	UID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "UID";
	$fdata["GoodName"] = "UID";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","UID");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "UID";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.`UID`";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["UID"] = $fdata;
		$tdataEntry[".searchableFields"][] = "UID";
//	Approved
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "Approved";
	$fdata["GoodName"] = "Approved";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","Approved");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "Approved";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.Approved";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "YesNo";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "OptionValue";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "OptionName";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["Approved"] = $fdata;
		$tdataEntry[".searchableFields"][] = "Approved";
//	OnSiteCheckIn
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "OnSiteCheckIn";
	$fdata["GoodName"] = "OnSiteCheckIn";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("Form","OnSiteCheckIn");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "OnSiteCheckIn";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Concat(QRCodeURL.ProgramURL, Form.ID)";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "QRCode");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["OnSiteCheckIn"] = $fdata;
		$tdataEntry[".searchableFields"][] = "OnSiteCheckIn";
//	NumOfHorses
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "NumOfHorses";
	$fdata["GoodName"] = "NumOfHorses";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","NumOfHorses");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "NumOfHorses";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.NumOfHorses";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["NumOfHorses"] = $fdata;
		$tdataEntry[".searchableFields"][] = "NumOfHorses";
//	ParadeOrder
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "ParadeOrder";
	$fdata["GoodName"] = "ParadeOrder";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","ParadeOrder");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ParadeOrder";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.ParadeOrder";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["ParadeOrder"] = $fdata;
		$tdataEntry[".searchableFields"][] = "ParadeOrder";
//	GroupOrganizer
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "GroupOrganizer";
	$fdata["GoodName"] = "GroupOrganizer";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","GroupOrganizer");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "GroupOrganizer";

		$fdata["sourceSingle"] = "GroupOrganizer";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.GroupOrganizer";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["GroupOrganizer"] = $fdata;
		$tdataEntry[".searchableFields"][] = "GroupOrganizer";
//	AgreetoRules
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 22;
	$fdata["strName"] = "AgreetoRules";
	$fdata["GoodName"] = "AgreetoRules";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","AgreetoRules");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "AgreetoRules";

		$fdata["sourceSingle"] = "AgreetoRules";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.AgreetoRules";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["list"] = $vdata;
	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["print"] = $vdata;
	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
		
	
	
	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["export"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "YesNo";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
		
	$edata["LinkField"] = "OptionValue";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "AlternateName";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
	$edata = array("EditFormat" => "Checkbox");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["add"] = $edata;
	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "YesNo";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
		
	$edata["LinkField"] = "OptionValue";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "AlternateName";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = true;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["AgreetoRules"] = $fdata;
		$tdataEntry[".searchableFields"][] = "AgreetoRules";
//	LiabilityWavierSigned
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 23;
	$fdata["strName"] = "LiabilityWavierSigned";
	$fdata["GoodName"] = "LiabilityWavierSigned";
	$fdata["ownerTable"] = "Form";
	$fdata["Label"] = GetFieldLabel("Form","LiabilityWavierSigned");
	$fdata["FieldType"] = 3;


	
	
										

		$fdata["strField"] = "LiabilityWavierSigned";

		$fdata["sourceSingle"] = "LiabilityWavierSigned";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form.LiabilityWavierSigned";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "YesNo";
			$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "OptionValue";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "AlternateName";

	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdataEntry["LiabilityWavierSigned"] = $fdata;
		$tdataEntry[".searchableFields"][] = "LiabilityWavierSigned";


$tables_data["Form"]=&$tdataEntry;
$field_labels["Form"] = &$fieldLabelsEntry;
$fieldToolTips["Form"] = &$fieldToolTipsEntry;
$placeHolders["Form"] = &$placeHoldersEntry;
$page_titles["Form"] = &$pageTitlesEntry;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["Form"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["Form"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_Entry()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "Form.ID,  Form.NameOfOrg,  Form.TypeOfOrg,  Form.SchoolClass,  Form.EntryType,  Form.KeyPeople,  Form.EntrySize,  Form.ContactName,  Form.ContactAddress,  Form.ContactEmail,  Form.ContactZip,  Form.ContactPhone,  Form.ContactFax,  Form.ContactCell,  Form.DescriptionOfEntry,  Form.`UID`,  Form.Approved,  Concat(QRCodeURL.ProgramURL, Form.ID) AS OnSiteCheckIn,  Form.NumOfHorses,  Form.ParadeOrder,  Form.GroupOrganizer,  Form.AgreetoRules,  Form.LiabilityWavierSigned";
$proto0["m_strFrom"] = "FROM Form  , QRCodeURL";
$proto0["m_strWhere"] = "(QRCodeURL.PageName = 'Entry Submit')";
$proto0["m_strOrderBy"] = "";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "QRCodeURL.PageName = 'Entry Submit'";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "PageName",
	"m_strTable" => "QRCodeURL",
	"m_srcTableName" => "Form"
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "= 'Entry Submit'";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto6["m_sql"] = "Form.ID";
$proto6["m_srcTableName"] = "Form";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "NameOfOrg",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto8["m_sql"] = "Form.NameOfOrg";
$proto8["m_srcTableName"] = "Form";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "TypeOfOrg",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto10["m_sql"] = "Form.TypeOfOrg";
$proto10["m_srcTableName"] = "Form";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "SchoolClass",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto12["m_sql"] = "Form.SchoolClass";
$proto12["m_srcTableName"] = "Form";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "EntryType",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto14["m_sql"] = "Form.EntryType";
$proto14["m_srcTableName"] = "Form";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "KeyPeople",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto16["m_sql"] = "Form.KeyPeople";
$proto16["m_srcTableName"] = "Form";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "EntrySize",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto18["m_sql"] = "Form.EntrySize";
$proto18["m_srcTableName"] = "Form";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactName",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto20["m_sql"] = "Form.ContactName";
$proto20["m_srcTableName"] = "Form";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactAddress",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto22["m_sql"] = "Form.ContactAddress";
$proto22["m_srcTableName"] = "Form";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactEmail",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto24["m_sql"] = "Form.ContactEmail";
$proto24["m_srcTableName"] = "Form";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactZip",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto26["m_sql"] = "Form.ContactZip";
$proto26["m_srcTableName"] = "Form";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactPhone",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto28["m_sql"] = "Form.ContactPhone";
$proto28["m_srcTableName"] = "Form";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactFax",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto30["m_sql"] = "Form.ContactFax";
$proto30["m_srcTableName"] = "Form";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "ContactCell",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto32["m_sql"] = "Form.ContactCell";
$proto32["m_srcTableName"] = "Form";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "DescriptionOfEntry",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto34["m_sql"] = "Form.DescriptionOfEntry";
$proto34["m_srcTableName"] = "Form";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "UID",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto36["m_sql"] = "Form.`UID`";
$proto36["m_srcTableName"] = "Form";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "Approved",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto38["m_sql"] = "Form.Approved";
$proto38["m_srcTableName"] = "Form";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$proto41=array();
$proto41["m_functiontype"] = "SQLF_CUSTOM";
$proto41["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "QRCodeURL.ProgramURL"
));

$proto41["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "Form.ID"
));

$proto41["m_arguments"][]=$obj;
$proto41["m_strFunctionName"] = "Concat";
$obj = new SQLFunctionCall($proto41);

$proto40["m_sql"] = "Concat(QRCodeURL.ProgramURL, Form.ID)";
$proto40["m_srcTableName"] = "Form";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "OnSiteCheckIn";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "NumOfHorses",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto44["m_sql"] = "Form.NumOfHorses";
$proto44["m_srcTableName"] = "Form";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$obj = new SQLField(array(
	"m_strName" => "ParadeOrder",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto46["m_sql"] = "Form.ParadeOrder";
$proto46["m_srcTableName"] = "Form";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
						$proto48=array();
			$obj = new SQLField(array(
	"m_strName" => "GroupOrganizer",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto48["m_sql"] = "Form.GroupOrganizer";
$proto48["m_srcTableName"] = "Form";
$proto48["m_expr"]=$obj;
$proto48["m_alias"] = "";
$obj = new SQLFieldListItem($proto48);

$proto0["m_fieldlist"][]=$obj;
						$proto50=array();
			$obj = new SQLField(array(
	"m_strName" => "AgreetoRules",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto50["m_sql"] = "Form.AgreetoRules";
$proto50["m_srcTableName"] = "Form";
$proto50["m_expr"]=$obj;
$proto50["m_alias"] = "";
$obj = new SQLFieldListItem($proto50);

$proto0["m_fieldlist"][]=$obj;
						$proto52=array();
			$obj = new SQLField(array(
	"m_strName" => "LiabilityWavierSigned",
	"m_strTable" => "Form",
	"m_srcTableName" => "Form"
));

$proto52["m_sql"] = "Form.LiabilityWavierSigned";
$proto52["m_srcTableName"] = "Form";
$proto52["m_expr"]=$obj;
$proto52["m_alias"] = "";
$obj = new SQLFieldListItem($proto52);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto54=array();
$proto54["m_link"] = "SQLL_MAIN";
			$proto55=array();
$proto55["m_strName"] = "Form";
$proto55["m_srcTableName"] = "Form";
$proto55["m_columns"] = array();
$proto55["m_columns"][] = "ID";
$proto55["m_columns"][] = "NameOfOrg";
$proto55["m_columns"][] = "TypeOfOrg";
$proto55["m_columns"][] = "SchoolClass";
$proto55["m_columns"][] = "EntryType";
$proto55["m_columns"][] = "KeyPeople";
$proto55["m_columns"][] = "EntrySize";
$proto55["m_columns"][] = "ContactName";
$proto55["m_columns"][] = "ContactAddress";
$proto55["m_columns"][] = "ContactEmail";
$proto55["m_columns"][] = "ContactZip";
$proto55["m_columns"][] = "ContactPhone";
$proto55["m_columns"][] = "ContactFax";
$proto55["m_columns"][] = "ContactCell";
$proto55["m_columns"][] = "DescriptionOfEntry";
$proto55["m_columns"][] = "UID";
$proto55["m_columns"][] = "NumOfHorses";
$proto55["m_columns"][] = "Approved";
$proto55["m_columns"][] = "LowerThirds";
$proto55["m_columns"][] = "ParadeOrder";
$proto55["m_columns"][] = "GroupOrganizer";
$proto55["m_columns"][] = "AgreetoRules";
$proto55["m_columns"][] = "LiabilityWavierSigned";
$obj = new SQLTable($proto55);

$proto54["m_table"] = $obj;
$proto54["m_sql"] = "Form";
$proto54["m_alias"] = "";
$proto54["m_srcTableName"] = "Form";
$proto56=array();
$proto56["m_sql"] = "";
$proto56["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto56["m_column"]=$obj;
$proto56["m_contained"] = array();
$proto56["m_strCase"] = "";
$proto56["m_havingmode"] = false;
$proto56["m_inBrackets"] = false;
$proto56["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto56);

$proto54["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto54);

$proto0["m_fromlist"][]=$obj;
												$proto58=array();
$proto58["m_link"] = "SQLL_CROSSJOIN";
			$proto59=array();
$proto59["m_strName"] = "QRCodeURL";
$proto59["m_srcTableName"] = "Form";
$proto59["m_columns"] = array();
$proto59["m_columns"][] = "ID";
$proto59["m_columns"][] = "ProgramURL";
$proto59["m_columns"][] = "PageName";
$obj = new SQLTable($proto59);

$proto58["m_table"] = $obj;
$proto58["m_sql"] = "QRCodeURL";
$proto58["m_alias"] = "";
$proto58["m_srcTableName"] = "Form";
$proto60=array();
$proto60["m_sql"] = "";
$proto60["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto60["m_column"]=$obj;
$proto60["m_contained"] = array();
$proto60["m_strCase"] = "";
$proto60["m_havingmode"] = false;
$proto60["m_inBrackets"] = false;
$proto60["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto60);

$proto58["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto58);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="Form";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_Entry = createSqlQuery_Entry();


	
					
;

																							

$tdataEntry[".sqlquery"] = $queryData_Entry;



include_once(getabspath("include/Entry_events.php"));
$tableEvents["Form"] = new eventclass_Entry;
$tdataEntry[".hasEvents"] = true;

?>