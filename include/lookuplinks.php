<?php

/**
* getLookupMainTableSettings - tests whether the lookup link exists between the tables
*
*  returns array with ProjectSettings class for main table if the link exists in project settings.
*  returns NULL otherwise
*/
function getLookupMainTableSettings($lookupTable, $mainTableShortName, $mainField, $desiredPage = "")
{
	global $lookupTableLinks;
	if(!isset($lookupTableLinks[$lookupTable]))
		return null;
	if(!isset($lookupTableLinks[$lookupTable][$mainTableShortName.".".$mainField]))
		return null;
	$arr = &$lookupTableLinks[$lookupTable][$mainTableShortName.".".$mainField];
	$effectivePage = $desiredPage;
	if(!isset($arr[$effectivePage]))
	{
		$effectivePage = PAGE_EDIT;
		if(!isset($arr[$effectivePage]))
		{
			if($desiredPage == "" && 0 < count($arr))
			{
				$effectivePage = $arr[0];
			}
			else
				return null;
		}
	}
	return new ProjectSettings($arr[$effectivePage]["table"], $effectivePage);
}

/** 
* $lookupTableLinks array stores all lookup links between tables in the project
*/
function InitLookupLinks()
{
	global $lookupTableLinks;

	$lookupTableLinks = array();

		if( !isset( $lookupTableLinks["OrgTypes"] ) ) {
			$lookupTableLinks["OrgTypes"] = array();
		}
		if( !isset( $lookupTableLinks["OrgTypes"]["Entry.TypeOfOrg"] )) {
			$lookupTableLinks["OrgTypes"]["Entry.TypeOfOrg"] = array();
		}
		$lookupTableLinks["OrgTypes"]["Entry.TypeOfOrg"]["edit"] = array("table" => "Form", "field" => "TypeOfOrg", "page" => "edit");
		if( !isset( $lookupTableLinks["SchoolType"] ) ) {
			$lookupTableLinks["SchoolType"] = array();
		}
		if( !isset( $lookupTableLinks["SchoolType"]["Entry.SchoolClass"] )) {
			$lookupTableLinks["SchoolType"]["Entry.SchoolClass"] = array();
		}
		$lookupTableLinks["SchoolType"]["Entry.SchoolClass"]["edit"] = array("table" => "Form", "field" => "SchoolClass", "page" => "edit");
		if( !isset( $lookupTableLinks["EntryType"] ) ) {
			$lookupTableLinks["EntryType"] = array();
		}
		if( !isset( $lookupTableLinks["EntryType"]["Entry.EntryType"] )) {
			$lookupTableLinks["EntryType"]["Entry.EntryType"] = array();
		}
		$lookupTableLinks["EntryType"]["Entry.EntryType"]["edit"] = array("table" => "Form", "field" => "EntryType", "page" => "edit");
		if( !isset( $lookupTableLinks["YesNo"] ) ) {
			$lookupTableLinks["YesNo"] = array();
		}
		if( !isset( $lookupTableLinks["YesNo"]["Entry.Approved"] )) {
			$lookupTableLinks["YesNo"]["Entry.Approved"] = array();
		}
		$lookupTableLinks["YesNo"]["Entry.Approved"]["edit"] = array("table" => "Form", "field" => "Approved", "page" => "edit");
		if( !isset( $lookupTableLinks["YesNo"] ) ) {
			$lookupTableLinks["YesNo"] = array();
		}
		if( !isset( $lookupTableLinks["YesNo"]["Entry.AgreetoRules"] )) {
			$lookupTableLinks["YesNo"]["Entry.AgreetoRules"] = array();
		}
		$lookupTableLinks["YesNo"]["Entry.AgreetoRules"]["edit"] = array("table" => "Form", "field" => "AgreetoRules", "page" => "edit");
		if( !isset( $lookupTableLinks["YesNo"] ) ) {
			$lookupTableLinks["YesNo"] = array();
		}
		if( !isset( $lookupTableLinks["YesNo"]["Entry.AgreetoRules"] )) {
			$lookupTableLinks["YesNo"]["Entry.AgreetoRules"] = array();
		}
		$lookupTableLinks["YesNo"]["Entry.AgreetoRules"]["search"] = array("table" => "Form", "field" => "AgreetoRules", "page" => "search");
		if( !isset( $lookupTableLinks["YesNo"] ) ) {
			$lookupTableLinks["YesNo"] = array();
		}
		if( !isset( $lookupTableLinks["YesNo"]["Entry.LiabilityWavierSigned"] )) {
			$lookupTableLinks["YesNo"]["Entry.LiabilityWavierSigned"] = array();
		}
		$lookupTableLinks["YesNo"]["Entry.LiabilityWavierSigned"]["edit"] = array("table" => "Form", "field" => "LiabilityWavierSigned", "page" => "edit");
		if( !isset( $lookupTableLinks["OrgTypes"] ) ) {
			$lookupTableLinks["OrgTypes"] = array();
		}
		if( !isset( $lookupTableLinks["OrgTypes"]["adminapprovedentry.TypeOfOrg"] )) {
			$lookupTableLinks["OrgTypes"]["adminapprovedentry.TypeOfOrg"] = array();
		}
		$lookupTableLinks["OrgTypes"]["adminapprovedentry.TypeOfOrg"]["edit"] = array("table" => "AdminApprovedEntry", "field" => "TypeOfOrg", "page" => "edit");
		if( !isset( $lookupTableLinks["SchoolType"] ) ) {
			$lookupTableLinks["SchoolType"] = array();
		}
		if( !isset( $lookupTableLinks["SchoolType"]["adminapprovedentry.SchoolClass"] )) {
			$lookupTableLinks["SchoolType"]["adminapprovedentry.SchoolClass"] = array();
		}
		$lookupTableLinks["SchoolType"]["adminapprovedentry.SchoolClass"]["edit"] = array("table" => "AdminApprovedEntry", "field" => "SchoolClass", "page" => "edit");
		if( !isset( $lookupTableLinks["EntryType"] ) ) {
			$lookupTableLinks["EntryType"] = array();
		}
		if( !isset( $lookupTableLinks["EntryType"]["adminapprovedentry.EntryType"] )) {
			$lookupTableLinks["EntryType"]["adminapprovedentry.EntryType"] = array();
		}
		$lookupTableLinks["EntryType"]["adminapprovedentry.EntryType"]["edit"] = array("table" => "AdminApprovedEntry", "field" => "EntryType", "page" => "edit");
		if( !isset( $lookupTableLinks["YesNo"] ) ) {
			$lookupTableLinks["YesNo"] = array();
		}
		if( !isset( $lookupTableLinks["YesNo"]["adminapprovedentry.Approved"] )) {
			$lookupTableLinks["YesNo"]["adminapprovedentry.Approved"] = array();
		}
		$lookupTableLinks["YesNo"]["adminapprovedentry.Approved"]["edit"] = array("table" => "AdminApprovedEntry", "field" => "Approved", "page" => "edit");
		if( !isset( $lookupTableLinks["YesNo"] ) ) {
			$lookupTableLinks["YesNo"] = array();
		}
		if( !isset( $lookupTableLinks["YesNo"]["adminapprovedentry.AgreetoRules"] )) {
			$lookupTableLinks["YesNo"]["adminapprovedentry.AgreetoRules"] = array();
		}
		$lookupTableLinks["YesNo"]["adminapprovedentry.AgreetoRules"]["edit"] = array("table" => "AdminApprovedEntry", "field" => "AgreetoRules", "page" => "edit");
		if( !isset( $lookupTableLinks["YesNo"] ) ) {
			$lookupTableLinks["YesNo"] = array();
		}
		if( !isset( $lookupTableLinks["YesNo"]["adminapprovedentry.LiabilityWavierSigned"] )) {
			$lookupTableLinks["YesNo"]["adminapprovedentry.LiabilityWavierSigned"] = array();
		}
		$lookupTableLinks["YesNo"]["adminapprovedentry.LiabilityWavierSigned"]["edit"] = array("table" => "AdminApprovedEntry", "field" => "LiabilityWavierSigned", "page" => "edit");
		if( !isset( $lookupTableLinks["OrgTypes"] ) ) {
			$lookupTableLinks["OrgTypes"] = array();
		}
		if( !isset( $lookupTableLinks["OrgTypes"]["adminunapprovedentry.TypeOfOrg"] )) {
			$lookupTableLinks["OrgTypes"]["adminunapprovedentry.TypeOfOrg"] = array();
		}
		$lookupTableLinks["OrgTypes"]["adminunapprovedentry.TypeOfOrg"]["edit"] = array("table" => "AdminUnApprovedEntry", "field" => "TypeOfOrg", "page" => "edit");
		if( !isset( $lookupTableLinks["SchoolType"] ) ) {
			$lookupTableLinks["SchoolType"] = array();
		}
		if( !isset( $lookupTableLinks["SchoolType"]["adminunapprovedentry.SchoolClass"] )) {
			$lookupTableLinks["SchoolType"]["adminunapprovedentry.SchoolClass"] = array();
		}
		$lookupTableLinks["SchoolType"]["adminunapprovedentry.SchoolClass"]["edit"] = array("table" => "AdminUnApprovedEntry", "field" => "SchoolClass", "page" => "edit");
		if( !isset( $lookupTableLinks["EntryType"] ) ) {
			$lookupTableLinks["EntryType"] = array();
		}
		if( !isset( $lookupTableLinks["EntryType"]["adminunapprovedentry.EntryType"] )) {
			$lookupTableLinks["EntryType"]["adminunapprovedentry.EntryType"] = array();
		}
		$lookupTableLinks["EntryType"]["adminunapprovedentry.EntryType"]["edit"] = array("table" => "AdminUnApprovedEntry", "field" => "EntryType", "page" => "edit");
		if( !isset( $lookupTableLinks["YesNo"] ) ) {
			$lookupTableLinks["YesNo"] = array();
		}
		if( !isset( $lookupTableLinks["YesNo"]["adminunapprovedentry.Approved"] )) {
			$lookupTableLinks["YesNo"]["adminunapprovedentry.Approved"] = array();
		}
		$lookupTableLinks["YesNo"]["adminunapprovedentry.Approved"]["edit"] = array("table" => "AdminUnApprovedEntry", "field" => "Approved", "page" => "edit");
		if( !isset( $lookupTableLinks["YesNo"] ) ) {
			$lookupTableLinks["YesNo"] = array();
		}
		if( !isset( $lookupTableLinks["YesNo"]["adminunapprovedentry.AgreetoRules"] )) {
			$lookupTableLinks["YesNo"]["adminunapprovedentry.AgreetoRules"] = array();
		}
		$lookupTableLinks["YesNo"]["adminunapprovedentry.AgreetoRules"]["edit"] = array("table" => "AdminUnApprovedEntry", "field" => "AgreetoRules", "page" => "edit");
		if( !isset( $lookupTableLinks["YesNo"] ) ) {
			$lookupTableLinks["YesNo"] = array();
		}
		if( !isset( $lookupTableLinks["YesNo"]["adminunapprovedentry.LiabilityWavierSigned"] )) {
			$lookupTableLinks["YesNo"]["adminunapprovedentry.LiabilityWavierSigned"] = array();
		}
		$lookupTableLinks["YesNo"]["adminunapprovedentry.LiabilityWavierSigned"]["edit"] = array("table" => "AdminUnApprovedEntry", "field" => "LiabilityWavierSigned", "page" => "edit");
		if( !isset( $lookupTableLinks["OrgTypes"] ) ) {
			$lookupTableLinks["OrgTypes"] = array();
		}
		if( !isset( $lookupTableLinks["OrgTypes"]["adminallentry.TypeOfOrg"] )) {
			$lookupTableLinks["OrgTypes"]["adminallentry.TypeOfOrg"] = array();
		}
		$lookupTableLinks["OrgTypes"]["adminallentry.TypeOfOrg"]["edit"] = array("table" => "AdminAllEntry", "field" => "TypeOfOrg", "page" => "edit");
		if( !isset( $lookupTableLinks["SchoolType"] ) ) {
			$lookupTableLinks["SchoolType"] = array();
		}
		if( !isset( $lookupTableLinks["SchoolType"]["adminallentry.SchoolClass"] )) {
			$lookupTableLinks["SchoolType"]["adminallentry.SchoolClass"] = array();
		}
		$lookupTableLinks["SchoolType"]["adminallentry.SchoolClass"]["edit"] = array("table" => "AdminAllEntry", "field" => "SchoolClass", "page" => "edit");
		if( !isset( $lookupTableLinks["EntryType"] ) ) {
			$lookupTableLinks["EntryType"] = array();
		}
		if( !isset( $lookupTableLinks["EntryType"]["adminallentry.EntryType"] )) {
			$lookupTableLinks["EntryType"]["adminallentry.EntryType"] = array();
		}
		$lookupTableLinks["EntryType"]["adminallentry.EntryType"]["edit"] = array("table" => "AdminAllEntry", "field" => "EntryType", "page" => "edit");
		if( !isset( $lookupTableLinks["YesNo"] ) ) {
			$lookupTableLinks["YesNo"] = array();
		}
		if( !isset( $lookupTableLinks["YesNo"]["adminallentry.Approved"] )) {
			$lookupTableLinks["YesNo"]["adminallentry.Approved"] = array();
		}
		$lookupTableLinks["YesNo"]["adminallentry.Approved"]["edit"] = array("table" => "AdminAllEntry", "field" => "Approved", "page" => "edit");
		if( !isset( $lookupTableLinks["YesNo"] ) ) {
			$lookupTableLinks["YesNo"] = array();
		}
		if( !isset( $lookupTableLinks["YesNo"]["adminallentry.AgreetoRules"] )) {
			$lookupTableLinks["YesNo"]["adminallentry.AgreetoRules"] = array();
		}
		$lookupTableLinks["YesNo"]["adminallentry.AgreetoRules"]["edit"] = array("table" => "AdminAllEntry", "field" => "AgreetoRules", "page" => "edit");
		if( !isset( $lookupTableLinks["YesNo"] ) ) {
			$lookupTableLinks["YesNo"] = array();
		}
		if( !isset( $lookupTableLinks["YesNo"]["adminallentry.LiabilityWavierSigned"] )) {
			$lookupTableLinks["YesNo"]["adminallentry.LiabilityWavierSigned"] = array();
		}
		$lookupTableLinks["YesNo"]["adminallentry.LiabilityWavierSigned"]["edit"] = array("table" => "AdminAllEntry", "field" => "LiabilityWavierSigned", "page" => "edit");
		if( !isset( $lookupTableLinks["Form"] ) ) {
			$lookupTableLinks["Form"] = array();
		}
		if( !isset( $lookupTableLinks["Form"]["announcer.FormID"] )) {
			$lookupTableLinks["Form"]["announcer.FormID"] = array();
		}
		$lookupTableLinks["Form"]["announcer.FormID"]["add"] = array("table" => "announcer", "field" => "FormID", "page" => "add");
		if( !isset( $lookupTableLinks["Form"] ) ) {
			$lookupTableLinks["Form"] = array();
		}
		if( !isset( $lookupTableLinks["Form"]["announcer.FormID"] )) {
			$lookupTableLinks["Form"]["announcer.FormID"] = array();
		}
		$lookupTableLinks["Form"]["announcer.FormID"]["search"] = array("table" => "announcer", "field" => "FormID", "page" => "search");
		if( !isset( $lookupTableLinks["OrgTypes"] ) ) {
			$lookupTableLinks["OrgTypes"] = array();
		}
		if( !isset( $lookupTableLinks["OrgTypes"]["form2022.TypeOfOrg"] )) {
			$lookupTableLinks["OrgTypes"]["form2022.TypeOfOrg"] = array();
		}
		$lookupTableLinks["OrgTypes"]["form2022.TypeOfOrg"]["edit"] = array("table" => "Form2022", "field" => "TypeOfOrg", "page" => "edit");
		if( !isset( $lookupTableLinks["SchoolType"] ) ) {
			$lookupTableLinks["SchoolType"] = array();
		}
		if( !isset( $lookupTableLinks["SchoolType"]["form2022.SchoolClass"] )) {
			$lookupTableLinks["SchoolType"]["form2022.SchoolClass"] = array();
		}
		$lookupTableLinks["SchoolType"]["form2022.SchoolClass"]["edit"] = array("table" => "Form2022", "field" => "SchoolClass", "page" => "edit");
		if( !isset( $lookupTableLinks["EntryType"] ) ) {
			$lookupTableLinks["EntryType"] = array();
		}
		if( !isset( $lookupTableLinks["EntryType"]["form2022.EntryType"] )) {
			$lookupTableLinks["EntryType"]["form2022.EntryType"] = array();
		}
		$lookupTableLinks["EntryType"]["form2022.EntryType"]["edit"] = array("table" => "Form2022", "field" => "EntryType", "page" => "edit");
		if( !isset( $lookupTableLinks["YesNo"] ) ) {
			$lookupTableLinks["YesNo"] = array();
		}
		if( !isset( $lookupTableLinks["YesNo"]["form2022.Approved"] )) {
			$lookupTableLinks["YesNo"]["form2022.Approved"] = array();
		}
		$lookupTableLinks["YesNo"]["form2022.Approved"]["edit"] = array("table" => "Form2022", "field" => "Approved", "page" => "edit");
		if( !isset( $lookupTableLinks["YesNo"] ) ) {
			$lookupTableLinks["YesNo"] = array();
		}
		if( !isset( $lookupTableLinks["YesNo"]["form2022.AgreetoRules"] )) {
			$lookupTableLinks["YesNo"]["form2022.AgreetoRules"] = array();
		}
		$lookupTableLinks["YesNo"]["form2022.AgreetoRules"]["edit"] = array("table" => "Form2022", "field" => "AgreetoRules", "page" => "edit");
		if( !isset( $lookupTableLinks["YesNo"] ) ) {
			$lookupTableLinks["YesNo"] = array();
		}
		if( !isset( $lookupTableLinks["YesNo"]["form2022.LiabilityWavierSigned"] )) {
			$lookupTableLinks["YesNo"]["form2022.LiabilityWavierSigned"] = array();
		}
		$lookupTableLinks["YesNo"]["form2022.LiabilityWavierSigned"]["edit"] = array("table" => "Form2022", "field" => "LiabilityWavierSigned", "page" => "edit");
		if( !isset( $lookupTableLinks["Form2022"] ) ) {
			$lookupTableLinks["Form2022"] = array();
		}
		if( !isset( $lookupTableLinks["Form2022"]["announcer2022.FormID"] )) {
			$lookupTableLinks["Form2022"]["announcer2022.FormID"] = array();
		}
		$lookupTableLinks["Form2022"]["announcer2022.FormID"]["edit"] = array("table" => "announcer2022", "field" => "FormID", "page" => "edit");
		if( !isset( $lookupTableLinks["Form2022"] ) ) {
			$lookupTableLinks["Form2022"] = array();
		}
		if( !isset( $lookupTableLinks["Form2022"]["announcer2022.FormID"] )) {
			$lookupTableLinks["Form2022"]["announcer2022.FormID"] = array();
		}
		$lookupTableLinks["Form2022"]["announcer2022.FormID"]["add"] = array("table" => "announcer2022", "field" => "FormID", "page" => "add");
		if( !isset( $lookupTableLinks["Form2022"] ) ) {
			$lookupTableLinks["Form2022"] = array();
		}
		if( !isset( $lookupTableLinks["Form2022"]["announcer2022.FormID"] )) {
			$lookupTableLinks["Form2022"]["announcer2022.FormID"] = array();
		}
		$lookupTableLinks["Form2022"]["announcer2022.FormID"]["search"] = array("table" => "announcer2022", "field" => "FormID", "page" => "search");
		if( !isset( $lookupTableLinks["Form2022"] ) ) {
			$lookupTableLinks["Form2022"] = array();
		}
		if( !isset( $lookupTableLinks["Form2022"]["announcer2022.NameOfOrg"] )) {
			$lookupTableLinks["Form2022"]["announcer2022.NameOfOrg"] = array();
		}
		$lookupTableLinks["Form2022"]["announcer2022.NameOfOrg"]["edit"] = array("table" => "announcer2022", "field" => "NameOfOrg", "page" => "edit");
		if( !isset( $lookupTableLinks["Form2022"] ) ) {
			$lookupTableLinks["Form2022"] = array();
		}
		if( !isset( $lookupTableLinks["Form2022"]["announcer2022.KeyPeople"] )) {
			$lookupTableLinks["Form2022"]["announcer2022.KeyPeople"] = array();
		}
		$lookupTableLinks["Form2022"]["announcer2022.KeyPeople"]["edit"] = array("table" => "announcer2022", "field" => "KeyPeople", "page" => "edit");
		if( !isset( $lookupTableLinks["Form2022"] ) ) {
			$lookupTableLinks["Form2022"] = array();
		}
		if( !isset( $lookupTableLinks["Form2022"]["announcer2022.DescriptionOfEntry"] )) {
			$lookupTableLinks["Form2022"]["announcer2022.DescriptionOfEntry"] = array();
		}
		$lookupTableLinks["Form2022"]["announcer2022.DescriptionOfEntry"]["edit"] = array("table" => "announcer2022", "field" => "DescriptionOfEntry", "page" => "edit");
		if( !isset( $lookupTableLinks["Form2022"] ) ) {
			$lookupTableLinks["Form2022"] = array();
		}
		if( !isset( $lookupTableLinks["Form2022"]["announcer2022.LowerThirds"] )) {
			$lookupTableLinks["Form2022"]["announcer2022.LowerThirds"] = array();
		}
		$lookupTableLinks["Form2022"]["announcer2022.LowerThirds"]["edit"] = array("table" => "announcer2022", "field" => "LowerThirds", "page" => "edit");
}

?>