<?php
$tdatacarsign2024 = array();
$tdatacarsign2024[".searchableFields"] = array();
$tdatacarsign2024[".ShortName"] = "carsign2024";
$tdatacarsign2024[".OwnerID"] = "UID";
$tdatacarsign2024[".OriginalTable"] = "Form2024";


$tdatacarsign2024[".pagesByType"] = my_json_decode( "{\"list\":[\"list\"],\"print\":[\"print\",\"print1\"],\"search\":[\"search\"],\"view\":[\"view\",\"view1\"]}" );
$tdatacarsign2024[".originalPagesByType"] = $tdatacarsign2024[".pagesByType"];
$tdatacarsign2024[".pages"] = types2pages( my_json_decode( "{\"list\":[\"list\"],\"print\":[\"print\",\"print1\"],\"search\":[\"search\"],\"view\":[\"view\",\"view1\"]}" ) );
$tdatacarsign2024[".originalPages"] = $tdatacarsign2024[".pages"];
$tdatacarsign2024[".defaultPages"] = my_json_decode( "{\"list\":\"list\",\"print\":\"print\",\"search\":\"search\",\"view\":\"view\"}" );
$tdatacarsign2024[".originalDefaultPages"] = $tdatacarsign2024[".defaultPages"];

//	field labels
$fieldLabelscarsign2024 = array();
$fieldToolTipscarsign2024 = array();
$pageTitlescarsign2024 = array();
$placeHolderscarsign2024 = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelscarsign2024["English"] = array();
	$fieldToolTipscarsign2024["English"] = array();
	$placeHolderscarsign2024["English"] = array();
	$pageTitlescarsign2024["English"] = array();
	$fieldLabelscarsign2024["English"]["ID"] = "Entry ID";
	$fieldToolTipscarsign2024["English"]["ID"] = "";
	$placeHolderscarsign2024["English"]["ID"] = "";
	$fieldLabelscarsign2024["English"]["NameOfOrg"] = "Name Of Organization";
	$fieldToolTipscarsign2024["English"]["NameOfOrg"] = "";
	$placeHolderscarsign2024["English"]["NameOfOrg"] = "";
	$fieldLabelscarsign2024["English"]["ParadeOrder"] = "Parade Order";
	$fieldToolTipscarsign2024["English"]["ParadeOrder"] = "";
	$placeHolderscarsign2024["English"]["ParadeOrder"] = "";
	$pageTitlescarsign2024["English"]["print"] = "Car Sign";
	if (count($fieldToolTipscarsign2024["English"]))
		$tdatacarsign2024[".isUseToolTips"] = true;
}


	$tdatacarsign2024[".NCSearch"] = true;



$tdatacarsign2024[".shortTableName"] = "carsign2024";
$tdatacarsign2024[".nSecOptions"] = 0;

$tdatacarsign2024[".mainTableOwnerID"] = "UID";
$tdatacarsign2024[".entityType"] = 1;
$tdatacarsign2024[".connId"] = "vetdaybakchnl_paradesignup_at_server_lcsworld_com";


$tdatacarsign2024[".strOriginalTableName"] = "Form2024";

		 



$tdatacarsign2024[".showAddInPopup"] = false;

$tdatacarsign2024[".showEditInPopup"] = false;

$tdatacarsign2024[".showViewInPopup"] = false;

$tdatacarsign2024[".listAjax"] = false;
//	temporary
//$tdatacarsign2024[".listAjax"] = false;

	$tdatacarsign2024[".audit"] = false;

	$tdatacarsign2024[".locking"] = false;


$pages = $tdatacarsign2024[".defaultPages"];

if( $pages[PAGE_EDIT] ) {
	$tdatacarsign2024[".edit"] = true;
	$tdatacarsign2024[".afterEditAction"] = 1;
	$tdatacarsign2024[".closePopupAfterEdit"] = 1;
	$tdatacarsign2024[".afterEditActionDetTable"] = "";
}

if( $pages[PAGE_ADD] ) {
$tdatacarsign2024[".add"] = true;
$tdatacarsign2024[".afterAddAction"] = 1;
$tdatacarsign2024[".closePopupAfterAdd"] = 1;
$tdatacarsign2024[".afterAddActionDetTable"] = "";
}

if( $pages[PAGE_LIST] ) {
	$tdatacarsign2024[".list"] = true;
}



$tdatacarsign2024[".strSortControlSettingsJSON"] = "";

$tdatacarsign2024[".strClickActionJSON"] = "{\"fields\":{\"Approved\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactAddress\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactCell\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactEmail\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactFax\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactName\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactPhone\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ContactZip\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"DescriptionOfEntry\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"EntrySize\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"EntryType\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"ID\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"KeyPeople\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"NameOfOrg\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"OnSiteCheckIn\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"SchoolClass\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"TypeOfOrg\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"UID\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}}},\"row\":{\"action\":\"open\",\"codeData\":{},\"gridData\":{\"action\":\"checkbox\",\"table\":null},\"openData\":{\"how\":\"goto\",\"page\":\"view\",\"table\":null,\"url\":\"\"}}}";



if( $pages[PAGE_VIEW] ) {
$tdatacarsign2024[".view"] = true;
}

if( $pages[PAGE_IMPORT] ) {
$tdatacarsign2024[".import"] = true;
}

if( $pages[PAGE_EXPORT] ) {
$tdatacarsign2024[".exportTo"] = true;
}

if( $pages[PAGE_PRINT] ) {
$tdatacarsign2024[".printFriendly"] = true;
}



$tdatacarsign2024[".showSimpleSearchOptions"] = true; // temp fix #13449

// Allow Show/Hide Fields in GRID
$tdatacarsign2024[".allowShowHideFields"] = true; // temp fix #13449
//

// Allow Fields Reordering in GRID
$tdatacarsign2024[".allowFieldsReordering"] = true; // temp fix #13449
//

$tdatacarsign2024[".isUseAjaxSuggest"] = true;

$tdatacarsign2024[".rowHighlite"] = true;



						

$tdatacarsign2024[".ajaxCodeSnippetAdded"] = false;

$tdatacarsign2024[".buttonsAdded"] = false;

$tdatacarsign2024[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacarsign2024[".isUseTimeForSearch"] = false;


$tdatacarsign2024[".badgeColor"] = "CD853F";


$tdatacarsign2024[".allSearchFields"] = array();
$tdatacarsign2024[".filterFields"] = array();
$tdatacarsign2024[".requiredSearchFields"] = array();

$tdatacarsign2024[".googleLikeFields"] = array();
$tdatacarsign2024[".googleLikeFields"][] = "ID";
$tdatacarsign2024[".googleLikeFields"][] = "NameOfOrg";
$tdatacarsign2024[".googleLikeFields"][] = "ParadeOrder";



$tdatacarsign2024[".tableType"] = "list";

$tdatacarsign2024[".printerPageOrientation"] = 1;
$tdatacarsign2024[".nPrinterPageScale"] = 100;

$tdatacarsign2024[".nPrinterSplitRecords"] = 1;

$tdatacarsign2024[".geocodingEnabled"] = false;










$tdatacarsign2024[".pageSize"] = 500;

$tdatacarsign2024[".warnLeavingPages"] = true;

$tdatacarsign2024[".hideEmptyFieldsOnView"] = true;


$tstrOrderBy = "ORDER BY Form2024.ParadeOrder";
$tdatacarsign2024[".strOrderBy"] = $tstrOrderBy;

$tdatacarsign2024[".orderindexes"] = array();
	$tdatacarsign2024[".orderindexes"][] = array(3, (1 ? "ASC" : "DESC"), "Form2024.ParadeOrder");



$tdatacarsign2024[".sqlHead"] = "SELECT Form2024.ID,  Form2024.NameOfOrg,  Form2024.ParadeOrder";
$tdatacarsign2024[".sqlFrom"] = "FROM QRCodeURL  , Form2024";
$tdatacarsign2024[".sqlWhereExpr"] = "(Form2024.Approved =1)";
$tdatacarsign2024[".sqlTail"] = "";










//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = -1;
$tdatacarsign2024[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacarsign2024[".arrGroupsPerPage"] = $arrGPP;

$tdatacarsign2024[".highlightSearchResults"] = true;

$tableKeyscarsign2024 = array();
$tableKeyscarsign2024[] = "ID";
$tdatacarsign2024[".Keys"] = $tableKeyscarsign2024;


$tdatacarsign2024[".hideMobileList"] = array();




//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("CarSign2024","ID");
	$fdata["FieldType"] = 3;


		$fdata["AutoInc"] = true;

	
										

		$fdata["strField"] = "ID";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form2024.ID";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacarsign2024["ID"] = $fdata;
		$tdatacarsign2024[".searchableFields"][] = "ID";
//	NameOfOrg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "NameOfOrg";
	$fdata["GoodName"] = "NameOfOrg";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("CarSign2024","NameOfOrg");
	$fdata["FieldType"] = 201;


	
	
										

		$fdata["strField"] = "NameOfOrg";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form2024.NameOfOrg";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacarsign2024["NameOfOrg"] = $fdata;
		$tdatacarsign2024[".searchableFields"][] = "NameOfOrg";
//	ParadeOrder
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "ParadeOrder";
	$fdata["GoodName"] = "ParadeOrder";
	$fdata["ownerTable"] = "Form2024";
	$fdata["Label"] = GetFieldLabel("CarSign2024","ParadeOrder");
	$fdata["FieldType"] = 200;


	
	
										

		$fdata["strField"] = "ParadeOrder";

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Form2024.ParadeOrder";

	
	
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		
	
		$vdata["NeedEncode"] = true;

	
		$vdata["truncateText"] = true;
	$vdata["NumberOfChars"] = 80;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["weekdayMessage"] = array("message" => "", "messageType" => "Text");
	$edata["weekdays"] = "[]";


	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";
		$edata["acceptFileTypesHtml"] = "";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 0;
			$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = false;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 10;

		$fdata["filterBy"] = 0;

	

	
	
//end of Filters settings


	$tdatacarsign2024["ParadeOrder"] = $fdata;
		$tdatacarsign2024[".searchableFields"][] = "ParadeOrder";


$tables_data["CarSign2024"]=&$tdatacarsign2024;
$field_labels["CarSign2024"] = &$fieldLabelscarsign2024;
$fieldToolTips["CarSign2024"] = &$fieldToolTipscarsign2024;
$placeHolders["CarSign2024"] = &$placeHolderscarsign2024;
$page_titles["CarSign2024"] = &$pageTitlescarsign2024;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

//if !@TABLE.bReportCrossTab

$detailsTablesData["CarSign2024"] = array();
//endif

// tables which are master tables for current table (detail)
$masterTablesData["CarSign2024"] = array();



// -----------------end  prepare master-details data arrays ------------------------------//



require_once(getabspath("classes/sql.php"));











function createSqlQuery_carsign2024()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "Form2024.ID,  Form2024.NameOfOrg,  Form2024.ParadeOrder";
$proto0["m_strFrom"] = "FROM QRCodeURL  , Form2024";
$proto0["m_strWhere"] = "(Form2024.Approved =1)";
$proto0["m_strOrderBy"] = "ORDER BY Form2024.ParadeOrder";
	
					
;
						$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "Form2024.Approved =1";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "Approved",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "CarSign2024"
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "=1";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "CarSign2024"
));

$proto6["m_sql"] = "Form2024.ID";
$proto6["m_srcTableName"] = "CarSign2024";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "NameOfOrg",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "CarSign2024"
));

$proto8["m_sql"] = "Form2024.NameOfOrg";
$proto8["m_srcTableName"] = "CarSign2024";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "ParadeOrder",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "CarSign2024"
));

$proto10["m_sql"] = "Form2024.ParadeOrder";
$proto10["m_srcTableName"] = "CarSign2024";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto12=array();
$proto12["m_link"] = "SQLL_MAIN";
			$proto13=array();
$proto13["m_strName"] = "QRCodeURL";
$proto13["m_srcTableName"] = "CarSign2024";
$proto13["m_columns"] = array();
$proto13["m_columns"][] = "ID";
$proto13["m_columns"][] = "ProgramURL";
$proto13["m_columns"][] = "PageName";
$obj = new SQLTable($proto13);

$proto12["m_table"] = $obj;
$proto12["m_sql"] = "QRCodeURL";
$proto12["m_alias"] = "";
$proto12["m_srcTableName"] = "CarSign2024";
$proto14=array();
$proto14["m_sql"] = "";
$proto14["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto14["m_column"]=$obj;
$proto14["m_contained"] = array();
$proto14["m_strCase"] = "";
$proto14["m_havingmode"] = false;
$proto14["m_inBrackets"] = false;
$proto14["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto14);

$proto12["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto12);

$proto0["m_fromlist"][]=$obj;
												$proto16=array();
$proto16["m_link"] = "SQLL_CROSSJOIN";
			$proto17=array();
$proto17["m_strName"] = "Form2024";
$proto17["m_srcTableName"] = "CarSign2024";
$proto17["m_columns"] = array();
$proto17["m_columns"][] = "ID";
$proto17["m_columns"][] = "NameOfOrg";
$proto17["m_columns"][] = "TypeOfOrg";
$proto17["m_columns"][] = "SchoolClass";
$proto17["m_columns"][] = "EntryType";
$proto17["m_columns"][] = "KeyPeople";
$proto17["m_columns"][] = "EntrySize";
$proto17["m_columns"][] = "ContactName";
$proto17["m_columns"][] = "ContactAddress";
$proto17["m_columns"][] = "ContactEmail";
$proto17["m_columns"][] = "ContactZip";
$proto17["m_columns"][] = "ContactPhone";
$proto17["m_columns"][] = "ContactFax";
$proto17["m_columns"][] = "ContactCell";
$proto17["m_columns"][] = "DescriptionOfEntry";
$proto17["m_columns"][] = "UID";
$proto17["m_columns"][] = "NumOfHorses";
$proto17["m_columns"][] = "Approved";
$proto17["m_columns"][] = "LowerThirds";
$proto17["m_columns"][] = "ParadeOrder";
$proto17["m_columns"][] = "GroupOrganizer";
$proto17["m_columns"][] = "AgreetoRules";
$proto17["m_columns"][] = "LiabilityWavierSigned";
$obj = new SQLTable($proto17);

$proto16["m_table"] = $obj;
$proto16["m_sql"] = "Form2024";
$proto16["m_alias"] = "";
$proto16["m_srcTableName"] = "CarSign2024";
$proto18=array();
$proto18["m_sql"] = "";
$proto18["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto18["m_column"]=$obj;
$proto18["m_contained"] = array();
$proto18["m_strCase"] = "";
$proto18["m_havingmode"] = false;
$proto18["m_inBrackets"] = false;
$proto18["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto18);

$proto16["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto16);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto20=array();
						$obj = new SQLField(array(
	"m_strName" => "ParadeOrder",
	"m_strTable" => "Form2024",
	"m_srcTableName" => "CarSign2024"
));

$proto20["m_column"]=$obj;
$proto20["m_bAsc"] = 1;
$proto20["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto20);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="CarSign2024";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_carsign2024 = createSqlQuery_carsign2024();


	
					
;

			

$tdatacarsign2024[".sqlquery"] = $queryData_carsign2024;



include_once(getabspath("include/carsign2024_events.php"));
$tableEvents["CarSign2024"] = new eventclass_carsign2024;
$tdatacarsign2024[".hasEvents"] = true;

?>