<?php
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");


require_once("include/dbcommon.php");
require_once('classes/menupage.php');


Security::processLogoutRequest();
if( !isLogged() || Security::isGuest() ) 
{
	Security::tryRelogin();
}

if( !isLogged() )
{
	HeaderRedirect("login");
	return;
}


if (($_SESSION["MyURL"] == "") || (!Security::isGuest())) {
	Security::saveRedirectURL();
}


require_once('include/xtempl.php');
require_once(getabspath("classes/cipherer.php"));

include_once(getabspath("include/Entry_events.php"));
$tableEvents["Form"] = new eventclass_Entry;
include_once(getabspath("include/adminunapprovedentry_events.php"));
$tableEvents["AdminUnApprovedEntry"] = new eventclass_adminunapprovedentry;
include_once(getabspath("include/signin_events.php"));
$tableEvents["SignIn"] = new eventclass_signin;
include_once(getabspath("include/frmhardcopy_events.php"));
$tableEvents["FrmHardCopy"] = new eventclass_frmhardcopy;
include_once(getabspath("include/frmparadeorder_events.php"));
$tableEvents["FrmParadeOrder"] = new eventclass_frmparadeorder;
include_once(getabspath("include/carsign_events.php"));
$tableEvents["CarSign"] = new eventclass_carsign;
include_once(getabspath("include/carsign2024_events.php"));
$tableEvents["CarSign2024"] = new eventclass_carsign2024;

$xt = new Xtempl();

//array of params for classes
$params = array();
$params["id"] = postvalue_number("id"); 
$params["xt"] = &$xt;
$params["tName"] = GLOBAL_PAGES;
$params["pageType"] = PAGE_MENU;
$params["isGroupSecurity"] = $isGroupSecurity;
$params["needSearchClauseObj"] = false;
$params["pageName"] = postvalue("page"); 

$pageObject = new MenuPage($params);
$pageObject->init();

$pageObject->process();
?>